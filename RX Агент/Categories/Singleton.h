//
//  Singleton.h
//  Abie System
//
//  Created by RX Group on 20.04.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CalcModel.h"
#import "osagoModel.h"

@interface Singleton : NSObject


+ (Singleton *) sharedInstance;
@property CalcModel *model;
@property osagoModel *osago;
//страхователь
@property NSString *phone;


//фотографии
@property UIImage *photo;
@property NSInteger indexForPhoto;

//владелец
-(BOOL)firstDone;
-(BOOL)secondDone;
-(BOOL)thirdDone;
-(BOOL)fourthDone;
-(BOOL)fiveDone;
-(BOOL)sixDone;

-(void)refreshAll;
//калькулятор
@property BOOL isLimit;
@property int indexContain;
-(NSDictionary*)createJsonRequest;

@property NSInteger countDone1;
@property NSInteger countDone2;
@property NSInteger countDone3;
@property NSInteger countDone4;
@property NSInteger countDone6;
@end
