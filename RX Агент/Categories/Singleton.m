//
//  Singleton.m
//  RX Агент
//
//  Created by RX Group on 05.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

static Singleton *sharedSingleton = nil;

+ (Singleton *) sharedInstance
{
    if (sharedSingleton == nil)
    {
        sharedSingleton = [[Singleton alloc] init];
        sharedSingleton.model = [CalcModel new];
        sharedSingleton.osago = [osagoModel new];
        sharedSingleton.osago.isOwner = TRUE;
        sharedSingleton.model.KBM1 =@"";
        sharedSingleton.model.KBM2 =@"";
        sharedSingleton.model.KBM3 =@"";
        sharedSingleton.model.KBM4 =@"";
        sharedSingleton.model.maxExperience3=@"";
        sharedSingleton.model.maxAge22=@"";
        sharedSingleton.isLimit=TRUE;
      
       
    }
    return sharedSingleton;
}

-(void)refreshAll{
    sharedSingleton.model = [CalcModel new];
    sharedSingleton.osago = [osagoModel new];
    sharedSingleton.osago.isOwner = TRUE;
    sharedSingleton.model.KBM1 =@"";
    sharedSingleton.model.KBM2 =@"";
    sharedSingleton.model.KBM3 =@"";
    sharedSingleton.model.KBM4 =@"";
    sharedSingleton.model.maxExperience3=@"";
    sharedSingleton.model.maxAge22=@"";
    sharedSingleton.isLimit=TRUE;
}
-(NSDictionary*)createJsonRequest{
    NSDictionary *dictionary = [NSDictionary dictionary];
    NSString *category =[NSString stringWithFormat:@"%ld",(long) _model.categoryID];
    NSString *driverCount = [NSString stringWithFormat:@"%ld",(long) _model.countDriver];
    NSString *maxSit = @"";
    NSString *maxMass = @"";
    if(![self isEmpty:_model.countSit]){
        maxSit = [_model.countSit isEqualToString:@"Более 16"] ? @"true":@"false";
    }
    if(![self isEmpty:_model.mass]){
        maxMass = [_model.mass isEqualToString:@"Более 16 тонн"] ? @"true":@"false";
    }
    
    dictionary = @{
                 @"isIndividual": @"true",
                 @"startDate":_model.startDateJSON,
                 @"period": _model.periodJSON,
                 @"minKBM": [self getMinimalKBM],
                 @"category": category,
                 @"region": _model.regionJSON,
                 @"city": ![self isEmpty:_model.cityJSON] ?_model.cityJSON:@"",
                 @"drivers": _isLimit? driverCount:@"",
                 @"maxAge22":_model.maxAge22,
                 @"maxExperience3":_model.maxExperience3,
                 @"target": ![self isEmpty:_model.targetJSON] ? _model.targetJSON: @"",
                 @"power":![self isEmpty:_model.power] ?_model.power:@"",
                 @"withTrailer":_model.trailJSON ? @"true":@"false",
                 @"maxSeats16": maxSit ,
                 @"maxMass16":maxMass 
                   };
    
    return dictionary;
}
-(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}
-(NSString *)getMinimalKBM{
    NSArray *array = @[_model.KBM1,_model.KBM2,_model.KBM3,_model.KBM4];
    NSMutableArray *newArray = [NSMutableArray new];
    for (int i = 0; i<_model.countDriver; i++) {
        [newArray addObject:array[i]];
    }
    if(_isLimit){
        NSNumber *min=[newArray valueForKeyPath:@"@max.doubleValue"];
        return [min stringValue];
    }
    return _model.ownerKBM;
}
-(BOOL)firstDone{
    _countDone1 = 2;
    if(_osago.isOwner){
        if(_osago.hasTempReg){
            _countDone1 ++;
            if(![_osago.tempRegPageID isEqualToString:@""]){
                _countDone1 --;
            }
        }
        if(_osago.hasDKP){
           _countDone1 ++;
            if(_osago.noDKP){
                _countDone1 --;
            }else{
                if(![_osago.saleContractPageID isEqualToString:@""]){
                    _countDone1 --;
                }
            }
        }
        if(![_osago.documentPageFirstID isEqualToString:@""]){
            _countDone1 --;
        }
        if(![_osago.documentPageSecondID isEqualToString:@""]){
            _countDone1 --;
        }
    }else{
        if(![_osago.documentPageFirstID isEqualToString:@""]){
            _countDone1 --;
        }
        if(![_osago.documentPageSecondID isEqualToString:@""]){
            _countDone1 --;
        }
    }
    if(_countDone1==0){
        return YES;
    }else{
        return NO;
    }
    return NO;
}
-(BOOL)secondDone{
    _countDone2 = 2;
    if(_osago.hasTempReg){
        _countDone2 ++;
        if(![_osago.tempRegPageID isEqualToString:@""]){
            _countDone2 --;
        }
    }
    if(_osago.hasDKP){
        _countDone2 ++;
        if(_osago.noDKP){
            _countDone2 --;
        }else{
            if(![_osago.saleContractPageID isEqualToString:@""]){
                _countDone2 --;
            }
        }
    }
    if(![_osago.ownerDocumentPageFirstID isEqualToString:@""]){
        _countDone2 --;
    }
    if(![_osago.ownerDocumentPageSecondID isEqualToString:@""]){
        _countDone2 --;
    }
    if(_countDone2==0){
        return YES;
    }else{
        return NO;
    }
}
-(BOOL)thirdDone{
    _countDone3=6;
    if(![_osago.vehicleTargetJSON isEqualToString:@""]){
        _countDone3--;
    }
    if(_osago.noVehiclePassportPage){
        
        if(![_osago.vehicleCertificatePageFirstID isEqualToString:@""]&&![_osago.vehicleCertificatePageSecondID isEqualToString:@""]){
             _countDone3 = _countDone3 - 2;
        }
    }else{
        if(![_osago.vehiclePassportPageFirstID isEqualToString:@""]&&![_osago.vehiclePassportPageSecondID isEqualToString:@""]){
            _countDone3 = _countDone3 - 2;
        }
    }
    if(_osago.noVehicleCertificatePage){
      
        if(![_osago.vehiclePassportPageFirstID isEqualToString:@""]&&![_osago.vehiclePassportPageSecondID isEqualToString:@""]){
            _countDone3 = _countDone3 - 2;
        }
    }else{
        if(![_osago.vehicleCertificatePageFirstID isEqualToString:@""]&&![_osago.vehicleCertificatePageSecondID isEqualToString:@""]){
            _countDone3 = _countDone3 - 2;
        }
    }
    if(![_osago.cardPageFirstID isEqualToString:@""]){
        _countDone3 --;
    }
   
    if(_countDone3==0){
        return YES;
    }else{
        return NO;
    }
}
-(BOOL)fourthDone{
    _countDone4=2*_model.countDriver;
    if(_isLimit){
        for (int i=0; i<_model.countDriver; i++) {
            if (![_osago.dictionary[@"drivers"][i][@"driverLicensePageFirstID"] isEqualToString:@""]&&![_osago.dictionary[@"drivers"][i][@"driverLicensePageSecondID"] isEqualToString:@""] ) {
                _countDone4=_countDone4-2;
            }
        }
        if(_countDone4==0){
            return YES;
        }else{
            return NO;
        }
    }else{
        return YES;
    }
}
-(BOOL)fiveDone{
    if(_osago.cardDict){
        return YES;
    }else{
        return NO;
    }
}
-(BOOL)sixDone{
    _countDone6=4;
    if(![_model.startDateJSON isEqualToString:@""]){
        _countDone6--;
    }
    if(![_model.periodJSON isEqualToString:@""]){
        _countDone6--;
    }
    if(![_osago.creatorFullName isEqualToString:@""]){
        _countDone6--;
    }
    if(![_osago.creatorPhoneJSON isEqualToString:@""]&&_osago.creatorPhoneJSON.length==10){
        _countDone6--;
    }
    if(_countDone6==0){
        return YES;
    }else{
        return NO;
    }
}
@end
