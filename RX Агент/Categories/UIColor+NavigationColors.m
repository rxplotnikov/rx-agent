//
//  UIColor+NavigationColors.m
//  RX Агент
//
//  Created by RX Group on 06.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "UIColor+NavigationColors.h"

@implementation UIColor (NavigationColors)

+ (UIColor *) customRedColor{
    return [UIColor colorWithRed:255.f/255.f green:122.f/255.f blue:122.f/255.f alpha:1.f];
}
+ (UIColor *) customGreenColor{
    return [UIColor colorWithRed:188.f/255.f green:215.f/255.f blue:128.f/255.f alpha:1.f];
}
+ (UIColor *) customYellowColor{
    return [UIColor colorWithRed:251.f/255.f green:217.f/255.f blue:125.f/255.f alpha:1.f];
}
+ (UIColor *) customGrayColor{
    return [UIColor colorWithRed:229.f/255.f green:229.f/255.f blue:229.f/255.f alpha:1.f];
}

+ (UIColor *) cardColor1{
    return [UIColor colorWithRed:245.f/255.f green:129.f/255.f blue:112.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor2{
    return [UIColor colorWithRed:106.f/255.f green:87.f/255.f blue:231.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor3{
    return [UIColor colorWithRed:214.f/255.f green:54.f/255.f blue:53.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor4{
    return [UIColor colorWithRed:86.f/255.f green:162.f/255.f blue:219.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor5{
    return [UIColor colorWithRed:192.f/255.f green:109.f/255.f blue:243.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor6{
    return [UIColor colorWithRed:33.f/255.f green:33.f/255.f blue:33.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor7{
    return [UIColor colorWithRed:68.f/255.f green:183.f/255.f blue:95.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor8{
    return [UIColor colorWithRed:38.f/255.f green:152.f/255.f blue:169.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor9{
    return [UIColor colorWithRed:158.f/255.f green:165.f/255.f blue:186.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor10{
    return [UIColor colorWithRed:219.f/255.f green:230.f/255.f blue:89.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor11{
    return [UIColor colorWithRed:183.f/255.f green:68.f/255.f blue:82.f/255.f alpha:1.f];
}
+ (UIColor *) cardColor12{
    return [UIColor colorWithRed:79.f/255.f green:105.f/255.f blue:237.f/255.f alpha:1.f];
}
@end
