//
//  UIColor+NavigationColors.h
//  RX Агент
//
//  Created by RX Group on 06.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (NavigationColors)


+ (UIColor *) customRedColor;
+ (UIColor *) customGreenColor;
+ (UIColor *) customYellowColor;
+ (UIColor *) customGrayColor;
+ (UIColor *)cardColor1;
+ (UIColor *)cardColor2;
+ (UIColor *)cardColor3;
+ (UIColor *)cardColor4;
+ (UIColor *)cardColor5;
+ (UIColor *)cardColor6;
+ (UIColor *)cardColor7;
+ (UIColor *)cardColor8;
+ (UIColor *)cardColor9;
+ (UIColor *)cardColor10;
+ (UIColor *)cardColor11;
+ (UIColor *)cardColor12;
@end
