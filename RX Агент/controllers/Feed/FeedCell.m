//
//  FeedCell.m
//  RX Агент
//
//  Created by RX Group on 09.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "FeedCell.h"

@implementation FeedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.bg.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    self.bg.layer.shadowOpacity = 3.f;
    self.bg.layer.shadowRadius = 0.5f;
    self.bg.layer.shadowOffset = CGSizeMake(1.f, 1.f);
    self.bg.layer.cornerRadius = 7.f;

}

-(void)setIconFromType:(int)type isShort:(BOOL)isShort isPaid:(BOOL)isPaid{
     [_icon setImage:[UIImage imageNamed:@"cog_icon"]];
    _status.text = @"В обработке";
    if(type==0){
        _status.text = @"Новая заявка";
        if(isShort){
            
            [_icon setImage:[UIImage imageNamed:@"photo_icon"]];
        }else{
            
            [_icon setImage:[UIImage imageNamed:@"pencil_icon"]];
        }
    }
    if(type==1){
        _status.text = @"Отправлено";
        [_icon setImage:[UIImage imageNamed:@"air_icon"]];
    }
    if(type==3){
        _status.text = @"Примите условия";
        [_icon setImage:[UIImage imageNamed:@"question_icon"]];
    }
    if(type==4){
         _status.text = @"Отказ";
        [_icon setImage:[UIImage imageNamed:@"close_icon"]];
    }
    if(type==254){
        if(!isPaid){
             _status.text = @"Ожидает оплаты";
            [_icon setImage:[UIImage imageNamed:@"rub_icon"]];
        }else{
             _status.text = @"Успешно оформлена";
            [_icon setImage:[UIImage imageNamed:@"done_icon"]];
        }
    }
    
}
@end
