//
//  FeedCell.h
//  RX Агент
//
//  Created by RX Group on 09.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedLabel.h"

@interface FeedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bg;
@property (weak, nonatomic) IBOutlet RoundedLabel *numberPolice;
@property (weak, nonatomic) IBOutlet UILabel *costLbl;
@property (weak, nonatomic) IBOutlet UILabel *createDate;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *status;

//самолет
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *period;

//черновик
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UILabel *dateCher;
@property (weak, nonatomic) IBOutlet UILabel *periodCher;

//шестеренка
@property (weak, nonatomic) IBOutlet UILabel *FIOLbl;
@property (weak, nonatomic) IBOutlet UILabel *TSlbl;
@property (weak, nonatomic) IBOutlet UILabel *summLbl;

//успех
@property (weak, nonatomic) IBOutlet UILabel *startDate;
@property (weak, nonatomic) IBOutlet UILabel *perLbl;
@property (weak, nonatomic) IBOutlet UILabel *strahLbl;
@property (weak, nonatomic) IBOutlet UILabel *serNumbPol;


-(void)setIconFromType:(int)type isShort:(BOOL)isShort isPaid:(BOOL)isPaid;
@end
