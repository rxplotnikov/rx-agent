//
//  FeedViewController.h
//  RX Агент
//
//  Created by RX Group on 28.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSearchBar.h"

@interface FeedViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet CustomSearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *balanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *contractorTypeLbl;

@end
