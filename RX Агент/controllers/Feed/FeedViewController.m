//
//  FeedViewController.m
//  RX Агент
//
//  Created by RX Group on 28.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedCell.h"
#import "RXServerConnector.h"
#import "RXLoadingView.h"

@interface FeedViewController ()<SearchBarDelegate,UITableViewDelegate>{
    NSMutableDictionary *mainDict;
    NSMutableArray *polices;
    NSIndexPath *expandingIndexPath;
    NSIndexPath *expandedIndexPath;
    RXLoadingView *loadView;
    BOOL isSearch;
    NSMutableArray *searchArray;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isLoad;
    NSInteger countWork;
}

@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage=1;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!polices){
        [self receiveNotification];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveNotification)
                                                     name:@"load0"
                                                   object:nil];
    }
}

-(void)receiveNotification{
    self.searchBar.cancelButtonHidden = YES;
    self.searchBar.placeholder = @"Введите номер заявки";
    self.searchBar.delegate = self;
    [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/Employees/me" withBlock:^(NSMutableDictionary *dictionary) {
        self->mainDict = dictionary;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(![self->mainDict[@"profile"][@"isSystemAdmin"] boolValue]){
                [self configUI];
            }
        });
    }];
 
    [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies?PerPage=%ld",(long)currentPage*25] withBlock:^(NSMutableDictionary *dictionary) {
        self->totalPages = [dictionary[@"meta"][@"total_pages"]integerValue];
        self->countWork = [dictionary[@"meta"][@"in_work"]integerValue];
        self->polices = [NSMutableArray new];
        for (int i = 0; i<[dictionary[@"eosagoPolicies"]count]; i++) {
                [self->polices addObject:dictionary[@"eosagoPolicies"][i]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getCountForWork];
            [self->_table reloadData];
        });
    }];
}

-(void)searchBar:(CustomSearchBar *)searchBar textDidChange:(NSString *)searchText{
 
}
-(void)searchBarSearchButtonClicked:(CustomSearchBar *)searchBar{
    self->expandingIndexPath = nil;
    self->expandedIndexPath = nil;
    if(searchBar.text.length>0){
        
        [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies?Query=%@",searchBar.text] withBlock:^(NSMutableDictionary *dictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
            
            self->searchArray=[NSMutableArray new];
            for (int i = 0; i<[dictionary[@"eosagoPolicies"]count]; i++) {
                [self->searchArray addObject:dictionary[@"eosagoPolicies"][i]];
            }
            [self.table reloadData];
                
            });
        }];
    }else{
         [self.view endEditing:YES];
         [self.table reloadData];
    }
}




-(void)getCountForWork{
    
    NSDictionary* userInfo = @{@"total": @(countWork)};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"getCountWork" object:nil userInfo:userInfo];
}
-(void)configUI{
    _balanceLbl.text =[NSString stringWithFormat:@"%@ ₽",mainDict[@"profile"][@"contractor"][@"balance"]];
    _contractorTypeLbl.text = [NSString stringWithFormat:@"№ %@",mainDict[@"profile"][@"contractor"][@"id"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_searchBar.text.length>0){
        if(indexPath.row==0){
            
            if([searchArray[indexPath.row][@"state"]integerValue]==0&&[indexPath isEqual:expandedIndexPath]){
                return 120;
            }
            if([searchArray[indexPath.row][@"state"]integerValue]==254&&[indexPath isEqual:expandedIndexPath]){
                return 150;
            }
            if([searchArray[indexPath.row][@"state"]integerValue]==3||[searchArray[indexPath.row][@"state"]integerValue]==2||[searchArray[indexPath.row][@"state"]integerValue]==5||[searchArray[indexPath.row][@"state"]integerValue]==6||[searchArray[indexPath.row][@"state"]integerValue]==7||[searchArray[indexPath.row][@"state"]integerValue]==8||[searchArray[indexPath.row][@"state"]integerValue]==9){
                if([indexPath isEqual:expandedIndexPath]){
                    return 120;
                }
            }
        }else{
            if([searchArray[indexPath.row-1][@"state"]integerValue]==3||[searchArray[indexPath.row-1][@"state"]integerValue]==2||[searchArray[indexPath.row-1][@"state"]integerValue]==5||[searchArray[indexPath.row-1][@"state"]integerValue]==6||[searchArray[indexPath.row-1][@"state"]integerValue]==7||[searchArray[indexPath.row-1][@"state"]integerValue]==8||[searchArray[indexPath.row-1][@"state"]integerValue]==9){
                if([indexPath isEqual:expandedIndexPath]){
                    return 120;
                }
            }
            if([searchArray[indexPath.row-1][@"state"]integerValue]==254&&[indexPath isEqual:expandedIndexPath]){
                return 150;
            }
            if([searchArray[indexPath.row-1][@"state"]integerValue]==0&&[indexPath isEqual:expandedIndexPath]){
                return 120;
            }
        }
    }else{
        if(indexPath.row==0){
        
            if([polices[indexPath.row][@"state"]integerValue]==0&&[indexPath isEqual:expandedIndexPath]){
                return 120;
            }
            if([polices[indexPath.row][@"state"]integerValue]==254&&[indexPath isEqual:expandedIndexPath]){
                return 150;
            }
            if([polices[indexPath.row][@"state"]integerValue]==3||[polices[indexPath.row][@"state"]integerValue]==2||[polices[indexPath.row][@"state"]integerValue]==5||[polices[indexPath.row][@"state"]integerValue]==6||[polices[indexPath.row][@"state"]integerValue]==7||[polices[indexPath.row][@"state"]integerValue]==8||[polices[indexPath.row][@"state"]integerValue]==9){
                if([indexPath isEqual:expandedIndexPath]){
                    return 120;
                }
            }
        }else{
            if([polices[indexPath.row-1][@"state"]integerValue]==3||[polices[indexPath.row-1][@"state"]integerValue]==2||[polices[indexPath.row-1][@"state"]integerValue]==5||[polices[indexPath.row-1][@"state"]integerValue]==6||[polices[indexPath.row-1][@"state"]integerValue]==7||[polices[indexPath.row-1][@"state"]integerValue]==8||[polices[indexPath.row-1][@"state"]integerValue]==9){
                if([indexPath isEqual:expandedIndexPath]){
                    return 120;
                }
            }
            if([polices[indexPath.row-1][@"state"]integerValue]==254&&[indexPath isEqual:expandedIndexPath]){
                return 150;
            }
            if([polices[indexPath.row-1][@"state"]integerValue]==0&&[indexPath isEqual:expandedIndexPath]){
                return 120;
            }
        }
    }
        
    return 90;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_searchBar.text.length>0){
        if (expandedIndexPath) {
            return [searchArray count] + 1;
        }
        return searchArray.count;
    }else{
        if (expandedIndexPath) {
            return [polices count] + 1;
        }
        
       return [polices count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = nil;
    FeedCell *cell = nil;
    NSIndexPath *theIndexPath = [self actualIndexPathForTappedIndexPath:indexPath];
    if(_searchBar.text.length>0){
        if ([indexPath isEqual:expandedIndexPath]) {
            CellIdentifier = @"Expanded2";
            if([searchArray[theIndexPath.row-1][@"state"]integerValue]==0){
                CellIdentifier = @"Expanded3";
            }
            if([searchArray[theIndexPath.row-1][@"state"]integerValue]==1){
                CellIdentifier = @"ExpandedCellIdentifier";
            }
            if([searchArray[theIndexPath.row-1][@"state"]integerValue]==4){
                CellIdentifier = @"cell";
            }
            if([searchArray[theIndexPath.row-1][@"state"]integerValue]==254){
                CellIdentifier = @"Expanded4";
            }
            
        }else{
            CellIdentifier = @"cell";
        }
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[FeedCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:CellIdentifier];
        }
        
        if([[cell reuseIdentifier]isEqualToString:@"Expanded4"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.startDate.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:searchArray[indexPath.row-1][@"startDate"]]];
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==12){
                    cell.perLbl.text =@"Период страхования: 1 год";
                }
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==3){
                    cell.perLbl.text =@"Период страхования: 3 месяца";
                }
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==6){
                    cell.perLbl.text =@"Период страхования: 6 месяцев";
                }
                [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/InsuranceCompanies/%@",searchArray[indexPath.row-1][@"insuranceCompany"]] withBlock:^(NSMutableDictionary *dictionary) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.strahLbl.text =[NSString stringWithFormat:@"Страховая: %@",dictionary[@"insuranceCompany"][@"shortTitle"]];
                    });
                    
                }];
                
                cell.serNumbPol.text =[NSString stringWithFormat:@"Полис: %@ %@",searchArray[indexPath.row-1][@"series"],searchArray[indexPath.row-1][@"number"]];
                
            });
        }
        
        if([[cell reuseIdentifier]isEqualToString:@"Expanded2"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.FIOLbl.text = [NSString stringWithFormat:@"ФИО: %@",searchArray[indexPath.row-1][@"insurantShortName"]];
                cell.TSlbl.text =[NSString stringWithFormat:@"ТС: %@",searchArray[indexPath.row-1][@"vehicleModel"]];
                cell.summLbl.text =[NSString stringWithFormat:@"Стоимость оформления: %ld ₽",[searchArray[indexPath.row-1][@"baseCost"]integerValue]+[searchArray[indexPath.row-1][@"surcharge"]integerValue]];
            });
        }
        
        if([[cell reuseIdentifier]isEqualToString:@"Expanded3"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.dateCher.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:searchArray[indexPath.row-1][@"startDate"]]];
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==12){
                    cell.periodCher.text =@"Период страхования: 1 год";
                }
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==3){
                    cell.periodCher.text =@"Период страхования: 3 месяца";
                }
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==6){
                    cell.periodCher.text =@"Период страхования: 6 месяцев";
                }
                cell.sendButton.tag =indexPath.row-1;
            });
        }
        
        if([[cell reuseIdentifier]isEqualToString:@"ExpandedCellIdentifier"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.dateLbl.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:searchArray[indexPath.row-1][@"startDate"]]];
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==12){
                    cell.period.text =@"Период страхования: 1 год";
                }
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==3){
                    cell.period.text =@"Период страхования: 3 месяца";
                }
                if([searchArray[indexPath.row-1][@"insurancePeriod"]integerValue]==6){
                    cell.period.text =@"Период страхования: 6 месяцев";
                }
            });
        }
        if ([[cell reuseIdentifier] isEqualToString:@"cell"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.numberPolice.text = [NSString stringWithFormat:@"№ %@",[searchArray[theIndexPath.row][@"id"]stringValue]];
                cell.costLbl.text = [NSString stringWithFormat:@"%ld ₽",[searchArray[theIndexPath.row][@"baseCost"]integerValue]+[searchArray[theIndexPath.row][@"surcharge"]integerValue]];
                cell.createDate.text = [self dateFromJson:searchArray[theIndexPath.row][@"createDate"]];
                [ cell setIconFromType:[searchArray[theIndexPath.row][@"state"] intValue] isShort:[searchArray[theIndexPath.row][@"isShort"] boolValue] isPaid:[searchArray[theIndexPath.row][@"paid"] boolValue]];
            });
        }
    }else{
    if ([indexPath isEqual:expandedIndexPath]) {
        CellIdentifier = @"Expanded2";
        if([polices[theIndexPath.row-1][@"state"]integerValue]==0){
            CellIdentifier = @"Expanded3";
        }
        if([polices[theIndexPath.row-1][@"state"]integerValue]==1){
            CellIdentifier = @"ExpandedCellIdentifier";
        }
        if([polices[theIndexPath.row-1][@"state"]integerValue]==4){
            CellIdentifier = @"cell";
        }
        if([polices[theIndexPath.row-1][@"state"]integerValue]==254){
            CellIdentifier = @"Expanded4";
        }
       
    }else{
         CellIdentifier = @"cell";
    }
     cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[FeedCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    if([[cell reuseIdentifier]isEqualToString:@"Expanded4"]){
        dispatch_async(dispatch_get_main_queue(), ^{
        
            cell.startDate.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:self->polices[indexPath.row-1][@"startDate"]]];
            if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==12){
            cell.perLbl.text =@"Период страхования: 1 год";
        }
            if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==3){
            cell.perLbl.text =@"Период страхования: 3 месяца";
        }
            if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==6){
            cell.perLbl.text =@"Период страхования: 6 месяцев";
        }
            [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/InsuranceCompanies/%@",self->polices[indexPath.row-1][@"insuranceCompany"]] withBlock:^(NSMutableDictionary *dictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.strahLbl.text =[NSString stringWithFormat:@"Страховая: %@",dictionary[@"insuranceCompany"][@"shortTitle"]];
            });
            
        }];
        
            cell.serNumbPol.text =[NSString stringWithFormat:@"Полис: %@ %@",self->polices[indexPath.row-1][@"series"],self->polices[indexPath.row-1][@"number"]];
            
        });
    }
    
    if([[cell reuseIdentifier]isEqualToString:@"Expanded2"]){
          dispatch_async(dispatch_get_main_queue(), ^{
              cell.FIOLbl.text = [NSString stringWithFormat:@"ФИО: %@",self->polices[indexPath.row-1][@"insurantShortName"]];
              cell.TSlbl.text =[NSString stringWithFormat:@"ТС: %@",self->polices[indexPath.row-1][@"vehicleModel"]];
              cell.summLbl.text =[NSString stringWithFormat:@"Стоимость оформления: %ld ₽",[self->polices[indexPath.row-1][@"baseCost"]integerValue]+[self->polices[indexPath.row-1][@"surcharge"]integerValue]];
               });
    }
    
    if([[cell reuseIdentifier]isEqualToString:@"Expanded3"]){
          dispatch_async(dispatch_get_main_queue(), ^{
              cell.dateCher.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:self->polices[indexPath.row-1][@"startDate"]]];
              if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==12){
            cell.periodCher.text =@"Период страхования: 1 год";
        }
              if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==3){
            cell.periodCher.text =@"Период страхования: 3 месяца";
        }
              if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==6){
            cell.periodCher.text =@"Период страхования: 6 месяцев";
        }
        cell.sendButton.tag =indexPath.row-1;
              });
    }
    
    if([[cell reuseIdentifier]isEqualToString:@"ExpandedCellIdentifier"]){
         dispatch_async(dispatch_get_main_queue(), ^{
             cell.dateLbl.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:self->polices[indexPath.row-1][@"startDate"]]];
             if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==12){
            cell.period.text =@"Период страхования: 1 год";
        }
             if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==3){
            cell.period.text =@"Период страхования: 3 месяца";
        }
             if([self->polices[indexPath.row-1][@"insurancePeriod"]integerValue]==6){
            cell.period.text =@"Период страхования: 6 месяцев";
        }
              });
    }
    if ([[cell reuseIdentifier] isEqualToString:@"cell"]){
         dispatch_async(dispatch_get_main_queue(), ^{
             cell.numberPolice.text = [NSString stringWithFormat:@"№ %@",[self->polices[theIndexPath.row][@"id"]stringValue]];
             cell.costLbl.text = [NSString stringWithFormat:@"%ld ₽",[self->polices[theIndexPath.row][@"baseCost"]integerValue]+[self->polices[theIndexPath.row][@"surcharge"]integerValue]];
             cell.createDate.text = [self dateFromJson:self->polices[theIndexPath.row][@"createDate"]];
             [ cell setIconFromType:[self->polices[theIndexPath.row][@"state"] intValue] isShort:[self->polices[theIndexPath.row][@"isShort"] boolValue] isPaid:[self->polices[theIndexPath.row][@"paid"] boolValue]];
         });
    }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // disable touch on expanded cell
  
    FeedCell *cell = [self.table cellForRowAtIndexPath:indexPath];
    if ([[cell reuseIdentifier] isEqualToString:@"ExpandedCellIdentifier"]) {
        return;
    }
    if ([[cell reuseIdentifier] isEqualToString:@"Expanded2"]) {
        return;
    }
    if ([[cell reuseIdentifier] isEqualToString:@"Expanded3"]) {
        return;
    }
    if ([[cell reuseIdentifier] isEqualToString:@"Expanded4"]) {
        return;
    }
    if(_searchBar.text.length<1){
    if([polices[indexPath.row][@"state"]integerValue]==4){
        return ;
    }
    }
    // deselect row
    [tableView deselectRowAtIndexPath:indexPath
                             animated:NO];
    
    // get the actual index path
   indexPath = [self actualIndexPathForTappedIndexPath:indexPath];
    
    // save the expanded cell to delete it later
    NSIndexPath *theExpandedIndexPath = expandedIndexPath;
    
    // same row tapped twice - get rid of the expanded cell
    if ([indexPath isEqual:expandingIndexPath]) {
        expandingIndexPath = nil;
        expandedIndexPath = nil;
    }
    // add the expanded cell
    else {
        expandingIndexPath = indexPath;
        expandedIndexPath = [NSIndexPath indexPathForRow:[indexPath row] + 1
                                                    inSection:[indexPath section]];
    }
    
    [tableView beginUpdates];
    
    if (theExpandedIndexPath) {
        [self.table deleteRowsAtIndexPaths:@[theExpandedIndexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
    }
    if (expandedIndexPath) {
        [self.table insertRowsAtIndexPaths:@[expandedIndexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [tableView endUpdates];
    
    // scroll to the expanded cell
    [self.table scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionMiddle
                                     animated:YES];
}
- (NSIndexPath *)actualIndexPathForTappedIndexPath:(NSIndexPath *)indexPath
{
    if (expandedIndexPath && [indexPath row] > [expandedIndexPath row]) {
        return [NSIndexPath indexPathForRow:[indexPath row] - 1
                                  inSection:[indexPath section]];
    }
    
    return indexPath;
}
- (IBAction)sendCell:(UIButton *)sender {
    if(_searchBar.text.length>0){
        [[RXServerConnector sharedConnector]patchRequestFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies/%@/dispatch",searchArray[sender.tag][@"id"]] withBlock:^(NSMutableDictionary *dictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self receiveNotification];
                self->expandingIndexPath = nil;
                self->expandedIndexPath = nil;
            });
            
        } withErrorBlock:^(NSString *error) {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Внимание!"
                                         message:error
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                        }];
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }else{
    [[RXServerConnector sharedConnector]patchRequestFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies/%@/dispatch",polices[sender.tag][@"id"]] withBlock:^(NSMutableDictionary *dictionary) {
        dispatch_async(dispatch_get_main_queue(), ^{
             [self receiveNotification];
            self->expandingIndexPath = nil;
            self->expandedIndexPath = nil;
        });
       
    } withErrorBlock:^(NSString *error) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Внимание!"
                                     message:error
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }];
        
    }
}

-(NSString *)dateForCell:(NSString *)dateText{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    NSDate *date = [dateFormat dateFromString:dateText];
    NSString *dateStri=[dateFormatter stringFromDate:date];
    
    return dateStri;
}
-(NSString *)dateFromJson:(NSString *)dateText{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    NSDate *date = [dateFormat dateFromString:dateText];
    NSString *dateStri=[dateFormatter stringFromDate:date];
    
    return dateStri;
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        if (endScrolling >= scrollView.contentSize.height)
        {
            if(!isLoad){
                [self loadNewPage];
             }
        }
   
}
-(void)loadNewPage{
    if(currentPage<=totalPages&&_searchBar.text.length<1){
        currentPage++;
        isLoad=TRUE;
        [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies?Page=%ld",currentPage] withBlock:^(NSMutableDictionary *dictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
                for (int i = 0; i<[dictionary[@"eosagoPolicies"]count]; i++) {
                    [self->polices addObject:dictionary[@"eosagoPolicies"][i]];
                }
                self->isLoad=FALSE;
                [self->_table reloadData];
            });
            
        }];
    }
   
}
@end
