//
//  CardController.m
//  RX Агент
//
//  Created by RX Group on 10.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CardController.h"
#import "CardIO.h"
#import "CardCell.h"
#import "JGCardLayout.h"
#import "JGStackedLayout.h"
#import "CardsContent.h"
#import "Singleton.h"


@interface CardController ()<CardIOPaymentViewControllerDelegate,ColorPickerDelegate>{
    NSMutableArray *cardArray;
    NSMutableArray <UIView*> *cardViewArr;
    JGStackedLayout *stackedLayout;
    NSInteger currentIndex;
}

@end

@implementation CardController

- (void)viewDidLoad {
    [super viewDidLoad];
    cardArray = [NSMutableArray new];
   
    if( [[NSUserDefaults standardUserDefaults] objectForKey:@"cards"]){
        cardArray = [NSMutableArray arrayWithArray:[[[NSUserDefaults standardUserDefaults] objectForKey:@"cards"]mutableCopy]];
        if(cardArray.count>0){
            [_collectionView setHidden:NO];
            [_collectionView reloadData];
        }
    }
    _picker.delegate=self;
     stackedLayout = (JGStackedLayout *)self.collectionView.collectionViewLayout;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
  
}
- (IBAction)close:(id)sender {
    if ([self.collectionView.collectionViewLayout isKindOfClass:[JGStackedLayout class]]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self->_add_button setImage:[UIImage imageNamed:@"Group 47"] forState:UIControlStateNormal];
        [self.collectionView setCollectionViewLayout:stackedLayout animated:NO];
        [_settingView setHidden:YES];
    }
}
#pragma mark - Добавление карты
- (IBAction)addCard:(id)sender {
    if(_enterType == settingType&&[self.collectionView.collectionViewLayout isKindOfClass:[JGCardLayout class]]&&cardArray.count>0){
        [cardArray removeObjectAtIndex:currentIndex];
        [[NSUserDefaults standardUserDefaults] setObject:cardArray forKey:@"cards"];
        if(cardArray.count>0){
            [_collectionView setHidden:NO];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView setCollectionViewLayout:self->stackedLayout animated:NO];
            });
            [_collectionView reloadData];
        }else{
              [_collectionView reloadData];
              [_collectionView setHidden:YES];
        }
        [self->_add_button setImage:[UIImage imageNamed:@"Group 47"] forState:UIControlStateNormal];
       
      
        [_settingView setHidden:YES];
       
        return;
    }
    if(_enterType == payType&&[self.collectionView.collectionViewLayout isKindOfClass:[JGCardLayout class]]){
        [Singleton sharedInstance].osago.cardDict= cardArray[currentIndex];
        [self dismissViewControllerAnimated:YES completion:nil];
         return;
    }
    
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.navigationBarTintColor = [UIColor colorWithRed:243.f/255.f green:180.f/255.f blue:71.f/255.f alpha:1.f];
    scanViewController.hideCardIOLogo = TRUE;
    scanViewController.collectCardholderName=TRUE;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    UIImage *type;
    NSString *typeCard;
    if(![CardIOCreditCardInfo logoForCardType:info.cardType]&&[[info.cardNumber substringToIndex:1]isEqualToString:@"2"]
       ){
        type =[UIImage imageNamed:@"mir"];
        typeCard=@"3";
    }else{
        type =[CardIOCreditCardInfo logoForCardType:info.cardType];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView setCollectionViewLayout:self->stackedLayout animated:NO];
    });
   
    if(info.cardType==CardIOCreditCardTypeVisa){
        typeCard=@"1";
    }
    if(info.cardType==CardIOCreditCardTypeMastercard){
        typeCard=@"2";
    }
      
    UIColor *color = [self getRandomColor];
    NSArray * arr = [info.cardholderName componentsSeparatedByString:@" "];
   
    NSData *imageType = [NSKeyedArchiver archivedDataWithRootObject:type];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
   
    NSData *cardImage = [NSKeyedArchiver archivedDataWithRootObject:info.cardImage];
 
    NSDictionary *dict = @{@"month":[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryMonth],
                           @"year":[NSString stringWithFormat:@"%lu", (unsigned long)info.expiryYear],
                           @"imageType":imageType,
                           @"cardImage":info.cardImage ? cardImage:@"",
                           @"number":info.cardNumber,
                           @"color":colorData,
                           @"cvv":info.cvv,
                           @"nameHolder": arr[0],
                           @"lastNameHolder":arr[1],
                           @"isFullNumber":[NSNumber numberWithBool:NO],
                           @"isLight":[NSNumber numberWithBool:NO],
                           @"payCardPaymentSystem":typeCard
                           };
   
    [cardArray addObject:dict];
    if(cardArray.count>0){
        [[NSUserDefaults standardUserDefaults] setObject:cardArray forKey:@"cards"];
        [_collectionView setHidden:NO];
    }
    [_collectionView reloadData];
   
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Коллекция карт
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return cardArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
   
    CardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CardCell" forIndexPath:indexPath];
   
    cell.contentView.backgroundColor =[NSKeyedUnarchiver unarchiveObjectWithData:cardArray[indexPath.row][@"color"]];
    cell.backgroundColor = [UIColor clearColor];
    if([cardArray[indexPath.row][@"cardImage"] isKindOfClass:[NSData class]]){
        [cell.photoCard setImage:[NSKeyedUnarchiver unarchiveObjectWithData:cardArray[indexPath.row][@"cardImage"]]];
        [cell.photoCard setHidden:NO];
        [cell.typeCard setHidden:YES];
        cell.isPhoto=TRUE;
    }else{
        [cell.typeCard setImage:[NSKeyedUnarchiver unarchiveObjectWithData:cardArray[indexPath.row][@"imageType"]]];
        [cell.photoCard setHidden:YES];
        [cell.typeCard setHidden:NO];
       
    }
    if([cardArray[indexPath.row][@"isLight"]boolValue]){
        [cell.numberCard setTextColor:[UIColor whiteColor]];
        [cell.dateCard setTextColor:[UIColor whiteColor]];
        [cell.nameCard setTextColor:[UIColor whiteColor]];
    }else{
        [cell.numberCard setTextColor:[UIColor blackColor]];
        [cell.dateCard setTextColor:[UIColor blackColor]];
        [cell.nameCard setTextColor:[UIColor blackColor]];
    }
    if([cardArray[indexPath.row][@"isFullNumber"]boolValue]){
        cell.numberCard.text =[self separateSymbols:cardArray[indexPath.row][@"number"]];
        
    }else{
       cell.numberCard.text =[self formatCardNumber:cardArray[indexPath.row][@"number"]];
    }
    cell.nameCard.text = [[NSString stringWithFormat:@"%@ %@",cardArray[indexPath.row][@"nameHolder"],cardArray[indexPath.row][@"lastNameHolder"]] uppercaseString];
    cell.dateCard.text = [NSString stringWithFormat:@"%@/%@",cardArray[indexPath.row][@"month"],[[NSString stringWithFormat:@"%@", cardArray[indexPath.row][@"year"]] substringFromIndex:[[NSString stringWithFormat:@"%@", cardArray[indexPath.row][@"year"]] length] - 2]];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    currentIndex = indexPath.row;
    dispatch_async(dispatch_get_main_queue(), ^{
        CardCell *datasetCell = [self getCellForIndex:self->currentIndex];
        [self->_picker setHidden:datasetCell.isPhoto];
        [self->_color_switcher setOn:[self->cardArray[indexPath.row][@"isLight"]boolValue]];
        [self->_number_switcher setOn:[self->cardArray [indexPath.row][@"isFullNumber"]boolValue]];
        if ([collectionView.collectionViewLayout isKindOfClass:[JGStackedLayout class]]) {
            JGCardLayout *cardLayout = [[JGCardLayout alloc] initWithItemIndex:indexPath.item];
            [self.collectionView setCollectionViewLayout:cardLayout animated:YES];
            [self->_settingView setHidden:NO];
            if(self->_enterType == settingType){
                [self->_add_button setImage:[UIImage imageNamed:@"delete_card"] forState:UIControlStateNormal];
            }
            
            if(self->_enterType == payType){
                [self->_add_button setImage:[UIImage imageNamed:@"chose_card"] forState:UIControlStateNormal];
            }
        }else{
            [self->_add_button setImage:[UIImage imageNamed:@"Group 47"] forState:UIControlStateNormal];
            [self.collectionView setCollectionViewLayout:self->stackedLayout animated:YES];
            [self->_settingView setHidden:YES];
        }
    });
  
}

#pragma mark - Форматирование текста
-(NSString*)formatCardNumber:(NSString*)text{
    
    for (int i = 0; i < text.length - 4; i++) {
        
        if (![[text substringWithRange:NSMakeRange(i, 1)] isEqual:@" "]) {
            NSRange range = NSMakeRange(i, 1);
            text = [text stringByReplacingCharactersInRange:range withString:@"*"];
        }
        
    }
    
    return [self separateSymbols:text];
}

-(NSString *)separateSymbols:(NSString*)text{
    NSMutableString *resultString = [NSMutableString string];
    
    for(int i = 0; i<[text length]/4; i++)
    {
        NSUInteger fromIndex = i * 4;
        NSUInteger len = [text length] - fromIndex;
        if (len > 4) {
            len = 4;
        }
        
        [resultString appendFormat:@"%@ ",[text substringWithRange:NSMakeRange(fromIndex, len)]];
    }
    return resultString;
}
#pragma mark - Свитчеры
- (IBAction)switch_color:(UISwitch*)sender {
    CardCell *datasetCell = [self getCellForIndex:currentIndex];
    NSMutableDictionary *dict = [cardArray[currentIndex] mutableCopy];
    [dict setObject:[NSNumber numberWithBool:sender.isOn] forKey:@"isLight"];
    [cardArray replaceObjectAtIndex:currentIndex withObject:dict];
    [[NSUserDefaults standardUserDefaults] setObject:cardArray forKey:@"cards"];
  
    if([cardArray[currentIndex][@"isLight"]boolValue]){
        [datasetCell.numberCard setTextColor:[UIColor whiteColor]];
        [datasetCell.dateCard setTextColor:[UIColor whiteColor]];
        [datasetCell.nameCard setTextColor:[UIColor whiteColor]];
    }else{
         [datasetCell.numberCard setTextColor:[UIColor blackColor]];
         [datasetCell.dateCard setTextColor:[UIColor blackColor]];
         [datasetCell.nameCard setTextColor:[UIColor blackColor]];
    }
}

- (IBAction)switch_number:(UISwitch *)sender {
     CardCell *datasetCell = [self getCellForIndex:currentIndex];
     NSMutableDictionary *dict = [cardArray[currentIndex] mutableCopy];
     [dict setObject:[NSNumber numberWithBool:sender.isOn] forKey:@"isFullNumber"];
     [cardArray replaceObjectAtIndex:currentIndex withObject:dict];
     [[NSUserDefaults standardUserDefaults] setObject:cardArray forKey:@"cards"];
    if([cardArray[currentIndex][@"isFullNumber"]boolValue]){
        datasetCell.numberCard.text=[self separateSymbols:cardArray[currentIndex][@"number"]];
    }else{
        datasetCell.numberCard.text=[self formatCardNumber:cardArray[currentIndex][@"number"]];
    }
}

#pragma mark - Работа с цветом
-(void)selectColor:(UIColor *)color{
    NSMutableDictionary *dict = [cardArray[currentIndex] mutableCopy];
    [dict setObject:[NSKeyedArchiver archivedDataWithRootObject:color] forKey:@"color"];
    [cardArray replaceObjectAtIndex:currentIndex withObject:dict];
    [[NSUserDefaults standardUserDefaults] setObject:cardArray forKey:@"cards"];

    CardCell *datasetCell = [self getCellForIndex:currentIndex];
    datasetCell.contentView.backgroundColor = color;
}

-(UIColor*)getRandomColor{
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    return color;
}

-(CardCell*)getCellForIndex:(NSInteger)index{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    CardCell *datasetCell =[_collectionView cellForItemAtIndexPath:indexPath];
    return datasetCell;
}
@end
