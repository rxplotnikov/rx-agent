//
//  CameraViewController.h
//  RXAgent
//
//  Created by RX Group on 04.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBCameraButton.h"

@interface CameraViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *CameraView;
@property (weak, nonatomic) IBOutlet UIView *captureView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet DBCameraButton *cameraBtn;
@property (weak, nonatomic) IBOutlet UIView *PreView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property (weak, nonatomic) IBOutlet UIImageView *cameraAccess;
@property BOOL isVehicle;

@end
