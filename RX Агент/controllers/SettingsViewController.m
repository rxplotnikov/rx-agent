//
//  SettingsViewController.m
//  RX Агент
//
//  Created by RX Group on 28.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "SettingsViewController.h"
#import "CardController.h"
#import "LoginController.h"

@interface SettingsViewController (){
    NSMutableArray *tableItems;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    tableItems = [NSMutableArray arrayWithArray:@[@"Карты"]];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   [_headerLbl setFont:[UIFont fontWithName:@"Helvetica Light" size:24.f]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableItems count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //Поиск ячейки
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Если ячейка не найдена
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier] ;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell.imageView setImage:[UIImage imageNamed:@"icon4"]];
    cell.textLabel.text = [tableItems objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
    cell.textLabel.numberOfLines=2;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CardController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Card"];
    vc.enterType =0;
    [self presentViewController:vc animated:NO completion:nil];
}
- (IBAction)exitButtonAction:(id)sender {
   
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Внимание!"
                                                                   message:@"Вы действительно хотите выйти из аккаунта?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                          
//                                                              UIWebView *webview=[[UIWebView alloc]initWithFrame:self.view.frame];
//                                                              NSString *url=@"https://demo-accounts.rx-agent.ru/logout";
//                                                              NSURL *nsurl=[NSURL URLWithString:url];
//                                                              NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
//                                                              [webview loadRequest:nsrequest];
//                                                              [self.view addSubview:webview];
//                                                              [webview removeFromSuperview];
//                                                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://demo-accounts.rx-agent.ru/logout"]];
                                                           //   [self openSafari];
                                                              [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"token"];


                                                              UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                              LoginController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"auth"];
                                                              [[UIApplication sharedApplication].keyWindow setRootViewController:vc];
                                                          }];
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"Нет" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:defaultAction];
    [alert addAction:noAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
