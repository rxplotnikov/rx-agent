//
//  mainCell.h
//  RX Агент
//
//  Created by RX Group on 27.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol kbmDelegate <NSObject>

- (void)switcherAction:(BOOL)isOn;
@end

@interface mainCell : UITableViewCell
@property(weak, nonatomic) id<kbmDelegate>delegate;
@property UITextField *textfield;
@property UISwitch *switcher;
-(void)setCellText:(NSString*)text
    andPlaceholder:(NSString*)placeholder
      andTextfield:(BOOL)tfNeed
       andSwitcher:(BOOL)swNeed;
@end
