//
//  CalcViewController.m
//  RX Агент
//
//  Created by RX Group on 14.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CalcViewController.h"
#import "OsagoViewController.h"
#import "Singleton.h"
#import "RXServerConnector.h"

@interface CalcViewController (){
 
    NSMutableArray<UIView *> *containerArray;
    
}

@end

@implementation CalcViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [_headerLbl setFont:[UIFont fontWithName:@"Helvetica Light" size:24.f]];
     [Singleton sharedInstance].indexContain=1;
    _countLbl.text =[NSString stringWithFormat:@"Шаг %d/3",[Singleton sharedInstance].indexContain];
    containerArray = [NSMutableArray arrayWithArray:@[_container1,_container2,_container3]];
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(hideKeyboard)];
    tapRecognizer.cancelsTouchesInView = NO;
    [[self.view window]addGestureRecognizer:tapRecognizer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"HideButt"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableButt:)
                                                 name:@"enableBtn"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeWindow)
                                                 name:@"closeCalc"
                                               object:nil];
}
-(void)closeWindow{
     [self dismissViewControllerAnimated:NO completion:nil];
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    NSString *key = @"hide";
    NSDictionary *dictionary = [notification userInfo];
    BOOL boolValue;
    if([dictionary valueForKey:key]){
        boolValue =[[dictionary valueForKey:key] boolValue];
        [_nextBtn setHidden:boolValue];
    }
}

-(void)hideKeyboard{
    [self.view endEditing:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)enableButt:(NSNotification *)notification{
    _nextBtn.enabled=[notification.userInfo[@"hide"]boolValue];
}

- (IBAction)back:(id)sender {
    if(_nextBtn.isHidden){
         [[NSNotificationCenter defaultCenter] postNotificationName:@"hideTable" object:nil userInfo:nil];
    }else{
        if([Singleton sharedInstance].indexContain==1){
        CATransition *transition = [[CATransition alloc] init];
        transition.duration =0.5;
        transition.type = kCATransitionMoveIn;
        transition.subtype = kCATransitionFromLeft;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self dismissViewControllerAnimated:NO completion:nil];
            [[Singleton sharedInstance]refreshAll];
        } else
            if ([Singleton sharedInstance].indexContain>1){
            [Singleton sharedInstance].indexContain--;
             _countLbl.text =[NSString stringWithFormat:@"Шаг %d/3",[Singleton sharedInstance].indexContain];
                [self hiddenContainerMethod:[Singleton sharedInstance].indexContain];
        }
    }
}

- (IBAction)nextStep:(id)sender {
    if([Singleton sharedInstance].indexContain==1){
         [[NSNotificationCenter defaultCenter] postNotificationName:@"checkLimit" object:nil userInfo:nil];
    }
    
    if([Singleton sharedInstance].indexContain<=2){
    [Singleton sharedInstance].indexContain++;
      [self hiddenContainerMethod:[Singleton sharedInstance].indexContain];
    _countLbl.text =[NSString stringWithFormat:@"Шаг %d/3",[Singleton sharedInstance].indexContain];
        NSLog(@"%d",[Singleton sharedInstance].indexContain);
    }else{
       
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        OsagoViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"osago"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}
-(void)hiddenContainerMethod:(int)index{
    if(index==1){
        [_nextBtn setTitle:@"Далее" forState:UIControlStateNormal];
    }
    if(index==2){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"checkUpdate" object:nil userInfo:nil];
        [_nextBtn setTitle:@"Далее" forState:UIControlStateNormal];
    }
    if(index==3){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateView" object:nil userInfo:nil];
        [_nextBtn setTitle:@"Оформить" forState:UIControlStateNormal];
    }
    for (int i=0; i<containerArray.count; i++) {
        if(i==index-1){
            [containerArray[i]setHidden:NO];
        }else{
            [containerArray[i] setHidden:YES];
        }
    }
}

@end
