//
//  KBMViewController.h
//  RX Агент
//
//  Created by RX Group on 22.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBMViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *table;
@property NSInteger currentKBM;
@end
