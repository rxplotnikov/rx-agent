//
//  DetailController.h
//  RX Агент
//
//  Created by RX Group on 23.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
typedef enum:NSInteger{
    //1 экран
    RegionType=0,
    OwnerType=1,
    VehicleType=2,
    TargetType=3,
    MassType = 4,
    EasyTargetType = 5,
    CountType = 6,
    //2 экран
    TermType = 7,
    ExpType = 8,
    IdentType = 9,
    //заявка
    allTargets=10,
    
    ageType = 11
} TypeData;

@interface DetailController : UIViewController
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property NSArray *contentArray;
@property NSArray *secondContentArray;
@property TypeData dataType;
@property BOOL isRegion;
@end
