//
//  Controller2.h
//  RX Агент
//
//  Created by RX Group on 14.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundViewChose.h"
#import "Singleton.h"


@interface Controller2 : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollConst;
@property (weak, nonatomic) IBOutlet RoundViewChose *roundView;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@end
