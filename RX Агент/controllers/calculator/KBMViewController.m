//
//  KBMViewController.m
//  RX Агент
//
//  Created by RX Group on 22.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "KBMViewController.h"
#import "Singleton.h"
#import "RXServerConnector.h"
#import "KBMFormController.h"


@interface KBMViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *KBMarray;
}

@end

@implementation KBMViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeView)
                                                 name:@"closeView"
                                               object:nil];

    if(!KBMarray){
        [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/KBMItems"
                                                 withBlock:^(NSMutableDictionary *dictionary) {
                                                     self->KBMarray = [NSMutableArray new];
                                                     for (int i = 0; i<[dictionary[@"kbmItems"]count]; i++) {
                                                         [self->KBMarray addObject:dictionary[@"kbmItems"][i][@"value"]];
                                                     }
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [self->_table reloadData];
                                                     });
                                                 }];
    }

}
-(void)closeView{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return KBMarray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //Поиск ячейки
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Если ячейка не найдена
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:CellIdentifier] ;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [NSString stringWithFormat:@"%g",[KBMarray[indexPath.row] floatValue]];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
    cell.textLabel.numberOfLines=2;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_currentKBM==0){
        [Singleton sharedInstance].model.KBM1 = [NSString stringWithFormat:@"%g",[KBMarray[indexPath.row] floatValue]];
    }
    if(_currentKBM==1){
        [Singleton sharedInstance].model.KBM2 = [NSString stringWithFormat:@"%g",[KBMarray[indexPath.row] floatValue]];
    }
    if(_currentKBM==2){
        [Singleton sharedInstance].model.KBM3 = [NSString stringWithFormat:@"%g",[KBMarray[indexPath.row] floatValue]];
    }
    if(_currentKBM==3){
        [Singleton sharedInstance].model.KBM4 = [NSString stringWithFormat:@"%g",[KBMarray[indexPath.row] floatValue]];
    }
    if(_currentKBM==5){
        [Singleton sharedInstance].model.ownerKBM = [NSString stringWithFormat:@"%g",[KBMarray[indexPath.row] floatValue]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)close:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)KBMform:(id)sender {
    [self performSegueWithIdentifier:@"driverKBM" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"driverKBM"])
    {
        KBMFormController *KBMFormVC = [segue destinationViewController];
        KBMFormVC.index = _currentKBM;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
