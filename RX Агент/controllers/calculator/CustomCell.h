//
//  CustomCell.h
//  RX Агент
//
//  Created by RX Group on 26.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol JSONDelegate <NSObject>

- (void)switcherAction:(BOOL)isOn;
@end


@interface CustomCell : UITableViewCell
@property(weak, nonatomic) id<JSONDelegate>delegate;
@property UITextField *textfield;
@property UISwitch *switcher;
-(void)setCellText:(NSString*)text
        andSubtext:(NSString *)subtext
    andPlaceholder:(NSString *)placeholder
       andSwitcher:(BOOL)sWneed
      andTextfield:(BOOL)tFneed
   andCurrentIdent:(NSString*)current;
@end
