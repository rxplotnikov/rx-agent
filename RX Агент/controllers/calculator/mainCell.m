//
//  mainCell.m
//  RX Агент
//
//  Created by RX Group on 27.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "mainCell.h"
#import "Singleton.h"

@implementation mainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellText:(NSString*)text
    andPlaceholder:(NSString*)placeholder
      andTextfield:(BOOL)tfNeed
       andSwitcher:(BOOL)swNeed{
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    self.textLabel.numberOfLines=2;
    if(placeholder){
        [self.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
         [self.textLabel setTextColor:[UIColor lightGrayColor]];
        self.textLabel.text = placeholder;
    }else{
        [self.textLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:18.f]];
        [self.textLabel setTextColor:[UIColor blackColor]];
         self.textLabel.text = text;
    }
   
    if(tfNeed){
        self.textLabel.text = @"";
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    if(swNeed){
        self.accessoryType = UITableViewCellAccessoryNone;
    }
  
    if(tfNeed){
        if(!_textfield){
            _textfield = [[UITextField alloc] initWithFrame:CGRectMake(12, 0, self.frame.size.width - self.textLabel.frame.origin.x, self.frame.size.height)];
            _textfield.adjustsFontSizeToFitWidth = YES;
            [_textfield setFont:[UIFont fontWithName:@"Helvetica Ligth" size:18.f]];
            [_textfield setTextColor:[UIColor blackColor]];
            _textfield.placeholder = text;
            _textfield.keyboardType = UIKeyboardTypeNumberPad;
            _textfield.returnKeyType = UIReturnKeyDone;
            _textfield.backgroundColor = [UIColor clearColor];
            _textfield.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
            _textfield.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
            _textfield.tag = 0;
            _textfield.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
            [_textfield setEnabled:YES];
            [self.contentView addSubview:_textfield];
           
        }else{
            if(text!=_textfield.placeholder){
                [_textfield setFont:[UIFont fontWithName:@"Helvetica Bold" size:18.f]];
                [_textfield setTextColor:[UIColor blackColor]];
                _textfield.text = text;
            }
        }
    }
    if(swNeed){
        if(!_switcher){
            _switcher = [[UISwitch alloc] init];
            [_switcher setOn:![Singleton sharedInstance].isLimit];
            CGSize switchSize = [_switcher sizeThatFits:CGSizeZero];
            _switcher.frame = CGRectMake(self.contentView.bounds.size.width - switchSize.width - 5.0f,
                                         (self.contentView.bounds.size.height - switchSize.height) / 2.0f,
                                         switchSize.width,
                                         switchSize.height);
            _switcher.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            [_switcher addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
            [self.contentView addSubview:_switcher];
        }else{
              [_switcher removeFromSuperview];
            _switcher = [[UISwitch alloc] init];
            [_switcher setOn:![Singleton sharedInstance].isLimit];
            CGSize switchSize = [_switcher sizeThatFits:CGSizeZero];
            _switcher.frame = CGRectMake(self.contentView.bounds.size.width - switchSize.width - 5.0f,
                                         (self.contentView.bounds.size.height - switchSize.height) / 2.0f,
                                         switchSize.width,
                                         switchSize.height);
            _switcher.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            [_switcher addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
            [self.contentView addSubview:_switcher];
        }
    }else{
        if(_switcher){
            [_switcher removeFromSuperview];
        }
    }
  
}
-(void)switchChanged:(UISwitch *)switcher{
    [_delegate switcherAction:switcher.isOn];
}
@end
