//
//  DetailController.m
//  RX Агент
//
//  Created by RX Group on 23.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "DetailController.h"

@interface DetailController ()<UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>{
    NSMutableArray *ownerArray;
}

@end

@implementation DetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    ownerArray = [NSMutableArray arrayWithArray:@[@"Физическое лицо",@"Юридическое лицо"]];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(_dataType == RegionType){
        if (!_searchController.searchBar.superview) {
            self.table.tableHeaderView = self.searchController.searchBar;
        }
    }
}

- (UISearchController *)searchController {
    if (!_searchController) {
        _searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
        
        _searchController.searchBar.placeholder =@"Укажите регион или город";
        _searchController.searchResultsUpdater = self;
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.searchBar.delegate = self;
        [_searchController.searchBar sizeToFit];
        _searchController.searchBar.showsCancelButton=NO;
    }
    return _searchController;
}
- (NSArray *)searchResults {
        NSString *searchString = self.searchController.searchBar.text;
        if (searchString.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@", searchString];
            return [_contentArray filteredArrayUsingPredicate:predicate];
        }
        else {
            return _contentArray;
        }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self.table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)close:(id)sender {
    if(_dataType == RegionType){
        if(!_isRegion){
            [Singleton sharedInstance].model.region=nil;
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if(_dataType == OwnerType){
            return [ownerArray count];
        }
    
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        return self.searchResults.count;
    }
   
        return _contentArray.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier] ;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     if (self.searchController.isActive && (![self.searchController.searchBar.text isEqualToString:@""])) {
        cell.textLabel.text  = self.searchResults[indexPath.row][@"title"];
     }else{
         cell.textLabel.text  = _contentArray[indexPath.row][@"title"];
     }
    if(_dataType == VehicleType){
        cell.textLabel.text =[NSString stringWithFormat:@"%@ - %@",_contentArray[indexPath.row][@"title"],_contentArray[indexPath.row][@"shortDescription"]];
    }
    if(_dataType == OwnerType){
        cell.textLabel.text = ownerArray[indexPath.row];
    }
  
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
    cell.textLabel.numberOfLines=2;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if(_dataType == RegionType){
        if(_isRegion){
                    if (self.searchController.isActive && (![self.searchController.searchBar.text isEqualToString:@""]))
                    {
                        NSPredicate *filter = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"region = %@",self.searchResults[indexPath.row][@"id"]]];
                           [Singleton sharedInstance].model.region = self.searchResults[indexPath.row][@"title"];
                            [Singleton sharedInstance].model.regionJSON = self.searchResults[indexPath.row][@"id"];
                     
                        _contentArray = [_secondContentArray filteredArrayUsingPredicate:filter];
                        if(_contentArray.count>0){
                            NSMutableArray *arr = [NSMutableArray arrayWithArray:_contentArray];
                            NSDictionary *dict= @{ @"id": @"",
                                                   @"region": @"",
                                                   @"title": @"Другие города и населенные пункты"
                                                   };
                            [arr addObject:dict];
                            _contentArray = [arr mutableCopy];
                            [tableView reloadData];
                            self.searchController.searchBar.text=@"";
                            self.searchController.active = NO;
                        }else{
                            [Singleton sharedInstance].model.city=nil;
                             [Singleton sharedInstance].model.cityJSON = nil;
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }
                    }else{
                        NSPredicate *filter = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"region = %@",_contentArray[indexPath.row][@"id"]]];
                        [Singleton sharedInstance].model.region = _contentArray[indexPath.row][@"title"];
                        [Singleton sharedInstance].model.regionJSON = _contentArray[indexPath.row][@"id"];
                        _contentArray = [_secondContentArray filteredArrayUsingPredicate:filter];
                        if(_contentArray.count>0){
                            NSMutableArray *arr = [NSMutableArray arrayWithArray:_contentArray];
                            NSDictionary *dict= @{ @"id": @"",
                                                   @"region": @"",
                                                   @"title": @"Другие города и населенные пункты"
                                                   };
                            [arr addObject:dict];
                            _contentArray = [arr mutableCopy];
                            [tableView reloadData];
                        }else{
                            [Singleton sharedInstance].model.city=nil;
                             [Singleton sharedInstance].model.cityJSON =nil;
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }
                        
                    }
                _isRegion = FALSE;
            }else{
                if (self.searchController.isActive && (![self.searchController.searchBar.text isEqualToString:@""]))
                {
                    [Singleton sharedInstance].model.city = self.searchResults[indexPath.row][@"title"];
                    if([_contentArray[indexPath.row][@"id"] isKindOfClass:[NSNumber class]]){
                        [Singleton sharedInstance].model.cityJSON =[_contentArray[indexPath.row][@"id"]stringValue];
                        
                    }else{
                        [Singleton sharedInstance].model.cityJSON =_contentArray[indexPath.row][@"id"];
                    }
                        
            //     [self isEmpty:]?: [_contentArray[indexPath.row][@"id"] stringValue];
                    self.searchController.searchBar.text=@"";
                    self.searchController.active = NO;
                    [self dismissViewControllerAnimated:YES completion:nil];
                }else{
                    [Singleton sharedInstance].model.city = _contentArray[indexPath.row][@"title"];
                    if([_contentArray[indexPath.row][@"id"] isKindOfClass:[NSNumber class]]){
                         [Singleton sharedInstance].model.cityJSON =[_contentArray[indexPath.row][@"id"]stringValue];
                       
                    }else{
                        [Singleton sharedInstance].model.cityJSON =_contentArray[indexPath.row][@"id"];
                    }
                   //  [Singleton sharedInstance].model.cityJSON = [self isEmpty:_contentArray[indexPath.row][@"id"]]?_contentArray[indexPath.row][@"id"]: [_contentArray[indexPath.row][@"id"] stringValue];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
            }
    }
    if(_dataType == ageType){
        [Singleton sharedInstance].model.minAge = _contentArray [indexPath.row][@"title"];
        [Singleton sharedInstance].model.maxAge22 = _contentArray [indexPath.row][@"id"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if(_dataType==allTargets){
        [Singleton sharedInstance].osago.vehicleTarget=_contentArray[indexPath.row][@"title"];
        [Singleton sharedInstance].osago.vehicleTargetJSON = [_contentArray[indexPath.row][@"id"]stringValue];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if(_dataType == IdentType){
        [Singleton sharedInstance].model.ident = _contentArray[indexPath.row][@"title"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if(_dataType == CountType){
        [Singleton sharedInstance].model.countSit = _contentArray[indexPath.row][@"title"];
         [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if(_dataType == EasyTargetType){
        [Singleton sharedInstance].model.targetJSON = [_contentArray[indexPath.row][@"id"]stringValue];
        [Singleton sharedInstance].model.easyTarget = _contentArray[indexPath.row][@"title"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if(_dataType == VehicleType){
        [Singleton sharedInstance].model.categoryID = indexPath.row+1;
        [Singleton sharedInstance].osago.category=_contentArray[indexPath.row][@"title"];
        [Singleton sharedInstance].model.vehicle =[NSString stringWithFormat:@"%@ - %@",_contentArray[indexPath.row][@"title"],_contentArray[indexPath.row][@"shortDescription"]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if(_dataType == TermType){
        [Singleton sharedInstance].model.term = _contentArray[indexPath.row][@"title"];
        [Singleton sharedInstance].model.periodJSON = [_contentArray[indexPath.row][@"id"] stringValue];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if(_dataType == OwnerType){
        if(indexPath.row==0){
         
            [Singleton sharedInstance].model.owner =ownerArray[indexPath.row];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
          
            [self showMessage];
        }
    }
    if(_dataType == ExpType){
        [Singleton sharedInstance].model.minExp = _contentArray [indexPath.row][@"title"];
        [Singleton sharedInstance].model.maxExperience3 = _contentArray [indexPath.row][@"id"];
         [self dismissViewControllerAnimated:YES completion:nil];
    }
    if(_dataType == TargetType){
        [Singleton sharedInstance].model.targetJSON = [_contentArray[indexPath.row][@"id"]stringValue];
        [Singleton sharedInstance].model.target = _contentArray[indexPath.row][@"shortTitle"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if(_dataType == MassType){
        [Singleton sharedInstance].model.mass = _contentArray[indexPath.row][@"title"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}
-(void)showMessage{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Внимание!"
                                                                   message:@"Работа с юридическими лицами временно приостановлена."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
