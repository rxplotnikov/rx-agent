//
//  CalcModel.h
//  RX Агент
//
//  Created by RX Group on 23.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalcModel : NSObject
@property NSString *region;
@property NSString *regionJSON;
@property NSString *city;
@property NSString *cityJSON;
@property NSString *vehicle;
@property NSString *owner;
@property NSInteger categoryID;
@property NSString *power;
@property NSString *target;
@property NSString *mass;
@property NSString *easyTarget;
@property NSString *countSit;
@property NSString *targetJSON;
@property BOOL trailJSON;

@property NSString *term;
@property NSString *periodJSON;
@property NSString *startDate;
@property NSString *startDateJSON;

@property NSString *minAge;
@property NSString *maxAge22;
@property NSString *minExp;
@property NSString *maxExperience3;

@property NSInteger countDriver;
@property BOOL allKBM;
@property NSString *ownerKBM;
@property NSString *KBM1;
@property NSString *KBM2;
@property NSString *KBM3;
@property NSString *KBM4;

@property NSString *ident;
@end
