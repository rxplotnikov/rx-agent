
//
//  Controller1.m
//  RX Агент
//
//  Created by RX Group on 14.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "Controller1.h"
#import "Singleton.h"
#import "RXServerConnector.h"
#import "DetailController.h"
#import "RXLoadingView.h"


@interface Controller1 ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,JSONDelegate>{
    NSMutableArray *segmentArray;
    NSMutableArray *placeholderArray;
    NSMutableArray *typeArray;
    
    NSDictionary *maxMass;
    NSDictionary *dictSits;
    NSMutableDictionary *dictRegion;
    NSMutableDictionary *dictCities;
    NSMutableDictionary *dictCar;
    NSMutableArray *dictTargets;
    
    NSDictionary *optionalDict;
    UISwitch* switcher;
    UITextField *playerTextField;
    
    NSInteger loading;
    
    RXLoadingView *loadingView;
}

@end

@implementation Controller1

- (void)viewDidLoad {
    [super viewDidLoad];
    loadingView = [[RXLoadingView alloc]initWithText:@"Загрузка..."];
    [loadingView showView];
    maxMass = [NSDictionary dictionary];
    maxMass = @{@"maxMass":@[
                        @{@"title":@"16 тонн и менее"},
                        @{@"title":@"Более 16 тонн"}
                        ]};
    dictSits = [NSDictionary dictionary];
    dictSits = @{@"sits":@[
                        @{@"title":@"Более 16"},
                        @{@"title":@"До 16 включительно"}
                        ]};
  
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"hideTable"
                                               object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
}
-(void)chooseProperties:(NSString *)string{
                            if([string isEqualToString:@"Регулярные пассажирские перевозки"]){
                                    optionalDict=[NSDictionary dictionary];
                                    optionalDict= @{@"categories":@[
                                                            @[@{//A
                                                                  @"Name": @"Используется с прицепом",
                                                                  @"Placeholder": @"Прицеп",
                                                                  @"Type": @"switchA"
                                                                  }],@[//B
                                                                @{
                                                                    @"Name": @"Мощность",
                                                                    @"Placeholder": @"Мощность, л.с.",
                                                                    @"Type": @"textfield"
                                                                    },@{
                                                                    @"Name": @"Цель использования",
                                                                    @"Placeholder": @"Укажите цель использования",
                                                                    @"Type": @"tableTargetEasy"
                                                                    }
                                                                ],@[//C
                                                                @{
                                                                    @"Name": @"Используется с прицепом",
                                                                    @"Placeholder": @"Прицеп",
                                                                    @"Type": @"switchC"
                                                                    },
                                                                @{
                                                                    @"Name": @"Разрешенная максимальная масса",
                                                                    @"Placeholder": @"Укажите максимальную массу",
                                                                    @"Type": @"tableMass"
                                                                    }],@[//D
                                                                @{
                                                                    @"Name": @"Цель использования",
                                                                    @"Placeholder": @"Укажите цель использования",
                                                                    @"Type": @"tableTarget"
                                                                    }],@[//E
                                                                @{
                                                                    @"Name": @"Используется с прицепом",
                                                                    @"Placeholder": @"Прицеп",
                                                                    @"Type": @"switchE"
                                                                    }]
                                                            ]};
                                }else{
                                    optionalDict=[NSDictionary dictionary];
                                    optionalDict= @{@"categories":@[
                                                            @[@{//A
                                                                  @"Name": @"Используется с прицепом",
                                                                  @"Placeholder": @"Прицеп",
                                                                  @"Type": @"switchA"
                                                                  }],@[//B
                                                                @{
                                                                    @"Name": @"Мощность",
                                                                    @"Placeholder": @"Мощность, л.с.",
                                                                    @"Type": @"textfield"
                                                                    },@{
                                                                    @"Name": @"Цель использования",
                                                                    @"Placeholder": @"Укажите цель использования",
                                                                    @"Type": @"tableTargetEasy"
                                                                    }
                                                                ],@[//C
                                                                @{
                                                                    @"Name": @"Используется с прицепом",
                                                                    @"Placeholder": @"Прицеп",
                                                                    @"Type": @"switchC"
                                                                    },
                                                                @{
                                                                    @"Name": @"Разрешенная максимальная масса",
                                                                    @"Placeholder": @"Укажите максимальную массу",
                                                                    @"Type": @"tableMass"
                                                                    }],@[//D
                                                                @{
                                                                    @"Name": @"Цель использования",
                                                                    @"Placeholder": @"Укажите цель использования",
                                                                    @"Type": @"tableTarget"
                                                                    },
                                                                @{
                                                                    @"Name": @"Количество мест",
                                                                    @"Placeholder": @"Укажите количество мест",
                                                                    @"Type": @"tableCount"
                                                                    }],@[//E
                                                                @{
                                                                    @"Name": @"Используется с прицепом",
                                                                    @"Placeholder": @"Прицеп",
                                                                    @"Type": @"switchE"
                                                                    }]
                                                            ]};
                                }
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches
              withEvent:event];
    [self hideKeyboard];
}
-(void)hideKeyboard{
      [self checkAllField];
      [self.view endEditing:YES];
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    [self hidePicker];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([Singleton sharedInstance].indexContain==1){
        [self chooseProperties:[Singleton sharedInstance].model.target];
        segmentArray = [NSMutableArray arrayWithArray:@[@"Регион",@"Владелец",@"Категория"]];
        placeholderArray = [NSMutableArray arrayWithArray:@[@"Укажите регион",@"Укажите владельца ТС",@"Укажите категорию ТС"]];
        typeArray = [NSMutableArray arrayWithArray:@[@"tableRegion",@"tableOwner",@"tableVehicle"]];
        if(!dictTargets){
            [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/VehicleTargets"
                                                     withBlock:^(NSMutableDictionary *dictionary) {
                                                          self->dictTargets = [NSMutableArray new];
                                                         for (int i = 0; i<[dictionary[@"vehicleTargets"]count]; i++) {
                                                             if([dictionary[@"vehicleTargets"][i][@"eCategories"]count]>0){
                                                                 [self->dictTargets addObject:dictionary[@"vehicleTargets"][i]];
                                                             }
                                                         }
                                                         [self checkLoad];
                                                     }];
        }
        if(!dictRegion){
            [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/Regions"
                                                     withBlock:^(NSMutableDictionary *dictionary) {
                                                         self->dictRegion=dictionary;
                                                         [self checkLoad];
                                                     }];
        }
        if(!dictCities){
            [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/EOSAGOCities"
                                                     withBlock:^(NSMutableDictionary *dictionary) {
                                                         self->dictCities=dictionary;
                                                         [self checkLoad];
                                                     }];
        }
        if(!dictCar){
            [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/VehicleCategories"
                                                     withBlock:^(NSMutableDictionary *dictionary) {
                                                         self->dictCar=dictionary;
                                                         [self checkLoad];
                                                     }];
        }else{
            if([Singleton sharedInstance].model.vehicle){
                NSInteger index = [Singleton sharedInstance].model.categoryID-1;
                for (int i=0; i<[optionalDict[@"categories"][index]count]; i++) {
                    [segmentArray addObject:optionalDict[@"categories"][index][i][@"Name"]];
                    [placeholderArray addObject:optionalDict[@"categories"][index][i][@"Placeholder"]];
                    [typeArray addObject:optionalDict[@"categories"][index][i][@"Type"]];
                }
            }
            [_table reloadData];
        }
        [self checkAllField];
    }
}
-(void)checkAllField{
    if([Singleton sharedInstance].model.region.length>0&&[Singleton sharedInstance].model.owner.length>0&&[Singleton sharedInstance].model.categoryID!=0){
        if([Singleton sharedInstance].model.categoryID==1){
            [self sendTrueMessage];
        }
            if([Singleton sharedInstance].model.categoryID==2){
                if([Singleton sharedInstance].model.power.length>0&&[Singleton sharedInstance].model.easyTarget.length>0){
                    [self sendTrueMessage];
                }else{
                    [self sendFalseMessage];
                }
            }
        
        if([Singleton sharedInstance].model.categoryID==3){
            if([Singleton sharedInstance].model.mass.length>0){
                [self sendTrueMessage];
            }else{
                [self sendFalseMessage];
            }
        }
        
        if([Singleton sharedInstance].model.categoryID==4){
            if([[Singleton sharedInstance].model.target isEqualToString:@"Регулярные пассажирские перевозки"])
            {
                [self sendTrueMessage];
        }else{
            if([Singleton sharedInstance].model.target.length>0 &&[Singleton sharedInstance].model.countSit.length>0){
                [self sendTrueMessage];
            }else{
                [self sendFalseMessage];
            }
        }
        }
        if([Singleton sharedInstance].model.categoryID==5){
           [self sendTrueMessage];
        }
    }
}
-(void)sendTrueMessage{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"hide"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
}
-(void)sendFalseMessage{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:@"hide"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
}
-(void)checkLoad{
    loading++;
    if(loading==4){
        [loadingView dissmissView];
    }
}

-(void)hidePicker{
//    [_viewPicker setHidden:!_viewPicker.isHidden];
//    [_table reloadData];
//
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"HideButt" object:nil userInfo:dictionary];
//
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    return segmentArray[section];
//}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [headerView setBackgroundColor:[UIColor colorWithRed:245.f/255.f green:245.f/255.f blue:245.f/255.f alpha:1.f]];
    
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,5,self.table.frame.size.width-20,20.0)];
    [customLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
    [customLabel setTextColor:[UIColor colorWithRed:72.f/255.f green:72.f/255.f blue:72.f/255.f alpha:1.f]];
    customLabel.text = segmentArray[section];
    [headerView addSubview:customLabel];
    return headerView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return segmentArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
      
    if([typeArray[indexPath.section] isEqualToString:@"tableRegion"]){
     
        if([Singleton sharedInstance].model.region){
            if([Singleton sharedInstance].model.city){
                [cell setCellText:[Singleton sharedInstance].model.region andSubtext:[Singleton sharedInstance].model.city andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
            }else{
                [cell setCellText:[Singleton sharedInstance].model.region andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
            }
        }else{
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        }
    }
    if([typeArray[indexPath.section] isEqualToString:@"tableOwner"]){
        if([Singleton sharedInstance].model.owner)
            [cell setCellText:[Singleton sharedInstance].model.owner andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        else
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
    }
    if([typeArray[indexPath.section] isEqualToString:@"tableVehicle"]){
        if([Singleton sharedInstance].model.vehicle)
            [cell setCellText:[Singleton sharedInstance].model.vehicle andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        else
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
    }
    if([typeArray[indexPath.section] isEqualToString:@"switchA"]){
        
        [cell setCellText:placeholderArray[indexPath.section] andSubtext:nil andPlaceholder:nil andSwitcher:YES andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
         [Singleton sharedInstance].model.trailJSON=cell.switcher.isOn;
        cell.delegate=self;
    }
    
    if([typeArray[indexPath.section] isEqualToString:@"textfield"]){
        if([Singleton sharedInstance].model.power){
            [cell setCellText:[Singleton sharedInstance].model.power andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:YES andCurrentIdent:typeArray[indexPath.section]];
        }else{
            [cell setCellText:nil andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:YES andCurrentIdent:typeArray[indexPath.section]];
        }
        cell.textfield.delegate=self;
    }
    
    if([typeArray[indexPath.section] isEqualToString:@"tableTargetEasy"]){
        if([Singleton sharedInstance].model.easyTarget){
            [cell setCellText:[Singleton sharedInstance].model.easyTarget andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        }else{
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        }
    }
    if([typeArray[indexPath.section] isEqualToString:@"switchC"]){
        
        [cell setCellText:placeholderArray[indexPath.section] andSubtext:nil andPlaceholder:nil andSwitcher:YES andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
         [Singleton sharedInstance].model.trailJSON=cell.switcher.isOn;
         cell.delegate=self;
    }
    
    if([typeArray[indexPath.section] isEqualToString:@"tableMass"]){
        if([Singleton sharedInstance].model.mass)
            [cell setCellText:[Singleton sharedInstance].model.mass andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        else
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
    }
   
    
    
    if([typeArray[indexPath.section] isEqualToString:@"tableTarget"]){
        if([Singleton sharedInstance].model.target)
            [cell setCellText:[Singleton sharedInstance].model.target andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        else
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
    }
    
   
    if([typeArray[indexPath.section] isEqualToString:@"tableCount"]){
        if([Singleton sharedInstance].model.countSit)
            [cell setCellText:[Singleton sharedInstance].model.countSit andSubtext:nil andPlaceholder:nil andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        else
            [cell setCellText:nil andSubtext:nil andPlaceholder:placeholderArray[indexPath.section] andSwitcher:NO andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
    }
    
    if([typeArray[indexPath.section] isEqualToString:@"switchE"]){
        
        [cell setCellText:placeholderArray[indexPath.section] andSubtext:nil andPlaceholder:nil andSwitcher:YES andTextfield:NO andCurrentIdent:typeArray[indexPath.section]];
        [Singleton sharedInstance].model.trailJSON=cell.switcher.isOn;
         cell.delegate=self;
    }

    return cell;
}
-(void)switcherAction:(BOOL)isOn{
    [Singleton sharedInstance].model.trailJSON = isOn;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   
if(textField.text.length + (string.length - range.length) <= 3)
    [Singleton sharedInstance].model.power=[textField.text stringByReplacingCharactersInRange:range withString:string];
   
    return textField.text.length + (string.length - range.length) <= 3;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //I set the segue identifier in the interface builder
    if ([segue.identifier isEqualToString:@"detailsView"])
    {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
         DetailController *DC = [segue destinationViewController];
         DC.dataType = indexPath.section;
        
        if([typeArray[indexPath.section] isEqualToString:@"tableTarget"]){
            NSMutableArray *filterArray = [NSMutableArray new];
            for (int i = 0 ;i<[dictTargets count]; i++) {
                if([dictTargets[i][@"eCategories"] containsObject:[NSNumber numberWithInteger:[Singleton sharedInstance].model.categoryID]]){
                    [filterArray addObject:dictTargets[i]];
                }
            }
            DC.dataType =3;
            DC.contentArray = [filterArray mutableCopy];
        }
        
        if([typeArray[indexPath.section] isEqualToString:@"tableTargetEasy"]){
            NSMutableArray *filterArray = [NSMutableArray new];
            for (int i = 0 ;i<[dictTargets count]; i++) {
                if([dictTargets[i][@"eCategories"] containsObject:[NSNumber numberWithInteger:[Singleton sharedInstance].model.categoryID]]){
                    [filterArray addObject:dictTargets[i]];
                }
            }
            DC.dataType =5;
            DC.contentArray = [filterArray mutableCopy];
            
        }
        
        if([typeArray[indexPath.section] isEqualToString:@"tableMass"]){
            DC.contentArray= maxMass[@"maxMass"];
             DC.dataType =4;
        }
        if([typeArray[indexPath.section] isEqualToString:@"tableCount"]){
            DC.contentArray =dictSits[@"sits"];
            DC.dataType =6;
        }
        if(indexPath.section==0){
            DC.contentArray = dictRegion[@"regions"];
            DC.secondContentArray = dictCities[@"eosagoCities"];
            DC.isRegion =TRUE;
        }
        
        if(indexPath.section==2){
            DC.contentArray = [NSMutableArray arrayWithArray:dictCar[@"vehicleCategories"]];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(![typeArray[indexPath.section] isEqualToString:@"switchE"]&&![typeArray[indexPath.section] isEqualToString:@"switchC"]&&![typeArray[indexPath.section] isEqualToString:@"switchA"]&&![typeArray[indexPath.section] isEqualToString:@"textfield"]){
     [self performSegueWithIdentifier:@"detailsView" sender:indexPath];
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end



