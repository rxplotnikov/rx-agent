//
//  CustomCell.m
//  RX Агент
//
//  Created by RX Group on 26.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell{
    NSString *currentIdentifire;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellText:(NSString*)text
                 andSubtext:(NSString *)subtext
             andPlaceholder:(NSString *)placeholder
                andSwitcher:(BOOL)sWneed
               andTextfield:(BOOL)tFneed
            andCurrentIdent:(NSString*)current{
    
    if(current!=currentIdentifire){
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
       
       
        self.textLabel.numberOfLines=2;
    
        if(subtext){
            self.textLabel.text = [NSString stringWithFormat:@"%@, %@",text,subtext];
        }else{
            self.textLabel.text = text;
        }
        if(tFneed){
             self.textLabel.text =@"";
             self.accessoryType = UITableViewCellAccessoryNone;
        }
        if(sWneed){
            self.accessoryType = UITableViewCellAccessoryNone;
        }

        if(tFneed){
         
                _textfield = [[UITextField alloc] initWithFrame:CGRectMake(12, 0, self.frame.size.width - self.textLabel.frame.origin.x, self.frame.size.height)];
                _textfield.adjustsFontSizeToFitWidth = YES;
                _textfield.textColor = [UIColor blackColor];
                _textfield.placeholder = @"Мощность, л.с.";
                [_textfield setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
                [_textfield setTextColor:[UIColor lightGrayColor]];
            if(text){
                [_textfield setFont:[UIFont fontWithName:@"Helvetica Bold" size:18.f]];
                [_textfield setTextColor:[UIColor blackColor]];
                _textfield.text = text;
            }
                _textfield.keyboardType = UIKeyboardTypeNumberPad;
                _textfield.returnKeyType = UIReturnKeyDone;
                _textfield.backgroundColor = [UIColor clearColor];
                _textfield.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
                _textfield.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
                _textfield.tag = 0;
                _textfield.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
                [_textfield setEnabled:YES];
                [self.contentView addSubview:_textfield];
        }else{
            [_textfield removeFromSuperview];
        }
        
        if(sWneed){
            if(!_switcher){
                _switcher = [[UISwitch alloc] init];
                CGSize switchSize = [_switcher sizeThatFits:CGSizeZero];
                _switcher.frame = CGRectMake(self.contentView.bounds.size.width - switchSize.width - 5.0f,
                                             (self.contentView.bounds.size.height - switchSize.height) / 2.0f,
                                             switchSize.width,
                                             switchSize.height);
                _switcher.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                [_switcher addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                [_switcher setHidden:!sWneed];
                [self.contentView addSubview:_switcher];
            }else{
                 [_switcher removeFromSuperview];
                _switcher = [[UISwitch alloc] init];
                CGSize switchSize = [_switcher sizeThatFits:CGSizeZero];
                _switcher.frame = CGRectMake(self.contentView.bounds.size.width - switchSize.width - 5.0f,
                                             (self.contentView.bounds.size.height - switchSize.height) / 2.0f,
                                             switchSize.width,
                                             switchSize.height);
                _switcher.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                [_switcher addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                [_switcher setHidden:!sWneed];
                [self.contentView addSubview:_switcher];
            }
        }else{
             [_switcher removeFromSuperview];
        }
        if(placeholder){
            [self.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
            [self.textLabel setTextColor:[UIColor lightGrayColor]];
            self.textLabel.text = placeholder;
        }else{
            [self.textLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:18.f]];
            [self.textLabel setTextColor:[UIColor blackColor]];
        }
        currentIdentifire=current;
    }else{
        
        if(tFneed){
          
            if(text){
                [_textfield setFont:[UIFont fontWithName:@"Helvetica Bold" size:18.f]];
                [_textfield setTextColor:[UIColor blackColor]];
                _textfield.text = text;
            }
        }else{
            [_textfield removeFromSuperview];
            if(subtext){
                self.textLabel.text = [NSString stringWithFormat:@"%@, %@",text,subtext];
            }else{
                self.textLabel.text = text;
            }
        }
        if(!sWneed&&_switcher){
            [_switcher removeFromSuperview];
        }
        if(placeholder){
            [self.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
            [self.textLabel setTextColor:[UIColor lightGrayColor]];
            self.textLabel.text = placeholder;
        }else{
            [self.textLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:18.f]];
            [self.textLabel setTextColor:[UIColor blackColor]];
        }
    }

}

-(void)switchChanged:(UISwitch *)switcher{
    if([currentIdentifire isEqualToString:@"switchA"]||[currentIdentifire isEqualToString:@"switchC"]||[currentIdentifire isEqualToString:@"switchE"])
    [_delegate switcherAction:switcher.isOn];
    
}
@end
