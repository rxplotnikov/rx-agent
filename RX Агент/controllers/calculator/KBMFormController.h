//
//  KBMFormController.h
//  RX Агент
//
//  Created by RX Group on 26.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBMFormController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *birthField;
@property (weak, nonatomic) IBOutlet UITextField *serField;
@property (weak, nonatomic) IBOutlet UITextField *numberField;
@property (weak, nonatomic) IBOutlet UILabel *driverIndex;
@property NSInteger index;
@property (weak, nonatomic) IBOutlet UIView *optionalView;
@property (weak, nonatomic) IBOutlet UIButton *typeLbl;
@property (weak, nonatomic) IBOutlet UITextField *IDfield;
@property (weak, nonatomic) IBOutlet UILabel *mainTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *serialLbl;
@property (weak, nonatomic) IBOutlet UILabel *numberLbl;

@end
