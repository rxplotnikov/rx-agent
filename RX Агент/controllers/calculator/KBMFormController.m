//
//  KBMFormController.m
//  RX Агент
//
//  Created by RX Group on 26.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "KBMFormController.h"
#import "Singleton.h"
#import "RXServerConnector.h"
#import "RXLoadingView.h"
#import "DetailController.h"

@interface KBMFormController ()<UITextFieldDelegate>{
    UIDatePicker *datePicker;
    RXLoadingView *loadingView;
    NSDictionary *idDict;
}

@end

@implementation KBMFormController

- (void)viewDidLoad {
    [super viewDidLoad];
    idDict = @{@"IDS":@[
                  @{@"title":@"VIN"},
                  @{@"title":@"Кузов"},
                  @{@"title":@"Шасси"}
              ]};
  
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    _nameField.delegate=self;
    _birthField.delegate=self;
    _serField.delegate=self;
    _numberField.delegate=self;
    loadingView = [[RXLoadingView alloc]initWithText:@"Идет запрос на сервер..."];
    [self initAtributtedText:@"Отсутствует VIN"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    if(_index==5){
        _driverIndex.text = @"Владелец";
    }else{
        _driverIndex.text = [NSString stringWithFormat:@"Водитель №%ld",_index+1];
    }
    [_optionalView setHidden:[Singleton sharedInstance].isLimit];
    if([Singleton sharedInstance].model.ident){
        [self initAtributtedText:[NSString stringWithFormat:@"Отсутствует %@", [Singleton sharedInstance].model.ident]];
        _mainTypeLbl.text=[Singleton sharedInstance].model.ident;
        _IDfield.placeholder =[NSString stringWithFormat:@"Укажите %@",[Singleton sharedInstance].model.ident];
    }
    if(![Singleton sharedInstance].isLimit){
        _serialLbl.text=@"Серия паспорта";
        _numberLbl.text=@"Номер паспорта";
    }else{
        _serialLbl.text=@"Серия в/у";
        _numberLbl.text=@"Номер в/у";
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  
}
- (void)initAtributtedText:(NSString *)text{
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentLeft];
    
    UIFont *font1 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSFontAttributeName:font1,
                            NSParagraphStyleAttributeName:style,
                            NSForegroundColorAttributeName : [UIColor blueColor]
                            }; // Added line
    
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:text     attributes:dict1]];
    _typeLbl.titleLabel. numberOfLines = 2;
    [_typeLbl setAttributedTitle:attString forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
   
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if([sender isEqual:_birthField]){
        datePicker=[[UIDatePicker alloc]init];
        datePicker.datePickerMode=UIDatePickerModeDate;
        [datePicker setMaximumDate:[NSDate dateWithTimeInterval:-567993600 sinceDate:[NSDate date]]];
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"ru"]];
        [datePicker addTarget:self action:@selector(doneBirth:) forControlEvents:UIControlEventValueChanged];
        sender.inputView=datePicker;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
        dateFormat.dateStyle = NSDateFormatterShortStyle;
        NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
        [_birthField setText:[NSString stringWithFormat:@"%@",dateStri]];
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setTintColor:[UIColor grayColor]];
        UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Готово"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self
                                                                action:@selector(hideKeyboard)];
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
        [_birthField setInputAccessoryView:toolBar];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag==0){
        return YES;
    }
    if([Singleton sharedInstance].isLimit){
            if(textField.tag==1){
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
    
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 10;
    
            }
            if(textField.tag==2){
                if(range.length + range.location > textField.text.length)
                {
                    return NO;
                }
    
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return newLength <= 20;
            }
    }else{
        if(textField.tag==1){
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 4;
            
        }
        if(textField.tag==2){
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 6;
        }
    }
    return NO;
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

-(void)hideKeyboard{
    if(_birthField.isFirstResponder){
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
 
    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
    [_birthField setText:[NSString stringWithFormat:@"%@",dateStri]];
    }
     [self.view endEditing:YES];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)doneBirth:(UITextField*)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
   
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
    [_birthField setText:[NSString stringWithFormat:@"%@",dateStri]];
}
- (IBAction)sendKBM:(id)sender {
    [loadingView showView];
    
    NSDictionary *request = [self getDictionaryAndSettings:_mainTypeLbl.text];
   
    
    
    
}
-(NSDictionary *)getDictionaryAndSettings:(NSString *)text{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *name = _nameField.text;
    NSString *birth = _birthField.text;
    NSString *series =_serField.text;
    NSString *number = _numberField.text;
    NSString *currentdate =[dateFormat stringFromDate:[NSDate date]];
    NSString *unlim = [NSString stringWithFormat:@"%@",[Singleton sharedInstance].isLimit ? @"false": @"true"];
    NSString *param = _IDfield.text;
    NSDictionary *request = [NSDictionary new];
    if([Singleton sharedInstance].isLimit){
        request= @{
                   @"date": currentdate,
                   @"unlimited":unlim,
                   @"fullName": name,
                   @"dateOfBirth":birth,
                   @"series": series,
                   @"number": number,
                   @"vin": @"null",
                   @"body": @"null",
                   @"chassis": @"null"
                   };
    }else{
        if([text isEqualToString:@"VIN"]){
            request= @{
                       @"date": currentdate,
                       @"unlimited":unlim,
                       @"fullName": name,
                       @"dateOfBirth":birth,
                       @"series": series,
                       @"number": number,
                       @"vin": param,
                       @"body": @"null",
                       @"chassis": @"null"
                       };
        }
        if([text isEqualToString:@"Кузов"]){
            request= @{
                       @"date": currentdate,
                       @"unlimited":unlim,
                       @"fullName": name,
                       @"dateOfBirth":birth,
                       @"series": series,
                       @"number": number,
                       @"vin": @"null",
                       @"body": param,
                       @"chassis": @"null"
                       };
        }
        if([text isEqualToString:@"Шасси"]){
            request= @{
                       @"date": currentdate,
                       @"unlimited":unlim,
                       @"fullName": name,
                       @"dateOfBirth":birth,
                       @"series": series,
                       @"number": number,
                       @"vin": @"null",
                       @"body": @"null",
                       @"chassis": param
                       };
        }
    }
    if([Singleton sharedInstance].isLimit){
        if(currentdate.length>0&&name.length>0&&birth.length>0&&series.length>0&&number.length>0){
            [self sendJsonToServer:request];
        }else{
            [self showAlertWithText:@"Поля заполнены не полностью."];
        }
    }else{
        if(currentdate.length>0&&name.length>0&&birth.length>0&&series.length>0&&number.length>0&&param.length>0){
            [self sendJsonToServer:request];
        }else{
            [self showAlertWithText:@"Поля заполнены не полностью."];
        }
    }
    return request;
}
-(void)sendJsonToServer:(NSDictionary *)dictionary{
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dictionary options:0 error:nil];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [[RXServerConnector sharedConnector]postJsonToServer:myString posturl:@"https://demo-api.rx-agent.ru/v0/KBM" withBlock:^(NSDictionary *json) {
        if([Singleton sharedInstance].isLimit){
            if(self->_index==0){
                [Singleton sharedInstance].model.KBM1=[json[@"kbm"][@"kbm"]stringValue];
            }
            
            if(self->_index==1){
                [Singleton sharedInstance].model.KBM2=[json[@"kbm"][@"kbm"]stringValue];
            }
            
            if(self->_index==2){
                [Singleton sharedInstance].model.KBM3=[json[@"kbm"][@"kbm"]stringValue];
            }
            
            if(self->_index==3){
                [Singleton sharedInstance].model.KBM4=[json[@"kbm"][@"kbm"]stringValue];
            }
        }else{
            [Singleton sharedInstance].model.ownerKBM=[json[@"kbm"][@"kbm"]stringValue];
        }
        [self dismissViewControllerAnimated:YES completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->loadingView dissmissView];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"closeView"
                 object:self];
            });
            
        }];
    }];
}
- (IBAction)identifireChosen:(id)sender {
    [self performSegueWithIdentifier:@"detailShow" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"detailShow"]){
        DetailController *DC = segue.destinationViewController;
        DC.contentArray = idDict[@"IDS"];
        DC.dataType = 9;
    }
}
-(void)showAlertWithText:(NSString *)text{
    [loadingView dissmissView];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Внимание!"
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"ОК" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
