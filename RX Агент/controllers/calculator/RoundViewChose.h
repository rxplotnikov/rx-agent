//
//  RoundViewChose.h
//  RX Агент
//
//  Created by RX Group on 18.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RoundViewDelegate <NSObject>

- (void)buttonSelected:(NSInteger)index;

@end




@interface RoundViewChose : UIView
@property (nonatomic, assign) id<RoundViewDelegate> delegate;
@property NSInteger currentIndex;
@end
