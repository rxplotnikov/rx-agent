//
//  Controller2.m
//  RX Агент
//
//  Created by RX Group on 14.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "Controller2.h"
#import "mainCell.h"
#import "RXServerConnector.h"
#import "DetailController.h"
#import "KBMViewController.h"
#import "RXLoadingView.h"

@interface Controller2 ()<RoundViewDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,kbmDelegate>{
    float heightDriver;
    UIDatePicker *datePicker;
    NSMutableArray *firstTableSections;
    NSMutableArray *firstTablePlaceholders;
    NSMutableDictionary *periods;
    NSDictionary *minExp;
    NSDictionary *minAge;
    int indexLoad;
    RXLoadingView *loadView;
}

@end

@implementation Controller2

- (void)viewDidLoad
{
    [super viewDidLoad];

    minExp = @{@"exp" :@[
                          @{@"title":@"3 года и более",
                            @"id":@"false"
                            },
                          @{@"title":@"Менее 3 лет",
                            @"id":@"true"
                            }
                          ]};
    minAge = @{@"age" :@[
                       @{@"title":@"22 года и более",
                         @"id":@"false"
                         },
                       @{@"title":@"Менее 22 лет",
                         @"id":@"true"
                         }
                       ]};
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initView) name:@"checkUpdate" object:nil];
    [Singleton sharedInstance].model.countDriver=1;
    _roundView.delegate=self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    
}
-(void)configView{
    _mainTable.translatesAutoresizingMaskIntoConstraints=YES;
    _roundView.translatesAutoresizingMaskIntoConstraints=YES;
    _table.translatesAutoresizingMaskIntoConstraints=YES;
    if([Singleton sharedInstance].isLimit){
    [_mainTable setFrame:CGRectMake(0, 0, _scroll.frame.size.width, 360.f)];
    [_roundView setFrame:CGRectMake(0, 360.f, _scroll.frame.size.width, 60)];
    [_table setFrame:CGRectMake(0, 420.f, _scroll.frame.size.width, 72 * [Singleton sharedInstance].model.countDriver)];
    _scroll.contentSize = CGSizeMake(self.view.frame.size.width, 420.f + 72*[Singleton sharedInstance].model.countDriver);
    }else{
    [_mainTable setFrame:CGRectMake(0, 0, _scroll.frame.size.width, 216.f)];
    [_table setFrame:CGRectMake(0, 216.f, _scroll.frame.size.width, 72)];
    _scroll.contentSize = CGSizeMake(self.view.frame.size.width, 288.f);
    }
   
    [_scroll addSubview:_mainTable];
    [_scroll addSubview:_roundView];
    [_scroll addSubview:_table];
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initView];
}
-(void)initView{
   
    if([Singleton sharedInstance].indexContain==2){
        [self configView];
        if(indexLoad!=1){
        loadView = [[RXLoadingView alloc]initWithText:@"Загрузка..."];
        [loadView showView];
        }
        
        
        if(!periods)
        {
            [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/InsurancePeriods"
                                                     withBlock:^(NSMutableDictionary *dictionary) {
                                                         self->periods=dictionary;
                                                           [self loadBlurView];
                                                     }];
        }else{
            [_mainTable reloadData];
        }
        
        [_table reloadData];
        [self checkLimit];
        [self checkKBM];
        [self checkFileds];
    }
}
-(void)loadBlurView{
    indexLoad++;
    if(indexLoad==1){
        [loadView dissmissView];
    }
}
-(void)checkKBM{
    if([Singleton sharedInstance].isLimit){
        NSMutableArray *array =[NSMutableArray arrayWithCapacity:4];
        [array addObject: [Singleton sharedInstance].model.KBM1];
        [array addObject: [Singleton sharedInstance].model.KBM2];
        [array addObject: [Singleton sharedInstance].model.KBM3];
        [array addObject: [Singleton sharedInstance].model.KBM4];
        for (int i = 0; i<[Singleton sharedInstance].model.countDriver; i++) {
            if([self isEmpty:array[i]]){
                [Singleton sharedInstance].model.allKBM=FALSE;
                return;
            }else{
                [Singleton sharedInstance].model.allKBM=TRUE;
            }
        }
    }else{
         [Singleton sharedInstance].model.allKBM = [self isEmpty:[Singleton sharedInstance].model.ownerKBM] ? FALSE : TRUE;
    }
}
-(void)checkFileds{
    
    if([Singleton sharedInstance].model.term.length>0&&[Singleton sharedInstance].model.startDate.length>0&&[Singleton sharedInstance].model.allKBM){
        if(![Singleton sharedInstance].isLimit){
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"hide"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
        }else{
            if([Singleton sharedInstance].model.minExp.length>0&&[Singleton sharedInstance].model.minAge.length>0){
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"hide"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
            }else{
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:@"hide"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
            }
        }
        }else{
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:@"hide"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enableBtn" object:nil userInfo:dictionary];
        }
}
-(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches
              withEvent:event];
    [self hideKeyboard];
}



-(void)hideKeyboard
{
    [self checkKBM];
    [self checkFileds];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)checkLimit
{
    [_roundView setHidden:![Singleton sharedInstance].isLimit];
    if(![Singleton sharedInstance].isLimit)
    {
          [Singleton sharedInstance].model.countDriver=1;
       
        firstTableSections = [NSMutableArray arrayWithArray:@[@"Срок страхования",@"Дата начала",@"Лица, допущенные к управлению"]];
        firstTablePlaceholders = [NSMutableArray arrayWithArray:@[@"Укажите срок страхования",@"Дата начала",@"Без ограничений"]];
    }else{
        [Singleton sharedInstance].model.countDriver = _roundView.currentIndex+1;
      
        firstTableSections = [NSMutableArray arrayWithArray:@[@"Срок страхования",@"Дата начала",@"Минимальный стаж",@"Минимальный возраст",@"Лица, допущенные к управлению"]];
        firstTablePlaceholders = [NSMutableArray arrayWithArray:@[@"Укажите срок страхования",@"Дата начала",@"Укажите минимальный стаж",@"Укажите минимальный возраст",@"Без ограничений"]];
    }
    [self configView];
    [_table reloadData];
    [_mainTable reloadData];
}

-(void)buttonSelected:(NSInteger)index
{
     [Singleton sharedInstance].model.countDriver = index+1;
    [self configView];
    [self checkKBM];
    [self checkFileds];
    [_table reloadData];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    NSDate *date = [NSDate dateWithTimeInterval:5270400 sinceDate:[NSDate date]];
    datePicker.date =[NSDate dateWithTimeInterval:172800 sinceDate:[NSDate date]];
    
    [datePicker setMaximumDate:date];
    [datePicker setMinimumDate:[NSDate dateWithTimeInterval:86400 sinceDate:[NSDate date]]];
    [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"ru"]];
    [datePicker addTarget:self action:@selector(done) forControlEvents:UIControlEventValueChanged];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    mainCell *cell = [self.mainTable cellForRowAtIndexPath:indexPath];
    cell.textfield.inputView=datePicker;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
    dateFormat.dateStyle = NSDateFormatterLongStyle;
    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
    [cell.textfield setText:[NSString stringWithFormat:@"%@",dateStri]];
     [Singleton sharedInstance].model.startDate = [NSString stringWithFormat:@"%@",dateStri];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"YYYY-MM-dd"];
    NSString *dateStri2=[dateFormat2 stringFromDate:datePicker.date];
    [Singleton sharedInstance].model.startDateJSON = [NSString stringWithFormat:@"%@",dateStri2];
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Готово"
                                                             style:UIBarButtonItemStyleDone
                                                            target:self
                                                            action:@selector(hideKeyboard)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];

    [cell.textfield setInputAccessoryView:toolBar];
}

-(void)done
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    mainCell *cell = [self.mainTable cellForRowAtIndexPath:indexPath];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
    dateFormat.dateStyle = NSDateFormatterLongStyle;
    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
    [cell.textfield setText:[NSString stringWithFormat:@"%@",dateStri]];
    [Singleton sharedInstance].model.startDate = [NSString stringWithFormat:@"%@",dateStri];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"YYYY-MM-dd"];
    NSString *dateStri2=[dateFormat2 stringFromDate:datePicker.date];
    [Singleton sharedInstance].model.startDateJSON = [NSString stringWithFormat:@"%@",dateStri2];
    [self checkKBM];
    [self checkFileds];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     if([tableView isEqual:_table])
     {
         return  [Singleton sharedInstance].model.countDriver;
     }
        return firstTableSections.count;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([tableView isEqual:_table])
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor colorWithRed:245.f/255.f green:245.f/255.f blue:245.f/255.f alpha:1.f]];

        UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,5,150.0,20.0)];
        [customLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
        [customLabel setTextColor:[UIColor colorWithRed:233.f/255.f green:148.f/255.f blue:0.f/255.f alpha:1.f]];
        [headerView addSubview:customLabel];

            if(![Singleton sharedInstance].isLimit)
            {
                [customLabel setText:@"Владелец"];
            }else{
                [customLabel setText:[NSString stringWithFormat:@"Водитель №%ld",(long)section+1]];
            }
        return headerView;
    }
    if([tableView isEqual:_mainTable]){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor colorWithRed:245.f/255.f green:245.f/255.f blue:245.f/255.f alpha:1.f]];
        
        UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,5,self.table.frame.size.width-20,20.0)];
        [customLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
         [customLabel setTextColor:[UIColor colorWithRed:72.f/255.f green:72.f/255.f blue:72.f/255.f alpha:1.f]];
        customLabel.text = firstTableSections[section];
        [headerView addSubview:customLabel];
        return headerView;
    }
    return nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
     if([tableView isEqual:_table])
     {
       
        static NSString *CellIdentifier = @"Cell";

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:CellIdentifier];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = @"КБМ";
         if([Singleton sharedInstance].isLimit)
         {
             if(indexPath.section==0)
             {
                 if(![self isEmpty:[Singleton sharedInstance].model.KBM1])
                 {
                     cell.detailTextLabel.text = [Singleton sharedInstance].model.KBM1;
                 }else{
                     cell.detailTextLabel.text = @"Не указ.";
                 }
             }
             if(indexPath.section==1)
             {
                 if(![self isEmpty:[Singleton sharedInstance].model.KBM2])
                 {
                     cell.detailTextLabel.text = [Singleton sharedInstance].model.KBM2;
                 }else{
                     cell.detailTextLabel.text = @"Не указ.";
                 }
             }
             if(indexPath.section==2)
             {
                 if(![self isEmpty:[Singleton sharedInstance].model.KBM3])
                 {
                     cell.detailTextLabel.text = [Singleton sharedInstance].model.KBM3;
                 }else{
                     cell.detailTextLabel.text = @"Не указ.";
                 }
             }
             if(indexPath.section==3)
             {
                 if(![self isEmpty:[Singleton sharedInstance].model.KBM4])
                 {
                     cell.detailTextLabel.text = [Singleton sharedInstance].model.KBM4;
                 }else{
                     cell.detailTextLabel.text = @"Не указ.";
                 }
             }
         }else{
             if(indexPath.section==0)
             {
                 if([Singleton sharedInstance].model.ownerKBM)
                 {
                     cell.detailTextLabel.text = [Singleton sharedInstance].model.ownerKBM;
                 }else{
                     cell.detailTextLabel.text = @"Не указ.";
                 }
             }
         }
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
        cell.textLabel.numberOfLines=2;

        return cell;
     }
    
    if([tableView isEqual:_mainTable])
    {
        static NSString *CellIdentifier = @"MainCell";
       
        mainCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       
        if (cell == nil)
        {
            cell = [[mainCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:CellIdentifier];
        }
        
        if(indexPath.section==0)
        {
            if([Singleton sharedInstance].model.term)
            {
                  [cell setCellText:[Singleton sharedInstance].model.term andPlaceholder:nil  andTextfield:NO andSwitcher:NO];
            }else{
                [cell setCellText:nil andPlaceholder:firstTablePlaceholders[indexPath.section] andTextfield:NO andSwitcher:NO];
            }
        }
        
        if(indexPath.section==1)
        {
            if([Singleton sharedInstance].model.startDate)
            {
                [cell setCellText:[Singleton sharedInstance].model.startDate andPlaceholder:nil andTextfield:YES andSwitcher:NO];
            }else{
                [cell setCellText:firstTablePlaceholders[indexPath.section] andPlaceholder:nil andTextfield:YES andSwitcher:NO];
            }
            cell.textfield.delegate=self;
        }
        
        if(indexPath.section==2)
        {
            if([Singleton sharedInstance].isLimit){
                if([Singleton sharedInstance].model.minExp)
                {
                    [cell setCellText:[Singleton sharedInstance].model.minExp andPlaceholder:nil andTextfield:NO andSwitcher:NO];
                }else{
                    [cell setCellText:nil andPlaceholder:firstTablePlaceholders[indexPath.section] andTextfield:NO andSwitcher:NO];
                }
            }else{
                cell.delegate=self;
                [cell setCellText:firstTablePlaceholders[indexPath.section] andPlaceholder:nil andTextfield:NO andSwitcher:YES];
            }
        }
        
        if(indexPath.section==3)
        {
            if([Singleton sharedInstance].isLimit){
                if([Singleton sharedInstance].model.minAge)
                {
                    [cell setCellText:[Singleton sharedInstance].model.minAge andPlaceholder:nil andTextfield:NO andSwitcher:NO];
                }else{
                    [cell setCellText:nil andPlaceholder:firstTablePlaceholders[indexPath.section] andTextfield:NO andSwitcher:NO];
                }
            }else{
                cell.delegate=self;
                [cell setCellText:firstTablePlaceholders[indexPath.section] andPlaceholder:nil andTextfield:NO andSwitcher:YES];
            }
        }
        if(indexPath.section==4)
        {
            cell.delegate=self;
            [cell setCellText:firstTablePlaceholders[indexPath.section] andPlaceholder:nil andTextfield:NO andSwitcher:YES];
        }
        return cell;
    }
   
    return nil;
}
-(void)switcherAction:(BOOL)isOn
{
    [Singleton sharedInstance].isLimit=!isOn;
    [self checkLimit];
    [self checkKBM];
    [self checkFileds];
   
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:_table])
    {
        [self performSegueWithIdentifier:@"KBM" sender:indexPath];
    }
    if([tableView isEqual:_mainTable])
    {
        if(indexPath.section!=4&&indexPath.section!=1){
        if([Singleton sharedInstance].isLimit||indexPath.section==0)
            [self performSegueWithIdentifier:@"detailShow" sender:indexPath];
        }
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     NSIndexPath *indexPath = (NSIndexPath *)sender;
    if([segue.identifier isEqualToString:@"KBM"])
    {
        KBMViewController *KBM = segue.destinationViewController;
        if([Singleton sharedInstance].isLimit)
        {
            KBM.currentKBM = indexPath.section;
        }else{
            KBM.currentKBM =5;
        }
    }
    if ([segue.identifier isEqualToString:@"detailShow"])
    {
        DetailController *DC = [segue destinationViewController];
        if(indexPath.section==2)
        {
            DC.dataType = 8;
            DC.contentArray = minExp[@"exp"];
        }
        if(indexPath.section==3)
        {
            DC.dataType = 11;
            DC.contentArray = minAge[@"age"];
        }
        if(indexPath.section==0)
        {
            DC.dataType = 7;
            DC.contentArray = periods[@"insurancePeriods"];
        }
    }
}

@end
