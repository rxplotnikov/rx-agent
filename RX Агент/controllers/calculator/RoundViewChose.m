//
//  RoundViewChose.m
//  RX Агент
//
//  Created by RX Group on 18.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "RoundViewChose.h"
#import "Singleton.h"

@implementation RoundViewChose{
    UIActivityIndicatorView * activityIndicator;
    UIView *view;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
        if(self =[super initWithCoder:aDecoder]){
           
        }
    return self;
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
   
    self.layer.masksToBounds=YES;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.layer.borderWidth=1.f;
    for (int i =0; i<4; i++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(i*self.frame.size.width/4, 0, self.frame.size.width/4, self.frame.size.height)];
        [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:35.f]];
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(i*self.frame.size.width/4, 0)];
        [path addLineToPoint:CGPointMake(i*self.frame.size.width/4,self.frame.size.height)];
        path.lineWidth=1.f;
        [[UIColor lightGrayColor]setStroke];
        [path stroke];
        btn.tag=i;
        if(i ==  [Singleton sharedInstance].model.countDriver-1){
            [btn setBackgroundColor:[UIColor colorWithRed:180.f/255.f green:230.f/255.f blue:182.f/255.f alpha:1.f]];
        }
        [btn addTarget:self action:@selector(selectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:[NSString stringWithFormat:@"%d",i+1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self addSubview:btn];
     
    }
    
    
}
-(void)selectBtn:(UIButton*)btn{
    for (UIButton *butn in self.subviews) {
        if([butn isKindOfClass:[UIButton class]]){
            if(butn.tag == btn.tag){
                
                [butn setBackgroundColor:[UIColor colorWithRed:180.f/255.f green:230.f/255.f blue:182.f/255.f alpha:1.f]];
            }else{
                [butn setBackgroundColor:[UIColor clearColor]];
            }
        }
    }
    self->_currentIndex=btn.tag;
    [self.delegate buttonSelected:btn.tag];

}
- (void)threadStartAnimating {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->activityIndicator startAnimating];
    });
    
}
@end
