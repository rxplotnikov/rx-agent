//
//  Controller3.m
//  RX Агент
//
//  Created by RX Group on 14.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "Controller3.h"
#import "Singleton.h"
#import "RXServerConnector.h"
#import "RXLoadingView.h"

@interface Controller3 ()<UITableViewDelegate,UITableViewDataSource>{
    NSDictionary*dict;
    RXLoadingView *loadview;
    NSMutableArray *arrayKeys;
}

@end

@implementation Controller3

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateView)
                                                 name:@"updateView"
                                               object:nil];
}
-(void)updateView{
    if([Singleton sharedInstance].indexContain==3){
        arrayKeys = [NSMutableArray new];
      
        if(!dict){
            loadview = [[RXLoadingView alloc]initWithText:@"Идет запрос на сервер..."];
            [loadview showView];
        }
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:[[Singleton sharedInstance]createJsonRequest] options:0 error:nil];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        [[RXServerConnector sharedConnector]postJsonToServer:myString posturl:@"https://demo-api.rx-agent.ru/v0/EOSAGOCalculator" withBlock:^(NSDictionary *json) {

            self->dict=json;
            NSDictionary *dictionary = @{@"coef":@"0",
                                         @"title":@"Подробности расчета",
                                         @"value":[NSString stringWithFormat:@"Базовая ставка:  от %@ до %@",[NSString stringWithFormat:@"%@₽",[self->dict[@"eosagoCalculator"][@"baseCostFrom"]stringValue]],[NSString stringWithFormat:@"%@₽",[self->dict[@"eosagoCalculator"][@"baseCostTo"]stringValue]]]
                                             };
            [self->loadview dissmissView];
            [self->arrayKeys addObject:dictionary];
            for (int i=0; i<[self->dict[@"eosagoCalculator"][@"conditions"]count]; i++) {
                [self->arrayKeys addObject:self->dict[@"eosagoCalculator"][@"conditions"][i]];
            }
            dispatch_async(dispatch_get_main_queue(), ^{

                [self->_summTable reloadData];
                [self->_table reloadData];
                
            });
        }];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([tableView isEqual:_table]){
    return [arrayKeys count];
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if([tableView isEqual:_table]){
    return arrayKeys[section][@"title"];
    }
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if([tableView isEqual:_summTable]){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,5,tableView.bounds.size.width-12,20.0)];
        [customLabel setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
        [customLabel setTextColor:[UIColor colorWithRed:130.f/255.f green:109.f/255.f blue:64.f/255.f alpha:1.f]];
        [headerView addSubview:customLabel];
        customLabel.text=@"СТОИМОСТЬ ПОЛИСА";
       
      
        return headerView;
    }
    return nil;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     if([tableView isEqual:_table]){
        static NSString *CellIdentifier = @"Cell";
       
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:CellIdentifier];
        }
        cell.contentView.backgroundColor=[UIColor colorWithRed:245.f/255.f green:245.f/255.f blue:245.f/255.f alpha:1.f];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.numberOfLines=3;
         cell.textLabel.font=[UIFont fontWithName:@"Helvetica Light" size:16.f];
        cell.textLabel.text = arrayKeys[indexPath.section][@"value"];
         if(indexPath.section!=0){
        cell.detailTextLabel.text = [arrayKeys[indexPath.section][@"coef"]stringValue];
         cell.detailTextLabel.font=[UIFont fontWithName:@"Helvetica Light" size:16.f];
         }
         return cell;
     }
    if([tableView isEqual:_summTable]){
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:CellIdentifier];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.numberOfLines=2;
        if([Singleton sharedInstance].indexContain==3){
                float startSumm = [dict[@"eosagoCalculator"][@"sumFrom"]floatValue];
                float endSumm = [dict[@"eosagoCalculator"][@"sumTo"]floatValue];
                cell.textLabel.attributedText = [self getAtributedText:@"от " andText2:[NSString stringWithFormat:@"%.2f₽",startSumm] andText3:@" до " andText4:[NSString stringWithFormat:@"%.2f₽",endSumm]];
           
        }
        return cell;
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSMutableAttributedString*)getAtributedText:(NSString *)text1 andText2:(NSString *)text2 andText3:(NSString *)text3 andText4:(NSString*)text4{
    UIFont *font1 =[UIFont fontWithName:@"Helvetica Bold" size:24.f];
    UIFont *font2 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
   
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: font2 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString1 = [[NSMutableAttributedString alloc] initWithString:text1 attributes: arialDict];
    
    NSDictionary *arialDict2 = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString2 = [[NSMutableAttributedString alloc] initWithString:text2 attributes: arialDict2];
    
    NSDictionary *arialDict3 = [NSDictionary dictionaryWithObject: font2 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString3 = [[NSMutableAttributedString alloc] initWithString:text3 attributes: arialDict3];
    
    NSDictionary *arialDict4 = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString4 = [[NSMutableAttributedString alloc] initWithString:text4 attributes: arialDict4];
    
        [aAttrString3 appendAttributedString:aAttrString4];
        [aAttrString2 appendAttributedString:aAttrString3];
        [aAttrString1 appendAttributedString:aAttrString2];
        return aAttrString1;
}


@end
