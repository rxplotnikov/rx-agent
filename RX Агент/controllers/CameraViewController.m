//
//  CameraViewController.m
//  RXAgent
//
//  Created by RX Group on 04.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CameraViewController.h"
#import "CustomCamera.h"
#import "Singleton.h"

#import "AppDelegate.h"

@interface CameraViewController ()<CameraDelegate>{
    CustomCamera *camera;
    AppDelegate *appDelegate;
    UIImage *imagePhoto;
    NSManagedObjectContext *context;

}

@end

@implementation CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(granted){
                [self->_cameraAccess setHidden:YES];
                [self->_cameraBtn setHidden:NO];
                
            } else {
                [self->_cameraAccess setHidden:NO];
                [self->_cameraBtn setHidden:YES];
                
            }
        });
        
        
    }];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    camera = [[CustomCamera alloc]initWithView:_captureView];
    camera.delegate=self;
}

-(void)photoGrabbed:(UIImage *)image{
    [_sendBtn setHidden:NO];
    imagePhoto=image;
    [_PreView setHidden:NO];
    _photoImage.image = imagePhoto;
}

#pragma mark Preview
- (IBAction)goPreview:(id)sender {
    [_PreView setHidden:NO];
    _photoImage.image = imagePhoto;
   
}

- (IBAction)closePreview:(id)sender {
    [_PreView setHidden:YES];
}

- (IBAction)takeShot:(id)sender {
    
    [camera captureImage];
   
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)openImage:(UIImage *)image{
    _photoImage.image=image;
}

- (IBAction)sendToServer:(id)sender {
    [Singleton sharedInstance].osago.tempImage= imagePhoto;
    
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
