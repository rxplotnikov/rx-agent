//
//  ViewController.m
//  RX Агент
//
//  Created by RX Group on 21.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "ViewController.h"
#import "OsagoViewController.h"
#import "CalcViewController.h"
@interface ViewController (){
    NSArray *controllers;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    controllers = @[_feedContainer,_workContainer,_supportContainer,_settingsContainer];
    _tabBar.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];

}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)selectItemAtIndex:(NSInteger)index{
    if (index!=2) {
        for (UIView *viewC in controllers) {
            [UIView animateWithDuration:0.5f animations:^{
                if(viewC.tag==index){
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"load%ld",(long)viewC.tag] object:nil userInfo:nil];
                    [viewC setHidden:NO];
                }else{
                    [viewC setHidden:YES];
                }
            }];
        }
     
    }else{
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];

        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Е-Осаго" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            CATransition *transition = [[CATransition alloc] init];
            transition.duration = 0.5;
            transition.type = kCATransitionMoveIn;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CalcViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"calc"];
            [self presentViewController:vc animated:NO completion:nil];
        }]];
      
          [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _tabBar;
        popPresenter.sourceRect = _tabBar.bounds;
        [self presentViewController:actionSheet animated:YES completion:nil];
      
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
