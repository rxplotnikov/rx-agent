//
//  RXLoadingView.h
//  RX Агент
//
//  Created by RX Group on 25.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RXLoadingView : UIView

-(instancetype)initWithText:(NSString *)text;

-(void)showView;
-(void)dissmissView;

@end
