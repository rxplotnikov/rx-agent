//
//  osagoModel.h
//  RX Агент
//
//  Created by RX Group on 14.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface osagoModel : NSObject
@property BOOL isOwner;
@property BOOL hasTempReg;
@property BOOL hasDKP;
@property BOOL noDKP;
@property NSInteger currentIndexPhoto;
@property UIImage *tempImage;
@property NSString *category;

//Владелец
@property NSString *ownerDocumentPageFirstID;
@property NSString *ownerDocumentPageSecondID;

//Страхователь
@property NSString *documentPageFirstID;
@property NSString *documentPageSecondID;

@property NSString *saleContractPageID;
@property NSString *tempRegPageID;

@property NSString *vehicleTarget;
@property NSString *vehicleTargetJSON;
@property BOOL vehicleNumberIsEmpty;
@property BOOL noVehiclePassportPage;
@property BOOL noVehicleCertificatePage;
//ПТС
@property NSString *vehiclePassportPageFirstID;
@property NSString *vehiclePassportPageSecondID;
//СРТС
@property NSString *vehicleCertificatePageFirstID;
@property NSString *vehicleCertificatePageSecondID;
//ТО
@property NSString *cardPageFirstID;
@property NSString *cardPageSecondID;

//Водители
@property NSDictionary *dictionary;

//Карта
@property NSDictionary *cardDict;
@property NSMutableArray *tempPhoto;

//условия
@property NSString *creatorFullName;
@property NSString *creatorPhone;
@property NSString *creatorPhoneJSON;
@property NSString *agentComment;
@property BOOL isPriority;
@end
