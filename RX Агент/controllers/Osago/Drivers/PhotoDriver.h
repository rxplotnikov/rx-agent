//
//  PhotoDriver.h
//  RX Агент
//
//  Created by RX Group on 02.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonWithPhoto.h"
@protocol  DriverDelegate <NSObject>
-(void)buttonClickedAtIndex:(NSInteger)index;
-(void)attachmentDriverAtIndex:(NSInteger)index;
@end

@interface PhotoDriver : UIView
@property(weak, nonatomic) id<DriverDelegate>delegate;
-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSInteger)title andTags:(NSArray*)tags withBlock:(void (^)(float height))handler;
@property float totalHeight;
-(void)drawColor;
-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index;
-(void)createAttachAtIndex:(NSInteger)index;
@end
