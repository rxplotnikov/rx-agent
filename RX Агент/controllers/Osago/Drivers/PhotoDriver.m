//
//  PhotoDriver.m
//  RX Агент
//
//  Created by RX Group on 02.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "PhotoDriver.h"
#import "Singleton.h"

@implementation PhotoDriver{
    float newY;
}

-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSInteger)title andTags:(NSArray *)tags withBlock:(void (^)(float height))handler{
    if(self=[super initWithFrame:frame]){
    
        [self createHeaderText:[NSString stringWithFormat:@"Водитель №%ld",(long)title+1]];
        newY=newY+ 56;
        [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:[tags[0] integerValue]];
       
        [self createLabelAtPoint:CGPointMake(76, newY) withText:@"ВУ(1 сторона)"];
        newY=newY+68;
        [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:[tags[1] integerValue]];
       
        [self createLabelAtPoint:CGPointMake(76, newY) withText:@"ВУ(2 сторона)"];
        newY=newY+68;
        [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:[tags[2] integerValue]];
       
        [self createLabelAtPoint:CGPointMake(76, newY) withText:@"Перевод ВУ\n(для иностранных ВУ)"];
        newY=newY+68;
        _totalHeight = newY;
        handler(_totalHeight);
    }
    return self;
}
-(void)createAttachAtIndex:(NSInteger)index{
    CGRect rect;
    if(index==14||index==17||index==20||index==23){
        rect = CGRectMake(self.frame.size.width-55, 70, 35, 35);
    }
    if(index==15||index==18||index==21||index==24){
        rect = CGRectMake(self.frame.size.width-55, 138, 35, 35);
    }
    if(index==16||index==19||index==22||index==25){
        rect = CGRectMake(self.frame.size.width-55, 206, 35, 35);
    }
    UIButton *attach = [[UIButton alloc]initWithFrame:rect];
    attach.tag=index;
    [attach setImage:[UIImage imageNamed:@"attach"] forState:UIControlStateNormal];
    [attach addTarget:self action:@selector(attachClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:attach];
}

-(void)drawColor{
     [self setBackgroundColor:[UIColor whiteColor]];
}

-(void)createLabelAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width-point.x-49, 44)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:72.f/255.f green:72.f/255.f blue:72.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}
-(void)createPhotoButtonAtPoint:(CGPoint)point andTag:(NSInteger)index{
    ButtonWithPhoto *photo = [[ButtonWithPhoto alloc]initWithFrame:CGRectMake(point.x, point.y, 60, 60) andTag:index];
    photo.tag=index;
    photo.photoBtn.tag=index;
    [photo.photoBtn addTarget:self action:@selector(buttonClickedAtIndex:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:photo];
}

-(void)buttonClickedAtIndex:(UIButton *)btn{
    [self.delegate buttonClickedAtIndex:btn.tag];
}
-(void)attachClick:(UIButton *)btn{
    [self.delegate attachmentDriverAtIndex:btn.tag];
}
-(void)createHeaderText:(NSString*)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica" size:24.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, self.frame.size.width-10, 45)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:233.f/255.f green:148.f/255.f blue:0.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}
-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index{
   
    for (ButtonWithPhoto *photo in self.subviews) {
        if([photo isKindOfClass:[ButtonWithPhoto class]]){
            if(photo.tag == index){
              
                return photo;
            }
        }
    }
    return nil;
}
@end
