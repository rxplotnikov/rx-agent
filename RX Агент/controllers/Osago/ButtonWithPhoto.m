//
//  ButtonWithPhoto.m
//  RX Агент
//
//  Created by RX Group on 15.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "ButtonWithPhoto.h"

@implementation ButtonWithPhoto

-(instancetype)initWithFrame:(CGRect)frame andTag:(NSInteger)index{
    if(self=[super initWithFrame:frame]){
        [self setBackgroundColor:[UIColor colorWithRed:242.f/255.f green:242.f/255.f blue:242.f/255.f alpha:1.f]];
         self.layer.cornerRadius=10.f;
         self.layer.masksToBounds=YES;
        _photoBtn = [[UIButton alloc]initWithFrame:self.bounds];
        self.tag=index;
        [_photoBtn setImage:[UIImage imageNamed:@"photoBtn"] forState:UIControlStateNormal];
        [self addSubview:_photoBtn];
        
        _progressBar = [[MBCircularProgressBarView alloc]initWithFrame:CGRectMake(self.bounds.size.width/2-self.bounds.size.width*0.7/2, self.bounds.size.height/2-self.bounds.size.width*0.7/2, self.bounds.size.width*0.7, self.bounds.size.width*0.7)];
        [_progressBar setBackgroundColor:[UIColor clearColor]];
        _progressBar.progressLineWidth=1.f;
        _progressBar.progressAngle=100.f;
        _progressBar.unitFontSize = 14.f;
        _progressBar.valueFontSize = 16.f;
        [self addSubview:_progressBar];
        [_progressBar setHidden:YES];
    }
    return self;
}

-(void)setImageForButton:(UIImage*)image{
    [_photoBtn setHidden:NO];
    [_progressBar setHidden:YES];
    [_photoBtn setImage:image forState:UIControlStateNormal];
}

-(void)setProgress:(float)value{
     _progressBar.value=value;
}

-(void)loadImageToServer:(UIImage *)image{
    [_progressBar setHidden:NO];
    [_photoBtn setHidden:YES];
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"dd-MMMM-yyyy-HH-mm-ss"];
    NSString *result = [df stringFromDate:[NSDate date]];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5f);
    [[Singleton sharedInstance].osago.tempPhoto replaceObjectAtIndex:self.tag withObject:image];
    [[RXServerConnector sharedConnector]sendImageToServer:@"https://demo-api.rx-agent.ru/v0/attachments" andName:result andImage:imageData andIndex:self.tag withBlock:^(NSMutableDictionary *dictionary) {
        [self setImageForButton:[Singleton sharedInstance].osago.tempPhoto[self.tag]];
        if(self.tag==0)
            [Singleton sharedInstance].osago.documentPageFirstID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==1)
            [Singleton sharedInstance].osago.documentPageSecondID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==2)
            [Singleton sharedInstance].osago.saleContractPageID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==3)
            [Singleton sharedInstance].osago.tempRegPageID=[dictionary[@"attachment"][@"id"]stringValue];
        
        if(self.tag==4)
            [Singleton sharedInstance].osago.ownerDocumentPageFirstID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==5)
            [Singleton sharedInstance].osago.ownerDocumentPageSecondID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==6)
            [Singleton sharedInstance].osago.saleContractPageID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==7)
            [Singleton sharedInstance].osago.tempRegPageID=[dictionary[@"attachment"][@"id"]stringValue];
        
        if(self.tag==8)
            [Singleton sharedInstance].osago.vehiclePassportPageFirstID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==9)
            [Singleton sharedInstance].osago.vehiclePassportPageSecondID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==10)
            [Singleton sharedInstance].osago.vehicleCertificatePageFirstID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==11)
            [Singleton sharedInstance].osago.vehicleCertificatePageSecondID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==12)
            [Singleton sharedInstance].osago.cardPageFirstID=[dictionary[@"attachment"][@"id"]stringValue];
        if(self.tag==13)
            [Singleton sharedInstance].osago.cardPageSecondID=[dictionary[@"attachment"][@"id"]stringValue];
       
        NSMutableDictionary *mainDict = [[Singleton sharedInstance].osago.dictionary mutableCopy];
        NSMutableArray *arr= [mainDict[@"drivers"] mutableCopy];
        NSMutableDictionary *dict1 = [arr[0] mutableCopy];
        NSMutableDictionary *dict2 = [arr[1] mutableCopy];
        NSMutableDictionary *dict3 = [arr[2] mutableCopy];
        NSMutableDictionary *dict4 = [arr[3] mutableCopy];
        if(self.tag==14)
            [dict1 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageFirstID"];
        if(self.tag==15)
            [dict1 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageSecondID"];
        if(self.tag==16)
           [dict1 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"scanTranslationPageID"];
        [arr replaceObjectAtIndex:0 withObject:dict1];
        
       
        if(self.tag==17)
            [dict2 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageFirstID"];
        if(self.tag==18)
             [dict2 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageSecondID"];
        if(self.tag==19)
            [dict2 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"scanTranslationPageID"];
        [arr replaceObjectAtIndex:1 withObject:dict2];
        
        if(self.tag==20)
             [dict3 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageFirstID"];
        if(self.tag==21)
             [dict3 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageSecondID"];
        if(self.tag==22)
             [dict3 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"scanTranslationPageID"];
        [arr replaceObjectAtIndex:2 withObject:dict3];
        
        if(self.tag==23)
             [dict4 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageFirstID"];
        if(self.tag==24)
             [dict4 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"driverLicensePageSecondID"];
        if(self.tag==25)
            [dict4 setObject:[dictionary[@"attachment"][@"id"]stringValue] forKey:@"scanTranslationPageID"];
        
        [arr replaceObjectAtIndex:3 withObject:dict4];
        mainDict[@"drivers"]=[arr copy];
        [Singleton sharedInstance].osago.dictionary =[mainDict copy] ;
         [self.delegate loadIsOver];
    }];
}

@end
