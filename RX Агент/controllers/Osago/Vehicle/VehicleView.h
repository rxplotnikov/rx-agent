//
//  VehicleView.h
//  RX Агент
//
//  Created by RX Group on 08.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonWithPhoto.h"

@protocol  vehicleDelegate <NSObject>
-(void)vehicleSwitcherIsOn:(NSInteger)index andIsON:(BOOL)isON;
-(void)vehicleButtonClicked:(NSInteger)index;
-(void)vehicleAttachmentClick:(NSInteger)index;
-(void)needOpenPicker;
@end
@interface VehicleView : UIView

@property UITextField *textField;
@property UILabel *placeholderLbl;
@property(weak, nonatomic) id<vehicleDelegate>delegate;
@property float totalHeight;

-(instancetype)initWithFrame:(CGRect)frame andOgr:(BOOL)ogrVehicle andPTS:(BOOL)pts andSRTS:(BOOL)srts andPricep:(BOOL)pricep andLimit:(BOOL)isLimit withBlock:(void (^)(float height))handler;
-(void)initializeandOgr:(BOOL)ogrVehicle andPTS:(BOOL)pts andSRTS:(BOOL)srts andPricep:(BOOL)pricep andLimit:(BOOL)isLimit withBlock:(void (^)(float height))handler;

-(void)drawColor;
-(void)setImageForButton:(UIImage*)img andTag:(NSInteger)index;
-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index;
@property UIButton *btn;
@end
