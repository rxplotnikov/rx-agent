//
//  VehicleView.m
//  RX Агент
//
//  Created by RX Group on 08.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "VehicleView.h"
#import "Singleton.h"
#import "CategoryView.h"

@implementation VehicleView{
    float newY;
    BOOL isOpen;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame andOgr:(BOOL)ogrVehicle andPTS:(BOOL)pts andSRTS:(BOOL)srts andPricep:(BOOL)pricep andLimit:(BOOL)isLimit withBlock:(void (^)(float height))handler{
    if(self=[super initWithFrame:frame]){
        [self initializeandOgr:ogrVehicle andPTS:pts andSRTS:srts andPricep:pricep andLimit:isLimit withBlock:^(float height) {
            handler(height);
        }];
    }
    return self;
}
-(void)initializeandOgr:(BOOL)ogrVehicle andPTS:(BOOL)pts andSRTS:(BOOL)srts andPricep:(BOOL)pricep andLimit:(BOOL)isLimit withBlock:(void (^)(float height))handler{
    
    for (UIView *view in self.subviews) {
        if(![view isKindOfClass:[ButtonWithPhoto class]]){
            [view removeFromSuperview];
        }
    }
 
        [self createHeaderText:@"Транспортное средство"];
        [self createCategoryAtPoint:CGPointMake(self.frame.size.width-50, 10)];
    
        newY=[self getLabelHeight:@"Транспортное средство"]+10;
        
        [self createButtonAtPoint:CGPointMake(10, newY)];
         newY=newY+54;
        [self createViewAtPoint:CGPointMake(0, newY)];
        
         newY=newY+11;
    
    
        [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Госномер отсутствует"];
        [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+22-31/2) andIndexTag:1 andOn:ogrVehicle];
        newY=newY+54;
        [self createViewAtPoint:CGPointMake(0, newY)];
        newY=newY+11;

    
        if(![Singleton sharedInstance].osago.hasDKP){
            [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Нет сканов ПТС"];
            [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+22-31/2) andIndexTag:3 andOn:pts];
            newY=newY+54;
            [self createViewAtPoint:CGPointMake(0, newY)];
            newY=newY+11;
        }
    
        [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Нет сканов СРТС"];
        [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+22-31/2) andIndexTag:4 andOn:srts];
        
        newY=newY+54;
        [self createViewAtPoint:CGPointMake(0, newY)];
        
        newY=newY+11;
        [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Используется с прицепом"];
        [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+22-31/2) andIndexTag:5 andOn:pricep];
        
        newY=newY+54;
        [self createViewAtPoint:CGPointMake(0, newY)];
    
    
    if(!pts){
        newY=newY+ 11;
        if(![self getButtonAtIndex:8]){
            [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:8];
        }else{
             [[self getButtonAtIndex:8]setHidden:NO];
            [[self getButtonAtIndex:8]setFrame:CGRectMake(10, newY, 60, 60)];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"ПТС (1 сторона)"];
        if(![[Singleton sharedInstance].osago.vehiclePassportPageFirstID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:8];
        }
        newY=newY+68;
        
        newY=newY+ 7;
        if(![self getButtonAtIndex:9]){
            [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:9];
        }else{
            [[self getButtonAtIndex:9]setHidden:NO];
            [[self getButtonAtIndex:9]setFrame:CGRectMake(10, newY, 60, 60)];
        }
        if(![[Singleton sharedInstance].osago.vehiclePassportPageSecondID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:9];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"ПТС (2 сторона)"];
        newY=newY+68;
    }else{
        [[self getButtonAtIndex:8]setHidden:YES];
        [[self getButtonAtIndex:9]setHidden:YES];
    }
    if(!srts){
        newY=newY+ 7;
         if(![self getButtonAtIndex:10]){
             [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:10];
         }else{
             [[self getButtonAtIndex:10]setHidden:NO];
             [[self getButtonAtIndex:10]setFrame:CGRectMake(10, newY, 60, 60)];
         }
        if(![[Singleton sharedInstance].osago.vehicleCertificatePageFirstID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:10];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"СРТС (1 сторона)"];
        newY=newY+68;
        
        newY=newY+ 7;
         if(![self getButtonAtIndex:11]){
             [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:11];
         }else{
             [[self getButtonAtIndex:11]setHidden:NO];
             [[self getButtonAtIndex:11]setFrame:CGRectMake(10 , newY, 60, 60)];
         }
        if(![[Singleton sharedInstance].osago.vehicleCertificatePageSecondID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:11];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"СРТС (2 сторона)"];
        newY=newY+68;
    }else{
        [[self getButtonAtIndex:10]setHidden:YES];
        [[self getButtonAtIndex:11]setHidden:YES];
    }
        newY=newY+ 7;
        if(![self getButtonAtIndex:12]){
            [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:12];
        }else{
            [[self getButtonAtIndex:12]setFrame:CGRectMake(10, newY, 60, 60)];
        }
        if(![[Singleton sharedInstance].osago.cardPageFirstID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:12];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"ТО (1 сторона)"];
        newY=newY+68;
  
        newY=newY+ 7;
         if(![self getButtonAtIndex:13]){
             [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:13];
         }else{
             [[self getButtonAtIndex:13]setFrame:CGRectMake(10, newY, 60, 60)];
         }
        if(![[Singleton sharedInstance].osago.cardPageSecondID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:13];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"ТО (2 сторона)"];
        newY=newY+68;
      [self createViewAtPoint:CGPointMake(0, newY)];
     newY=newY+11;
        [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Без ограничения"];
        [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+22-31/2) andIndexTag:2 andOn:!isLimit];
        newY=newY+54;
    
        _totalHeight = newY;
     handler(_totalHeight);
    
}
-(void)drawColor{
    [self setBackgroundColor:[UIColor whiteColor]];
}

-(void)createCategoryAtPoint:(CGPoint)point{
    CategoryView *category = [[CategoryView alloc]initWithFrame:CGRectMake(point.x, point.y, 40, 40)];
    [self addSubview:category];
}

-(void)createHeaderText:(NSString*)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica" size:24.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, self.frame.size.width-50, [self getLabelHeight:string]+5)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:233.f/255.f green:148.f/255.f blue:0.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}

-(void)createAttachment:(CGPoint)point andTag:(NSInteger)index{
    UIButton *attach = [[UIButton alloc]initWithFrame:CGRectMake(point.x, point.y, 35, 35)];
    attach.tag=index;
    [attach setImage:[UIImage imageNamed:@"attach"] forState:UIControlStateNormal];
    [attach addTarget:self action:@selector(attachClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:attach];
}

-(void)createPhotoButtonAtPoint:(CGPoint)point andTag:(NSInteger)index{
    ButtonWithPhoto *photo = [[ButtonWithPhoto alloc]initWithFrame:CGRectMake(point.x, point.y, 60, 60) andTag:index];
    photo.tag=index;
    photo.photoBtn.tag=index;
    [photo.photoBtn addTarget:self action:@selector(buttonClickedAtIndex:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:photo];
}

-(void)createButtonAtPoint:(CGPoint)point{
   
    _btn = [[UIButton alloc]initWithFrame:CGRectMake(point.x, point.y,self.frame.size.width-point.x*2, 40)];
    if([[Singleton sharedInstance].osago.vehicleTarget isEqualToString:@""]){
       
        [_btn setTitle:@"Укажите цель использования..." forState:UIControlStateNormal];
        _btn.tag=100;
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentLeft];

        UIFont *font1 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
 
        NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font1,
                            NSParagraphStyleAttributeName:style,
                            NSForegroundColorAttributeName : [UIColor colorWithRed:97.f/255.f green:97.f/255.f blue:97.f/255.f alpha:0.4]
                            };
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Укажите цель использования..."    attributes:dict1]];
        [_btn setAttributedTitle:attString forState:UIControlStateNormal];
        [_btn addTarget:self action:@selector(openList:) forControlEvents:UIControlEventTouchUpInside];
        _btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }else{
        [_btn setTitle:[Singleton sharedInstance].osago.vehicleTarget forState:UIControlStateNormal];
        _btn.tag=100;
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentLeft];
        
        UIFont *font1 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
        
        NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                                NSFontAttributeName:font1,
                                NSParagraphStyleAttributeName:style,
                                NSForegroundColorAttributeName : [UIColor blueColor]
                                }; // Added line
        
        
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[Singleton sharedInstance].osago.vehicleTarget    attributes:dict1]];
         _btn.titleLabel. numberOfLines = 2;
        [_btn setAttributedTitle:attString forState:UIControlStateNormal];
        [_btn addTarget:self action:@selector(openList:) forControlEvents:UIControlEventTouchUpInside];
        _btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    [self addSubview:_btn];
}
-(void)createViewAtPoint:(CGPoint)point{
    UIView *view= [[UIView alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width, 0.5f)];
    [view setBackgroundColor:[UIColor colorWithRed:151.f/255.f green:151.f/255.f blue:151.f/255.f alpha:1.f]];
    [self addSubview:view];
}

-(void)createLabelAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width-point.x-49, [self getLabelHeight:string])];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:72.f/255.f green:72.f/255.f blue:72.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}


-(void)createSwitcherAtPoint:(CGPoint)point andIndexTag:(NSInteger)index andOn:(BOOL)isON{
    UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(point.x, point.y, 49, 31)];
    [onoff setOn:isON];
    onoff.tag=index;
    [onoff setOnTintColor:[UIColor colorWithRed:243.f/255.f green:180.f/255.f blue:71.f/255.f alpha:1.f]];
    [onoff addTarget: self action: @selector(flip:) forControlEvents: UIControlEventValueChanged];
    [self addSubview: onoff];
}
-(void)buttonClickedAtIndex:(UIButton *)btn{
    [self.delegate vehicleButtonClicked:btn.tag];
}

-(void)flip:(UISwitch*)switcher{
    [self.delegate vehicleSwitcherIsOn:switcher.tag andIsON:switcher.isOn];
}

-(void)openList:(UIButton*)btn{
    [self.delegate needOpenPicker];
    
}
-(void)attachClick:(UIButton *)btn{
    [self.delegate vehicleAttachmentClick:btn.tag];
}
- (CGFloat)getLabelHeight:(NSString*)text
{
    return 44. ;
}

-(void)setImageForButton:(UIImage*)img andTag:(NSInteger)index{
    for (UIButton *btn in self.subviews) {
        if([btn isKindOfClass:[UIButton class]]){
            if(btn.tag == index){
                [btn setImage:img forState:UIControlStateNormal];
            }
        }
    }
}

-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index{
    for (ButtonWithPhoto *photo in self.subviews) {
        if([photo isKindOfClass:[ButtonWithPhoto class]]){
            if(photo.tag == index){
                return photo;
            }
        }
    }
    return nil;
}

@end

