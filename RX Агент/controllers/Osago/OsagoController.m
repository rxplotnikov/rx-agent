//
//  OsagoController.m
//  RX Агент
//
//  Created by RX Group on 14.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "OsagoController.h"
#import "RoundViewChose.h"
#import "PhotoDriver.h"
#import "CardController.h"
#import "UIColor+NavigationColors.h"
#import "RXLoadingView.h"

@interface OsagoController ()<ButtonPhotoDelegate,vehicleDelegate,ownerDelegate,RoundViewDelegate,DriverDelegate,CardDelegate,ConditionsDelegate,UITextViewDelegate>{
    InsurantView *insView;
    VehicleView *vehView;
    OwnerView *ownerView;
    RoundViewChose*roundView;
    PhotoDriver *driverView;
    BankCardView *bankCard;
    ConditionsView *conditionView;
    RXLoadingView *loadView;
    UIDatePicker *datePicker;
    float totalHeight;
    float lastHeight;
    NSMutableArray <PhotoDriver*> *driverArray;
    NSMutableArray *arrayY;
    NSMutableDictionary *targetsDict;
    NSMutableDictionary *termDict;
    UIButton *addButton;
    CGPoint contentOffset;
    bool isScroll;
    UIButton *requestBtn;
}

@end

@implementation OsagoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/VehicleTargets" withBlock:^(NSMutableDictionary *dictionary) {
         self->targetsDict = dictionary;
    }];
    [[RXServerConnector sharedConnector]getDataFromURL:@"https://demo-api.rx-agent.ru/v0/InsurancePeriods" withBlock:^(NSMutableDictionary *dictionary) {
        self->termDict = dictionary;
    }];
    arrayY = [NSMutableArray arrayWithArray:@[@"",@"",@"",@"",@"",@""]];
    [RXServerConnector sharedConnector].delegate=self;
    _navigationBar.delegate=self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

-(void)hideKeyboard{
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([Singleton sharedInstance].model.term){
       [self refreshViews];
    }
    if(!insView){
        [self initViews];
    }
    if([Singleton sharedInstance].osago.vehicleTarget){
        [self refreshViews];
    }
    if([Singleton sharedInstance].osago.cardDict){
         [self refreshViews];
        UIImage *imageType =[NSKeyedUnarchiver unarchiveObjectWithData:[Singleton sharedInstance].osago.cardDict[@"imageType"]];
        NSString *number =[Singleton sharedInstance].osago.cardDict[@"number"];
        NSString *data = [NSString stringWithFormat:@"%@/%@",[Singleton sharedInstance].osago.cardDict[@"month"],[[NSString stringWithFormat:@"%@", [Singleton sharedInstance].osago.cardDict[@"year"]] substringFromIndex:[[NSString stringWithFormat:@"%@", [Singleton sharedInstance].osago.cardDict[@"year"]] length] - 2]];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:[Singleton sharedInstance].osago.cardDict[@"color"]];
        UIImage *imageBackground;
        if([[Singleton sharedInstance].osago.cardDict[@"cardImage"] isKindOfClass:[NSData class]]){
            imageBackground = [NSKeyedUnarchiver unarchiveObjectWithData:[Singleton sharedInstance].osago.cardDict[@"cardImage"]];
        }
        [bankCard setCardType:imageType andNumber:number andDate:data andColor:color andFrame:CGRectMake(_scroll.frame.size.width/2-150, roundView.frame.origin.y + roundView.frame.size.height + 280*[Singleton sharedInstance].model.countDriver , 300, 150) andImage:imageBackground andName:[[NSString stringWithFormat:@"%@ %@",[Singleton sharedInstance].osago.cardDict[@"nameHolder"],[Singleton sharedInstance].osago.cardDict[@"lastNameHolder"]] uppercaseString]];
       
    }
    if([Singleton sharedInstance].osago.tempImage){
        if([Singleton sharedInstance].osago.currentIndexPhoto<4){
            [[insView getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
            if([Singleton sharedInstance].osago.currentIndexPhoto==2){
                [[ownerView getButtonAtIndex:6]setImageForButton:[Singleton sharedInstance].osago.tempImage];
            }
            if([Singleton sharedInstance].osago.currentIndexPhoto==3){
                [[ownerView getButtonAtIndex:7]setImageForButton:[Singleton sharedInstance].osago.tempImage];
            }
        }
        
        if([Singleton sharedInstance].osago.currentIndexPhoto>=4&&[Singleton sharedInstance].osago.currentIndexPhoto<=7){
            [[ownerView getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
            if([Singleton sharedInstance].osago.currentIndexPhoto==6){
                [[insView getButtonAtIndex:2]setImageForButton:[Singleton sharedInstance].osago.tempImage];
            }
            if([Singleton sharedInstance].osago.currentIndexPhoto==7){
                [[insView getButtonAtIndex:3]setImageForButton:[Singleton sharedInstance].osago.tempImage];
            }
        }
        if([Singleton sharedInstance].osago.currentIndexPhoto>=8&&[Singleton sharedInstance].osago.currentIndexPhoto<=13){
              [[vehView getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
        }
        if([Singleton sharedInstance].osago.currentIndexPhoto>=14&&[Singleton sharedInstance].osago.currentIndexPhoto<=16){
            [[driverArray[0] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
            [driverArray[0] createAttachAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto];
        }
        if([Singleton sharedInstance].osago.currentIndexPhoto>=17&&[Singleton sharedInstance].osago.currentIndexPhoto<=19){
            [[driverArray[1] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
            [driverArray[1] createAttachAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto];
        }
        if([Singleton sharedInstance].osago.currentIndexPhoto>=20&&[Singleton sharedInstance].osago.currentIndexPhoto<=22){
            [[driverArray[2] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
            [driverArray[2] createAttachAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto];
        }
        if([Singleton sharedInstance].osago.currentIndexPhoto>=23&&[Singleton sharedInstance].osago.currentIndexPhoto<=25){
            [[driverArray[3] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
            [driverArray[3] createAttachAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto];
        }
        [Singleton sharedInstance].osago.tempImage=nil;
    }
}

-(void)initViews{
    driverArray= [NSMutableArray new];
    
    insView = [[InsurantView alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96)andSettings:[Singleton sharedInstance].osago.isOwner ? 1 : 0 andTimeReg:[Singleton sharedInstance].osago.hasTempReg andDKP:[Singleton sharedInstance].osago.hasDKP andNoDKP:[Singleton sharedInstance].osago.noDKP  withBlock:^(float height) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self->insView setFrame:CGRectMake(0, 10, self.scroll.frame.size.width, height)];
            for (int i =0 ; i<4; i++) {
                [self->insView getButtonAtIndex:i].delegate=self;
            }
            [self->insView drawColor];
            [self refreshHeight];
        });
        
    }];
   
    insView.delegate=self;
    [self.scroll addSubview:insView];
    
    ownerView = [[OwnerView alloc]initWithFrame:CGRectMake(0, insView.totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96) andTimeReg:[Singleton sharedInstance].osago.hasTempReg andDKP:[Singleton sharedInstance].osago.hasDKP andNoDKP:[Singleton sharedInstance].osago.noDKP withBlock:^(float height) {
          dispatch_async(dispatch_get_main_queue(), ^{
        [self->ownerView setFrame:CGRectMake(0, self->insView.totalHeight+20, self.scroll.frame.size.width, height)];
        [self->ownerView drawColor];
        for (int i =4 ; i<=7; i++) {
            [self->ownerView getButtonAtIndex:i].delegate=self;
        }
              });
    }];
    
    ownerView.delegate=self;
    [ownerView setHidden:YES];
    [self.scroll addSubview:ownerView];
    
    vehView = [[VehicleView alloc]initWithFrame:CGRectMake(0, insView.totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96) andOgr:[Singleton sharedInstance].osago.vehicleNumberIsEmpty andPTS:[Singleton sharedInstance].osago.noVehiclePassportPage andSRTS:[Singleton sharedInstance].osago.noVehicleCertificatePage andPricep:[Singleton sharedInstance].model.trailJSON andLimit:[Singleton sharedInstance].isLimit  withBlock:^(float height) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->vehView setFrame:CGRectMake(0, self->insView.totalHeight+20, self.scroll.frame.size.width, height)];
            [self->vehView drawColor];
            for (int i =8 ; i<=13; i++) {
                [self->vehView getButtonAtIndex:i].delegate=self;
            }
        });
    }];
    
    vehView.delegate=self;
     [self.scroll addSubview:vehView];
  
    roundView=[[RoundViewChose alloc]initWithFrame:CGRectMake(0,  vehView.frame.origin.y + vehView.totalHeight+40, self.scroll.frame.size.width, 70)];
   
    [roundView setBackgroundColor:[UIColor whiteColor]];
    roundView.delegate=self;
    if([Singleton sharedInstance].isLimit){
        [roundView setHidden:NO];
    }else{
        [roundView setHidden:YES];
    }
    [self.scroll addSubview:roundView];
    
    NSArray *array =@[@[@14,@15,@16],@[@17,@18,@19],@[@20,@21,@22],@[@23,@24,@25]];
  
        for (int i=0; i<4; i++) {
            driverView = [[PhotoDriver alloc]initWithFrame:CGRectMake(0,  roundView.frame.origin.y + 90 + 280*i, self.scroll.frame.size.width, 260) andTitle:i andTags:array[i]  withBlock:^(float height) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->driverView setFrame:CGRectMake(0,  self->roundView.frame.origin.y + 90 + 280*i, self.scroll.frame.size.width, 260)];
                    
                    if([Singleton sharedInstance].model.countDriver<i+1){
                        [self->driverView setHidden:YES];
                    }
                });
            }];
            driverView.delegate=self;
            [driverArray addObject:driverView];
            [self.scroll addSubview:driverView];
    }
   
    for (int i=0; i<driverArray.count; i++) {
        [driverArray[i] drawColor];
        [driverArray[i] setFrame:CGRectMake(0,  self->roundView.frame.origin.y + 90 + 280*i, self.scroll.frame.size.width, 260)];
        for (int j=0; j<3; j++) {
            [driverArray[i] getButtonAtIndex:[array[i][j]integerValue]].delegate=self;
        }
        if([Singleton sharedInstance].model.countDriver<i+1){
            [driverArray[i] setHidden:YES];
        }else{
            [driverArray[i] setHidden:NO ];
        }
    }
    addButton = [[UIButton alloc]initWithFrame:CGRectMake(_scroll.frame.size.width/2-135, roundView.frame.origin.y + roundView.frame.size.height+20 + 280*[Singleton sharedInstance].model.countDriver , 270, 50)];
    [addButton setImage:[UIImage imageNamed:@"Group 47"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addCard) forControlEvents:UIControlEventTouchUpInside];
    [self.scroll addSubview:addButton];
    bankCard = [[BankCardView alloc]initWithFrame:CGRectMake(_scroll.frame.size.width/2-135, roundView.frame.origin.y + roundView.frame.size.height + 280*[Singleton sharedInstance].model.countDriver , 270, 50)];
    [bankCard setHidden:YES];
    bankCard.delegate=self;
    [self.scroll addSubview:bankCard];
   
    conditionView = [[ConditionsView alloc]initWithFrame:CGRectMake(0, addButton.frame.origin.y + addButton.frame.size.height+20 , self.scroll.frame.size.width, 100) andStartDate:[Singleton sharedInstance].model.startDate andPeriod:[Singleton sharedInstance].model.term andName:[Singleton sharedInstance].osago.creatorFullName andPhone:[Singleton sharedInstance].osago.creatorPhone andComment:[Singleton sharedInstance].osago.agentComment isFast:[Singleton sharedInstance].osago.isPriority withBlock:^(float height) {
         dispatch_async(dispatch_get_main_queue(), ^{
             self->conditionView.datefield.delegate=self;
             self->conditionView.numberField.delegate=self;
             self->conditionView.nameField.delegate=self;
             self->conditionView.textView.delegate=self;
            [self->conditionView setFrame:CGRectMake(0, self->addButton.frame.origin.y + self->addButton.frame.size.height+20, self.scroll.frame.size.width, height)];
            [self->conditionView drawColor];
             self->requestBtn = [[UIButton alloc]initWithFrame:CGRectMake(self->_scroll.frame.size.width/2-self->_scroll.frame.size.width*0.8/2, self->conditionView.frame.origin.y+self->conditionView.frame.size.height+20, self->_scroll.frame.size.width*0.8, self->_scroll.frame.size.height*0.07)];
             [self->requestBtn setBackgroundImage:[UIImage imageNamed:@"next_btn"] forState:UIControlStateNormal];
             [self->requestBtn addTarget:self action:@selector(issue) forControlEvents:UIControlEventTouchUpInside];
             [self->requestBtn setTitle:@"Оформить" forState:UIControlStateNormal];
             [self->requestBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
             [self->requestBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica Medium" size:23.f]];
             [self.scroll addSubview:self->requestBtn];
            
         });
    }];
    conditionView.delegate=self;
    [self.scroll addSubview:conditionView];
    
    [self updateArrayY];
    [self refreshHeight];
}
-(void)updateArrayY{
    if([Singleton sharedInstance].osago.isOwner){
        if([Singleton sharedInstance].isLimit){
            [arrayY replaceObjectAtIndex:0 withObject:[NSNumber numberWithDouble:insView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:1 withObject:[NSNumber numberWithDouble:vehView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:2 withObject:[NSNumber numberWithDouble:roundView.frame.origin.y]];
            if([Singleton sharedInstance].osago.cardDict){
                [arrayY replaceObjectAtIndex:3 withObject:[NSNumber numberWithDouble:bankCard.frame.origin.y]];
            }else{
                [arrayY replaceObjectAtIndex:3 withObject:[NSNumber numberWithDouble:addButton.frame.origin.y]];
            }
            [arrayY replaceObjectAtIndex:4 withObject:[NSNumber numberWithDouble:conditionView.frame.origin.y]];
        }else{
            [arrayY replaceObjectAtIndex:0 withObject:[NSNumber numberWithDouble:insView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:1 withObject:[NSNumber numberWithDouble:vehView.frame.origin.y]];
            if([Singleton sharedInstance].osago.cardDict){
                [arrayY replaceObjectAtIndex:2 withObject:[NSNumber numberWithDouble:bankCard.frame.origin.y]];
            }else{
                [arrayY replaceObjectAtIndex:2 withObject:[NSNumber numberWithDouble:addButton.frame.origin.y]];
            }
            [arrayY replaceObjectAtIndex:3 withObject:[NSNumber numberWithDouble:conditionView.frame.origin.y]];
        }
    }else{
        if([Singleton sharedInstance].isLimit){
            [arrayY replaceObjectAtIndex:0 withObject:[NSNumber numberWithDouble:insView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:1 withObject:[NSNumber numberWithDouble:ownerView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:2 withObject:[NSNumber numberWithDouble:vehView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:3 withObject:[NSNumber numberWithDouble:roundView.frame.origin.y]];
            if([Singleton sharedInstance].osago.cardDict){
                [arrayY replaceObjectAtIndex:4 withObject:[NSNumber numberWithDouble:bankCard.frame.origin.y]];
            }else{
                [arrayY replaceObjectAtIndex:4 withObject:[NSNumber numberWithDouble:addButton.frame.origin.y]];
            }
            [arrayY replaceObjectAtIndex:5 withObject:[NSNumber numberWithDouble:conditionView.frame.origin.y]];
        }else{
            [arrayY replaceObjectAtIndex:0 withObject:[NSNumber numberWithDouble:insView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:1 withObject:[NSNumber numberWithDouble:ownerView.frame.origin.y]];
            [arrayY replaceObjectAtIndex:2 withObject:[NSNumber numberWithDouble:vehView.frame.origin.y]];
            if([Singleton sharedInstance].osago.cardDict){
                [arrayY replaceObjectAtIndex:3 withObject:[NSNumber numberWithDouble:bankCard.frame.origin.y]];
            }else{
                [arrayY replaceObjectAtIndex:3 withObject:[NSNumber numberWithDouble:addButton.frame.origin.y]];
            }
            [arrayY replaceObjectAtIndex:4 withObject:[NSNumber numberWithDouble:conditionView.frame.origin.y]];
        }
        
    }
}

//-(void)colorNavigationAtIndex:(NSInteger)index{
//    [_navigationBar setColorForItem:0 andColor:[UIColor customGrayColor]];
//    [_navigationBar setColorForItem:1 andColor:[UIColor customGrayColor]];
//    [_navigationBar setColorForItem:2 andColor:[UIColor customGrayColor]];
//    [_navigationBar setColorForItem:3 andColor:[UIColor customGrayColor]];
//    [_navigationBar setColorForItem:4 andColor:[UIColor customGrayColor]];
//    [_navigationBar setColorForItem:5 andColor:[UIColor customGrayColor]];
//    if(index==0){
//        if([Singleton sharedInstance].firstDone){
//            [_navigationBar setColorForItem:0 andColor:[UIColor customGreenColor]];
//        }else{
//            [_navigationBar setColorForItem:0 andColor:[UIColor customYellowColor]];
//        }
//    }
//
//    if(index==1){
//        if([Singleton sharedInstance].osago.isOwner){
//            if([Singleton sharedInstance].thirdDone){
//                [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//            }else{
//                [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//            }
//        }else{
//            if([Singleton sharedInstance].secondDone){
//                 [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//            }else{
//                 [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//            }
//        }
//    }
//    if(index==2){
//        if([Singleton sharedInstance].osago.isOwner){
//
//        }else{
//            if([Singleton sharedInstance].thirdDone){
//                [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//            }else{
//                [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//            }
//        }
//    }
//}
-(void)refreshViews{
    [insView initializeAndSetting:[Singleton sharedInstance].osago.isOwner ? 1 : 0 andTimeReg:[Singleton sharedInstance].osago.hasTempReg andDKP:[Singleton sharedInstance].osago.hasDKP andNoDKP:[Singleton sharedInstance].osago.noDKP withBlock:^(float height) {
        dispatch_async(dispatch_get_main_queue(), ^{
          
            [self->insView setFrame:CGRectMake(0, 10, self.scroll.frame.size.width, height)];
            for (int i =0 ; i<4; i++) {
                [self->insView getButtonAtIndex:i].delegate=self;
            }
            
            [self->insView drawColor];
            
        });
    }];
    
    [ownerView initializeAndSettingTimeReg:[Singleton sharedInstance].osago.hasTempReg andDKP:[Singleton sharedInstance].osago.hasDKP andNoDKP:[Singleton sharedInstance].osago.noDKP withBlock:^(float height) {
        
        
            [self->ownerView setFrame:CGRectMake(0, self->insView.frame.origin.y+self->insView.totalHeight+20, self.scroll.frame.size.width, height)];
            for (int i =4 ; i<=7; i++) {
                [self->ownerView getButtonAtIndex:i].delegate=self;
            }
            [self->ownerView drawColor];
      
       
    }];
    [ownerView setHidden:[Singleton sharedInstance].osago.isOwner];
    
    [vehView initializeandOgr:[Singleton sharedInstance].osago.vehicleNumberIsEmpty andPTS:[Singleton sharedInstance].osago.noVehiclePassportPage andSRTS:[Singleton sharedInstance].osago.noVehicleCertificatePage andPricep:[Singleton sharedInstance].model.trailJSON andLimit:[Singleton sharedInstance].isLimit withBlock:^(float height) {
        
         if([Singleton sharedInstance].osago.isOwner)   {
             [self->vehView setFrame:CGRectMake(0,self->insView.frame.origin.y+self->insView.totalHeight+20, self.scroll.frame.size.width, height)];
         }else{
             [self->vehView setFrame:CGRectMake(0,self->ownerView.frame.origin.y+ self->ownerView.totalHeight+20,self.scroll.frame.size.width, height)];
         }
        for (int i =8 ; i<=13; i++) {
            [self->vehView getButtonAtIndex:i].delegate=self;
        }
        [self->vehView drawColor];
        [self refreshHeight];
        
    }];
  
        [self->roundView setFrame:CGRectMake(0,self->vehView.frame.origin.y + self->vehView.frame.size.height +20, self.scroll.frame.size.width, 70)];
    if([Singleton sharedInstance].isLimit){
         [roundView setHidden:NO];
        if([Singleton sharedInstance].osago.cardDict){
            [addButton setHidden:YES];
            [bankCard setHidden:NO];
            [bankCard setFrame:CGRectMake(_scroll.frame.size.width/2-(self.scroll.frame.size.width*0.8)/2, roundView.frame.origin.y + roundView.frame.size.height+20 + 280*[Singleton sharedInstance].model.countDriver, self.scroll.frame.size.width*0.8, self.scroll.frame.size.width*0.8*0.5)];

        }else{
            [addButton setHidden:NO];
             [bankCard setHidden:YES];
            [addButton setFrame:CGRectMake(_scroll.frame.size.width/2-135, roundView.frame.origin.y + 90 + 280*[Singleton sharedInstance].model.countDriver, 270, 50)];
           
        }
        for (int i=0; i<driverArray.count; i++) {
            [driverArray[i] setFrame:CGRectMake(0,  self->roundView.frame.origin.y + 90 + 280*i, self.scroll.frame.size.width, 260)];
            if([Singleton sharedInstance].model.countDriver<i+1){
                [driverArray[i] setHidden:YES];
            }else{
                [driverArray[i] setHidden:NO ];
            }
        }
        
    }else{
        if([Singleton sharedInstance].osago.cardDict){
            [addButton setHidden:YES];
            [bankCard setHidden:NO];
            [bankCard setFrame:CGRectMake(_scroll.frame.size.width/2-(self.scroll.frame.size.width*0.8)/2, vehView.frame.origin.y + vehView.frame.size.height+20 , self.scroll.frame.size.width*0.8, self.scroll.frame.size.width*0.8*0.5)];
           
        }else{
            [addButton setHidden:NO];
            [bankCard setHidden:YES];
            [addButton setFrame:CGRectMake(_scroll.frame.size.width/2-135, vehView.frame.origin.y + vehView.frame.size.height+20 , 270, 50)];
           
        }
       
        [roundView setHidden:YES];
        for (int i=0; i<driverArray.count; i++) {
            [driverArray[i] setHidden:YES];
        }
    }
   
    [conditionView initializeWithstartDate:[Singleton sharedInstance].model.startDate andPeriod:[Singleton sharedInstance].model.term andName:[Singleton sharedInstance].osago.creatorFullName andPhone:[Singleton sharedInstance].osago.creatorPhone andComment:[Singleton sharedInstance].osago.agentComment isFast:[Singleton sharedInstance].osago.isPriority withBlock:^(float height) {
        self->conditionView.datefield.delegate=self;
        self->conditionView.numberField.delegate=self;
        self->conditionView.nameField.delegate=self;
        self->conditionView.textView.delegate=self;
        if([Singleton sharedInstance].osago.cardDict){
            [self->conditionView setFrame:CGRectMake(0, self->bankCard.frame.origin.y+self->bankCard.frame.size.height+20, self.scroll.frame.size.width, height)];
        }else{
            [self->conditionView setFrame:CGRectMake(0, self->addButton.frame.origin.y+self->addButton.frame.size.height+20, self.scroll.frame.size.width, height)];
        }
       
    }];
    [requestBtn setFrame:CGRectMake(_scroll.frame.size.width/2-_scroll.frame.size.width*0.8/2, conditionView.frame.origin.y+conditionView.frame.size.height+20,_scroll.frame.size.width*0.8, _scroll.frame.size.height*0.07)];
    [self updateArrayY];
}

-(void)refreshHeight{
   
    totalHeight = 10+insView.totalHeight+20+vehView.totalHeight+20+conditionView.totalHeight+20+_scroll.frame.size.height*0.07+20;
    
    if([Singleton sharedInstance].osago.cardDict){
        totalHeight = totalHeight+self.scroll.frame.size.width*0.8*0.5 +20;
    }else{
        totalHeight = totalHeight +70;
    }
    
    if(![Singleton sharedInstance].osago.isOwner){
        totalHeight= totalHeight+ownerView.totalHeight+20;
    }
    if([Singleton sharedInstance].isLimit){
        totalHeight=totalHeight + 280*[Singleton sharedInstance].model.countDriver;
         totalHeight= totalHeight+roundView.frame.size.height+20;
    }
    
    _scroll.contentSize = CGSizeMake(_scroll.frame.size.width, totalHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Navigation
-(void)selectItemAtIndex:(NSInteger)index{
    [_scroll setContentOffset:CGPointMake(0, [arrayY[index]floatValue]) animated:YES];
}

#pragma mark Delegates
//Делегаты для Страхователя
-(void)loadIsOver{
    [self refreshViews];
}

-(void)progressLoading:(float)progress atIndex:(NSInteger)index{
        [UIView animateWithDuration:1.f animations:^{
            if(index<4){
                [[self->insView getButtonAtIndex:index]setProgress:progress*100];
            }
            if(index>=4&&index<=7){
                 [[self->ownerView getButtonAtIndex:index]setProgress:progress*100];
            }
            if(index>=8&&index<=13){
                [[self->vehView getButtonAtIndex:index]setProgress:progress*100];
            }
            if(index>=14&&index<=16){
                [[self->driverArray[0] getButtonAtIndex:index] setProgress:progress*100];
            }
            if(index>=17&&index<=19){
                [[self->driverArray[1] getButtonAtIndex:index] setProgress:progress*100];
            }
            if(index>=20&&index<=22){
                [[self->driverArray[2] getButtonAtIndex:index] setProgress:progress*100];
            }
            if(index>=23&&index<=25){
                [[self->driverArray[3] getButtonAtIndex:index] setProgress:progress*100];
            }
        }];
  
}

-(void)switcherIsOn:(NSInteger)index isOn:(BOOL)isOn{
    
        if(index==0){
            [Singleton sharedInstance].osago.isOwner=isOn;
            [_navigationBar initialize];
        }
        
        if(index==1){
            [Singleton sharedInstance].osago.hasTempReg=isOn;
        }
        
        if(index==2){
            [Singleton sharedInstance].osago.hasDKP=isOn;
        }
    
        if(index==3){
            [Singleton sharedInstance].osago.noDKP=isOn;
        }
        
         [self refreshViews];
   
}

-(void)attachmentClicked:(NSInteger)index{
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Изменить фото"];
}

-(void)buttonClicked:(NSInteger)index{
    
    if(![[Singleton sharedInstance].osago.documentPageFirstID isEqualToString:@""]&&index==0){
        [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.documentPageSecondID isEqualToString:@""]&&index==1){
        [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.saleContractPageID isEqualToString:@""]&&index==2){
       [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.tempRegPageID isEqualToString:@""]&&index==3){
       [self openPhotoAtIndex:index];
    }
    
        [Singleton sharedInstance].osago.currentIndexPhoto=index;
       [self showAlertAction:@"Добавить фото"];
    
}
//Делегаты для владельца

-(void)ownerSwitcherIsOn:(NSInteger)index andIsON:(BOOL)isOn{
    if(index==1){
        [Singleton sharedInstance].osago.hasTempReg=isOn;
    }
    
    if(index==2){
        [Singleton sharedInstance].osago.hasDKP=isOn;
    }
    
    if(index==3){
        [Singleton sharedInstance].osago.noDKP=isOn;
    }
    [self refreshViews];
}

-(void)ownerButtonClicked:(NSInteger)index{
    
    if(![[Singleton sharedInstance].osago.ownerDocumentPageFirstID isEqualToString:@""]&&index==4){
        [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.ownerDocumentPageSecondID isEqualToString:@""]&&index==5){
        [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.saleContractPageID isEqualToString:@""]&&index==6){
        [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.tempRegPageID isEqualToString:@""]&&index==7){
        [self openPhotoAtIndex:index];
    }
   
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Добавить фото"];
}

-(void)ownerAttachmentClick:(NSInteger)index{
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Изменить фото"];
}

//Делегаты для ТС
-(void)vehicleAttachmentClick:(NSInteger)index{
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Изменить фото"];
}

-(void)vehicleSwitcherIsOn:(NSInteger)index andIsON:(BOOL)isON{
    
    if(index==1){
        [Singleton sharedInstance].osago.vehicleNumberIsEmpty=isON;
    }
    if(index==2){
        [Singleton sharedInstance].isLimit=!isON;
        [_navigationBar initialize];
    }
    if(index==3){
        [Singleton sharedInstance].osago.noVehiclePassportPage=isON;
        if([Singleton sharedInstance].osago.noVehicleCertificatePage){
            [Singleton sharedInstance].osago.noVehicleCertificatePage=!isON;
        }
    }
    
    if(index==4){
        [Singleton sharedInstance].osago.noVehicleCertificatePage=isON;
        if([Singleton sharedInstance].osago.noVehiclePassportPage){
         [Singleton sharedInstance].osago.noVehiclePassportPage=!isON;
        }
    }
    
    if(index==5){
        [Singleton sharedInstance].model.trailJSON=isON;
    }
     [self refreshViews];
}

-(void)vehicleButtonClicked:(NSInteger)index{
    if(![[Singleton sharedInstance].osago.vehiclePassportPageFirstID isEqualToString:@""]&&index==8){
      [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.vehiclePassportPageSecondID isEqualToString:@""]&&index==9){
        [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.vehicleCertificatePageFirstID isEqualToString:@""]&&index==10){
       [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.vehicleCertificatePageSecondID isEqualToString:@""]&&index==11){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.cardPageFirstID isEqualToString:@""]&&index==12){
       [self openPhotoAtIndex:index];
    }
    
    if(![[Singleton sharedInstance].osago.cardPageSecondID isEqualToString:@""]&&index==13){
        [self openPhotoAtIndex:index];
    }
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Добавить фото"];
}
//делегат для Водителей
-(void)buttonSelected:(NSInteger)index
{
    [Singleton sharedInstance].model.countDriver = index+1;
    [self refreshViews];
}
-(void)buttonClickedAtIndex:(NSInteger)index{
  
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][0][@"driverLicensePageFirstID"]isEqualToString:@""] &&index==14){
         [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][0][@"driverLicensePageSecondID"]isEqualToString:@""]&&index==15){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][0][@"scanTranslationPageID"]isEqualToString:@""]&&index==16){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][1][@"driverLicensePageFirstID"]isEqualToString:@""]&&index==17){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][1][@"driverLicensePageSecondID"]isEqualToString:@""]&&index==18){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][1][@"scanTranslationPageID"]isEqualToString:@""]&&index==19){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][2][@"driverLicensePageFirstID"]isEqualToString:@""]&&index==20){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][2][@"driverLicensePageSecondID"]isEqualToString:@""]&&index==21){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][2][@"scanTranslationPageID"]isEqualToString:@""]&&index==22){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][3][@"driverLicensePageFirstID"]isEqualToString:@""]&&index==23){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][3][@"driverLicensePageSecondID"]isEqualToString:@""]&&index==24){
        [self openPhotoAtIndex:index];
    }
    if(![[Singleton sharedInstance].osago.dictionary[@"drivers"][3][@"scanTranslationPageID"]isEqualToString:@""]&&index==25){
        [self openPhotoAtIndex:index];
    }
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Добавить фото"];
}
-(void)attachmentDriverAtIndex:(NSInteger)index{
    [Singleton sharedInstance].osago.currentIndexPhoto=index;
    [self showAlertAction:@"Изменить фото"];
}
//карта
-(void)cardNeedEdit{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Изменить"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self performSegueWithIdentifier:@"addCard" sender:nil];
                                                      }]];
    
   
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Отмена"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [alertController dismissViewControllerAnimated:YES completion:nil];
                                                      }]];
    
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = self.view;
    popPresenter.sourceRect = self.view.bounds;
    [self presentViewController:alertController animated:YES completion:nil];
}
//Условия
-(void)switcherAction:(UISwitch *)switcher isOn:(BOOL)isON{
    [Singleton sharedInstance].osago.isPriority=isON;
    [self refreshViews];
}
-(void)needOpenTable{
     [self performSegueWithIdentifier:@"showDetailTerm" sender:nil];
}
-(void)openPhotoAtIndex:(NSInteger)index{
    NYTExamplePhoto *photo = [NYTExamplePhoto new];
    photo.image=[Singleton sharedInstance].osago.tempPhoto[index];
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] init];
    [photosViewController displayPhoto:photo animated:YES];
    [self presentViewController:photosViewController animated:YES completion:nil];
    return;
}

-(void)needOpenPicker{
   [self performSegueWithIdentifier:@"showDetail" sender:nil];
}

-(void)addCard{
    [self performSegueWithIdentifier:@"addCard" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showDetail"]){
        DetailController *DC = [segue destinationViewController];
        DC.contentArray = targetsDict[@"vehicleTargets"];
        DC.dataType=10;
        [self presentViewController:DC animated:YES completion:nil];
    }
    if([segue.identifier isEqualToString:@"showDetailTerm"]){
        DetailController *DC = [segue destinationViewController];
        DC.contentArray = termDict[@"insurancePeriods"];
        DC.dataType=7;
        [self presentViewController:DC animated:YES completion:nil];
    }
    if([segue.identifier isEqualToString:@"addCard"]){
        CardController *CC = [segue destinationViewController];
        CC.enterType=1;
        [self presentViewController:CC animated:YES completion:nil];
    }
    
}
-(void)showAlertAction:(NSString *)message{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Камера"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                          CameraViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"camera"];
                                                          [self presentViewController:vc animated:YES completion:nil];
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Галерея"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
                                                              UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
                                                              imagePickerController.delegate = self;
                                                              imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                              [self presentViewController:imagePickerController animated:YES completion:nil];
                                                          }
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Отмена"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [alertController dismissViewControllerAnimated:YES completion:nil];
                                                      }]];
    
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = self.view;
    popPresenter.sourceRect = self.view.bounds;
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage* image = info[UIImagePickerControllerOriginalImage];
    if(image){
        dispatch_async(dispatch_get_main_queue(), ^{
            [Singleton sharedInstance].osago.tempImage = image;
            if([Singleton sharedInstance].osago.tempImage){
                if([Singleton sharedInstance].osago.currentIndexPhoto<4){
                    [[self->insView getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
                if([Singleton sharedInstance].osago.currentIndexPhoto>=4&&[Singleton sharedInstance].osago.currentIndexPhoto<=7){
                    [[self->ownerView getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
                if([Singleton sharedInstance].osago.currentIndexPhoto>=8&&[Singleton sharedInstance].osago.currentIndexPhoto<=13){
                    [[self->vehView getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
                if([Singleton sharedInstance].osago.currentIndexPhoto>=14&&[Singleton sharedInstance].osago.currentIndexPhoto<=16){
                    [[self->driverArray[0] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
                if([Singleton sharedInstance].osago.currentIndexPhoto>=17&&[Singleton sharedInstance].osago.currentIndexPhoto<=19){
                    [[self->driverArray[1] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
                if([Singleton sharedInstance].osago.currentIndexPhoto>=20&&[Singleton sharedInstance].osago.currentIndexPhoto<=22){
                    [[self->driverArray[2] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
                if([Singleton sharedInstance].osago.currentIndexPhoto>=23&&[Singleton sharedInstance].osago.currentIndexPhoto<=25){
                    [[self->driverArray[3] getButtonAtIndex:[Singleton sharedInstance].osago.currentIndexPhoto]loadImageToServer:[Singleton sharedInstance].osago.tempImage];
                }
            }
        });
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)back:(id)sender {
    CATransition *transition = [[CATransition alloc] init];
    transition.duration =0.5;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    contentOffset = self.scroll.contentOffset;
    CGPoint newOffset;
    newOffset.x = contentOffset.x;
    newOffset.y = contentOffset.y;
    //check push return in keyboar
    if(!isScroll){
        //180 is height of keyboar
        newOffset.y += 220;
        isScroll=YES;
    }
    [self.scroll setContentOffset:newOffset animated:YES];
    if ([textView.text isEqualToString:@"Комментарий (пожелания к заявке)"])
    {
        textView.text = @"";
        [textView setTextColor:[UIColor blackColor]];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    isScroll = NO;
    [self.scroll setContentOffset:contentOffset animated:YES];
    [textView endEditing:true];
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Комментарий (пожелания к заявке)";
         [textView setTextColor:[UIColor lightGrayColor]];
    }
    [textView resignFirstResponder];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:conditionView.numberField]){
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
         [Singleton sharedInstance].osago.creatorPhone=[textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *stringWithoutSpaces = [[Singleton sharedInstance].osago.creatorPhone
                                         stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *formated= [stringWithoutSpaces
                             stringByReplacingOccurrencesOfString:@"(" withString:@""];
        NSString *formated2= [formated
                              stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        NSString *formated3= [formated2
                              stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [Singleton sharedInstance].osago.creatorPhoneJSON =formated3;
    BOOL deleting = [newText length] < [textField.text length];

    NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
    NSUInteger digits = [stripppedNumber length];

    if (digits > 10)
        stripppedNumber = [stripppedNumber substringToIndex:10];

    UITextRange *selectedRange = [textField selectedTextRange];
    NSInteger oldLength = [textField.text length];
    if (digits == 0)
        textField.text = @"";
    else if (digits < 3 || (digits == 3 && deleting))
        textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
    else if (digits < 6 || (digits == 6 && deleting))
        textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
    else
        textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
    UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
    UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
    [textField setSelectedTextRange:newRange];
   
    }
    if([textField isEqual:conditionView.nameField]){
         [Singleton sharedInstance].osago.creatorFullName=[textField.text stringByReplacingCharactersInRange:range withString:string];
        return YES;
    }
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
   
    if([sender isEqual:conditionView.datefield]){
        datePicker=[[UIDatePicker alloc]init];
        datePicker.datePickerMode=UIDatePickerModeDate;
        NSDate *date = [NSDate dateWithTimeInterval:5270400 sinceDate:[NSDate date]];
        datePicker.date =[NSDate dateWithTimeInterval:172800 sinceDate:[NSDate date]];
        
        [datePicker setMaximumDate:date];
        [datePicker setMinimumDate:[NSDate dateWithTimeInterval:86400 sinceDate:[NSDate date]]];
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"ru"]];
        [datePicker addTarget:self action:@selector(done) forControlEvents:UIControlEventValueChanged];
        
        conditionView.datefield.inputView=datePicker;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
        [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
        dateFormat.dateStyle = NSDateFormatterLongStyle;
        NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
        [conditionView.datefield setText:[NSString stringWithFormat:@"%@",dateStri]];
        [Singleton sharedInstance].model.startDate = [NSString stringWithFormat:@"%@",dateStri];
        
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        [dateFormat2 setDateFormat:@"YYYY-MM-dd"];
        NSString *dateStri2=[dateFormat2 stringFromDate:datePicker.date];
        [Singleton sharedInstance].model.startDateJSON = [NSString stringWithFormat:@"%@",dateStri2];
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setTintColor:[UIColor grayColor]];
        UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Готово"
                                                                 style:UIBarButtonItemStyleDone
                                                                target:self
                                                                action:@selector(hideKeyboard)];
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
        
        [conditionView.datefield setInputAccessoryView:toolBar];
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
      [Singleton sharedInstance].osago.agentComment=[textView.text stringByReplacingCharactersInRange:range withString:text];
    return YES;
}

-(void)done
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
    dateFormat.dateStyle = NSDateFormatterLongStyle;
    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
    [conditionView.datefield setText:[NSString stringWithFormat:@"%@",dateStri]];
    [Singleton sharedInstance].model.startDate = [NSString stringWithFormat:@"%@",dateStri];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"YYYY-MM-dd"];
    NSString *dateStri2=[dateFormat2 stringFromDate:datePicker.date];
    [Singleton sharedInstance].model.startDateJSON = [NSString stringWithFormat:@"%@",dateStri2];
}
-(void)issue{
    if([self isDone]){
        loadView = [[RXLoadingView alloc]initWithText:@"Отправка запроса на сервер..."];
        [loadView showView];
        NSMutableArray *arrayDrivers=[NSMutableArray new];
        if([Singleton sharedInstance].isLimit){
            for (int i =0; i<[Singleton sharedInstance].model.countDriver; i++) {
                [arrayDrivers addObject:[Singleton sharedInstance].osago.dictionary[@"drivers"][i]];
            }
        }
        NSDictionary *issue = @{    @"ownerIsIndividual":@"true",
                                    @"insurantIsOwner": [Singleton sharedInstance].osago.isOwner ? @"true":@"false",
                                    @"ownerHasTemporaryRegistration": [Singleton sharedInstance].osago.hasTempReg ? @"true":@"false",
                                    @"hasSaleContract": [Singleton sharedInstance].osago.hasDKP ? @"true":@"false",
                                    @"noSaleContractPage": [Singleton sharedInstance].osago.noDKP ? @"true":@"false",
                                    @"insurantDocumentPageFirstID": [Singleton sharedInstance].osago.documentPageFirstID,
                                    @"insurantDocumentPageSecondID": [Singleton sharedInstance].osago.documentPageSecondID,
                                    @"ownerTemporaryRegistrationPageID": [Singleton sharedInstance].osago.tempRegPageID,
                                    @"saleContractPageID":  [Singleton sharedInstance].osago.saleContractPageID,
                                    
                                    @"ownerDocumentPageFirstID": [Singleton sharedInstance].osago.ownerDocumentPageFirstID,
                                    @"ownerDocumentPageSecondID": [Singleton sharedInstance].osago.ownerDocumentPageSecondID,
                                    
                                    @"vehicleCategory": [NSString stringWithFormat:@"%ld",(long) [Singleton sharedInstance].model.categoryID],
                                    @"vehicleTarget": [Singleton sharedInstance].osago.vehicleTargetJSON,
                                    @"vehicleSeats" : @"",
                                    @"vehicleNumberIsEmpty":![Singleton sharedInstance].osago.vehicleNumberIsEmpty ? @"true":@"false",
                                    @"withTrailer": [Singleton sharedInstance].model.trailJSON ? @"true":@"false",
                                    @"noVehiclePassportPage": [Singleton sharedInstance].osago.noVehiclePassportPage ? @"true":@"false",
                                    @"noVehicleCertificatePage": [Singleton sharedInstance].osago.noVehicleCertificatePage ? @"true":@"false",
                                    @"vehiclePassportPageFirstID": [Singleton sharedInstance].osago.vehiclePassportPageFirstID,
                                    @"vehiclePassportPageSecondID": [Singleton sharedInstance].osago.vehiclePassportPageSecondID,
                                    @"vehicleCertificatePageFirstID": [Singleton sharedInstance].osago.vehicleCertificatePageFirstID,
                                    @"vehicleCertificatePageSecondID": [Singleton sharedInstance].osago.vehicleCertificatePageSecondID,
                                    @"cardPageFirstID": [Singleton sharedInstance].osago.cardPageFirstID,
                                    @"cardPageSecondID": [Singleton sharedInstance].osago.cardPageSecondID,
                                    @"unlimitedDrivers": ![Singleton sharedInstance].isLimit ? @"true":@"false",
                                    @"drivers":  arrayDrivers,
                                    @"payCardPaymentSystem": [Singleton sharedInstance].osago.cardDict[@"payCardPaymentSystem"],
                                    @"payCardFirstName": [Singleton sharedInstance].osago.cardDict[@"nameHolder"],
                                    @"payCardLastName": [Singleton sharedInstance].osago.cardDict[@"lastNameHolder"],
                                    @"payCardMonth": [Singleton sharedInstance].osago.cardDict[@"month"],
                                    @"payCardYear": [Singleton sharedInstance].osago.cardDict[@"year"],
                                    @"payCardNumber": [Singleton sharedInstance].osago.cardDict[@"number"],
                                    @"payCardCVC": [Singleton sharedInstance].osago.cardDict[@"cvv"],
                                    
                                    @"startDate": [Singleton sharedInstance].model.startDateJSON,
                                    @"insurancePeriod": [Singleton sharedInstance].model.periodJSON,
                                    @"creatorFullName": [Singleton sharedInstance].osago.creatorFullName,
                                    @"creatorPhone": [Singleton sharedInstance].osago.creatorPhoneJSON,
                                    @"isShort": @"true",
                                    @"isPriority": [Singleton sharedInstance].osago.isPriority ? @"true":@"false",
                                    @"agentComment": [Singleton sharedInstance].osago.agentComment
                                    };

        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:issue options:0 error:nil];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        [[RXServerConnector sharedConnector]postJsonToServer:myString posturl:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies" withBlock:^(NSDictionary *json) {
            [[RXServerConnector sharedConnector]patchRequestFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies/%@/dispatch",json[@"eosagoPolicy"][@"id"]] withBlock:^(NSMutableDictionary *dictionary) {
                [self dismissViewControllerAnimated:YES completion:^{
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"closeCalc"
                     object:self];
                    [self->loadView dissmissView];
                    [[Singleton sharedInstance]refreshAll];
                }];
            } withErrorBlock:^(NSString *error) {
                 [self->loadView dissmissView];
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Внимание!"
                                             message:error
                                             preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self dismissViewControllerAnimated:YES completion:^{
                                                    [[NSNotificationCenter defaultCenter]
                                                     postNotificationName:@"closeCalc"
                                                     object:self];
                                                    [[Singleton sharedInstance]refreshAll];
                                                }];
                                            }];
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            }];
            
        }];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Внимние!" message:@"Заполнены не все поля" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
   
    
}
-(BOOL)isDone{
    if([Singleton sharedInstance].firstDone&&[Singleton sharedInstance].thirdDone&&[Singleton sharedInstance].fiveDone&&[Singleton sharedInstance].sixDone){
        if([Singleton sharedInstance].osago.isOwner){
            if([Singleton sharedInstance].isLimit){
                if ([Singleton sharedInstance].fourthDone) {
                    return YES;
                }else{
                    return NO;
                }
            }else{
                return YES;
            }
        }else{
            if([Singleton sharedInstance].isLimit){
                if ([Singleton sharedInstance].secondDone&&[Singleton sharedInstance].fourthDone) {
                    return YES;
                }else{
                    return NO;
                }
            }else{
                if ([Singleton sharedInstance].secondDone){
                    return YES;
                }else{
                    return NO;
                }
            }
            
        }
    }
    return NO;
}
-(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}
@end
