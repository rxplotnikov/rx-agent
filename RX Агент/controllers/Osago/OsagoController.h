//
//  OsagoController.h
//  RX Агент
//
//  Created by RX Group on 14.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "navigation.h"
#import "InsurantView.h"
#import "Singleton.h"
#import "CameraViewController.h"
#import "RXServerConnector.h"
#import <NYTPhotoViewer/NYTPhotoViewer.h>
#import "NYTExamplePhoto.h"
#import "VehicleView.h"
#import "DetailController.h"
#import "OwnerView.h"
#import "BankCardView.h"
#import "ConditionsView.h"

@interface OsagoController : UIViewController<RXConnectorDelegate,NavigationDelegate,insurantDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet navigation *navigationBar;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@end
