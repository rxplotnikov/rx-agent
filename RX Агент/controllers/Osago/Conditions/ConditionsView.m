//
//  ConditionsView.m
//  RX Агент
//
//  Created by RX Group on 02.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "ConditionsView.h"

@implementation ConditionsView{
    float newY;
  
}

-(instancetype)initWithFrame:(CGRect)frame andStartDate:(NSString *)start andPeriod:(NSString *)period andName:(NSString *)name andPhone:(NSString *)phone andComment:(NSString*)comment isFast:(BOOL)isFast withBlock:(void (^)(float height))handler{
    if(self=[super initWithFrame:frame]){
       
        [self initializeWithstartDate:start andPeriod:period andName:name andPhone:phone andComment:comment isFast:isFast withBlock:^(float height) {
            handler(height);
        }];
     
    }
    return self;
}

-(void)initializeWithstartDate:(NSString *)startDate andPeriod:(NSString *)period andName:(NSString *)name andPhone:(NSString *)phone andComment:(NSString*)comment isFast:(BOOL)isFast withBlock:(void(^)(float height))handler{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    _totalHeight=0;
    newY=0;
    [self createHeaderText:@"Условия"];
    
    [self createLabelAtPoint:CGPointMake(10,44) withText:@"Дата начала"];
    newY = newY + 64;
    [self createDateFieldAtPoint:CGPointMake(10, newY) withPlaceholder:@"Укажите дату начала" andText:startDate];
    newY = newY + 35;
    [self createLineAtPoint:CGPointMake(10, newY)];
    newY=newY+11;
    [self createButtonAtPoint:CGPointMake(10 , newY)];
    newY=newY+56;
    [self createLineAtPoint:CGPointMake(10, newY)];
    [self createLabelAtPoint:CGPointMake(10,newY) withText:@"Заявку принял"];
    newY=newY+20;
    [self nameField:CGPointMake(10, newY) withPlaceholder:@"Укажите ФИО" andText:name];
    newY=newY+35;
    [self createLineAtPoint:CGPointMake(10, newY)];
    [self createLabelAtPoint:CGPointMake(10,newY) withText:@"Телефон для обратной связи"];
    newY=newY+20;
    [self createTextFieldAtPoint:CGPointMake(10, newY) withPlaceholder:nil andText:phone];
    newY=newY+35;
    [self createLineAtPoint:CGPointMake(10, newY)];
    newY=newY+20;
    [self createLabelAtPoint:CGPointMake(10,newY) withText:@"Оформить за 4 часа"];
    [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+25/2-31/2) andIndexTag:0 andOn:isFast];
    newY=newY+45;
    [self createLineAtPoint:CGPointMake(10, newY)];
    newY=newY+11;
    [self createTextViewAtPoint:CGPointMake(10, newY) andComment:comment];
    newY=newY+220;
    
    _totalHeight = _totalHeight+newY;
   
    handler(_totalHeight);
}
-(void)drawColor{
    [self setBackgroundColor:[UIColor whiteColor]];
}

-(void)createTextViewAtPoint:(CGPoint)point andComment:(NSString *)comment{
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width-point.x*2, 200)];
    if(![comment isEqualToString:@""]){
        _textView.text=comment;
         [_textView setTextColor:[UIColor blackColor]];
    }else{
        _textView.text=@"Комментарий (пожелания к заявке)";
         [_textView setTextColor:[UIColor lightGrayColor]];
    }
    _textView.layer.borderWidth = 0.5f;
    _textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [_textView setFont:[UIFont fontWithName:@"Helvetica Light" size:18.f]];
   
    [self addSubview:_textView];
}

-(void)createSwitcherAtPoint:(CGPoint)point andIndexTag:(NSInteger)index andOn:(BOOL)isON{
    UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(point.x, point.y, 49, 31)];
    [onoff setOn:isON];
    onoff.tag=index;
    [onoff setOnTintColor:[UIColor colorWithRed:243.f/255.f green:180.f/255.f blue:71.f/255.f alpha:1.f]];
    [onoff addTarget: self action: @selector(flip:) forControlEvents: UIControlEventValueChanged];
    [self addSubview: onoff];
}

-(void)flip:(UISwitch*)switcher{
    [self.delegate switcherAction:switcher isOn:switcher.isOn];
}

-(void)createTextFieldAtPoint:(CGPoint)point withPlaceholder:(NSString*)string andText:(NSString *)text{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, 30, 40)];
    fromLabel.text = @"+7";
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor lightGrayColor];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
    _numberField = [[UITextField alloc] initWithFrame:CGRectMake(25+point.x, point.y,self.frame.size.width-(25+point.x), 40)];
    _numberField.placeholder = string;
    if(text){
      _numberField.text=text;
    }
    _numberField.borderStyle = UITextBorderStyleNone;
    _numberField.font = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    _numberField.autocorrectionType = UITextAutocorrectionTypeNo;
    _numberField.keyboardType = UIKeyboardTypeNumberPad;
    _numberField.returnKeyType = UIReturnKeyDone;
    _numberField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _numberField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [self addSubview:_numberField];
}

-(void)nameField:(CGPoint)point withPlaceholder:(NSString*)string andText:(NSString *)text{
   
    _nameField = [[UITextField alloc] initWithFrame:CGRectMake(point.x, point.y,self.frame.size.width-point.x*2, 40)];
    _nameField.placeholder = string;
    if(text){
        _nameField.text=text;
    }
    _nameField.borderStyle = UITextBorderStyleNone;
    _nameField.font = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    _nameField.autocorrectionType = UITextAutocorrectionTypeNo;
    _nameField.keyboardType = UIKeyboardTypeDefault;
    _nameField.returnKeyType = UIReturnKeyDone;
    _nameField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _nameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [self addSubview:_nameField];
}
-(void)createButtonAtPoint:(CGPoint)point{
    
    _btn = [[UIButton alloc]initWithFrame:CGRectMake(point.x, point.y,self.frame.size.width-point.x*2, 40)];
    if(![Singleton sharedInstance].model.term){
        
        [_btn setTitle:@"Выберите период страхования..." forState:UIControlStateNormal];
        _btn.tag=100;
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentLeft];
        
        UIFont *font1 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
        
        NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                                NSFontAttributeName:font1,
                                NSParagraphStyleAttributeName:style,
                                NSForegroundColorAttributeName : [UIColor colorWithRed:97.f/255.f green:97.f/255.f blue:97.f/255.f alpha:0.4]
                                };
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Выберите период страхования..."    attributes:dict1]];
        [_btn setAttributedTitle:attString forState:UIControlStateNormal];
        [_btn addTarget:self action:@selector(openList) forControlEvents:UIControlEventTouchUpInside];
        _btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
    }else{
        
        [_btn setTitle:[Singleton sharedInstance].model.term forState:UIControlStateNormal];
        _btn.tag=100;
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setAlignment:NSTextAlignmentLeft];
        
        UIFont *font1 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
        
        NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                                NSFontAttributeName:font1,
                                NSParagraphStyleAttributeName:style,
                                NSForegroundColorAttributeName : [UIColor blueColor]
                                }; // Added line
        
        
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
        [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[Singleton sharedInstance].model.term    attributes:dict1]];
        _btn.titleLabel.numberOfLines = 2;
        [_btn setAttributedTitle:attString forState:UIControlStateNormal];
        [_btn addTarget:self action:@selector(openList) forControlEvents:UIControlEventTouchUpInside];
        _btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    [self addSubview:_btn];
}

-(void)createLabelAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width-point.x, 25)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:72.f/255.f green:72.f/255.f blue:72.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}


-(void)openList{
    [self.delegate needOpenTable];
}

-(void)createHeaderText:(NSString*)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica" size:24.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, self.frame.size.width-10,45)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:233.f/255.f green:148.f/255.f blue:0.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}

-(void)createDateFieldAtPoint:(CGPoint)point withPlaceholder:(NSString*)string andText:(NSString *)text{
    _datefield = [[UITextField alloc] initWithFrame:CGRectMake(point.x, point.y,self.frame.size.width-point.x, 40)];
    _datefield.placeholder = string;
    if(text){
        _datefield.text=text;
    }
    _datefield.borderStyle = UITextBorderStyleNone;
    _datefield.font = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    _datefield.autocorrectionType = UITextAutocorrectionTypeNo;
    _datefield.keyboardType = UIKeyboardTypeNumberPad;
    _datefield.returnKeyType = UIReturnKeyDone;
    _datefield.clearButtonMode = UITextFieldViewModeWhileEditing;
    _datefield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [self addSubview:_datefield];
}

-(void) createLineAtPoint:(CGPoint )point{
    UIView *view= [[UIView alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width, 0.3f)];
    [view setBackgroundColor:[UIColor colorWithRed:151.f/255.f green:151.f/255.f blue:151.f/255.f alpha:1.f]];
    [self addSubview:view];
}
@end
