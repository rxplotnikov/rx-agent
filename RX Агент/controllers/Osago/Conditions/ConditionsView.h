//
//  ConditionsView.h
//  RX Агент
//
//  Created by RX Group on 02.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@protocol  ConditionsDelegate <NSObject>
-(void)needOpenTable;
-(void)switcherAction:(UISwitch *)switcher isOn:(BOOL)isON;

@end

@interface ConditionsView : UIView
@property(weak, nonatomic) id<ConditionsDelegate>delegate;
-(instancetype)initWithFrame:(CGRect)frame andStartDate:(NSString *)start andPeriod:(NSString *)period andName:(NSString *)name andPhone:(NSString *)phone andComment:(NSString*)comment isFast:(BOOL)isFast withBlock:(void (^)(float height))handler;
-(void)initializeWithstartDate:(NSString *)startDate andPeriod:(NSString *)period andName:(NSString *)name andPhone:(NSString *)phone andComment:(NSString*)comment isFast:(BOOL)isFast withBlock:(void(^)(float height))handler;
-(void)drawColor;

@property UITextField *numberField;
@property UITextField *nameField;

@property UITextField *datefield;
@property UITextView *textView;
@property float totalHeight;
@property UIButton *btn;
@end
