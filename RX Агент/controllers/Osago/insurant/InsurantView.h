//
//  InsurantView.h
//  RX Агент
//
//  Created by RX Group on 30.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonWithPhoto.h"
@protocol  insurantDelegate <NSObject>
-(void)switcherIsOn:(NSInteger)index isOn:(BOOL)isOn;
-(void)buttonClicked:(NSInteger)index;
-(void)attachmentClicked:(NSInteger)index;
@end
@interface InsurantView : UIView

@property UITextField *textField;
@property UILabel *placeholderLbl;
@property(weak, nonatomic) id<insurantDelegate>delegate;
@property float totalHeight;
-(instancetype)initWithFrame:(CGRect)frame andSettings:(int)level andTimeReg:(BOOL)timeReg andDKP:(BOOL)dkp andNoDKP:(BOOL)noDKP withBlock:(void (^)(float height))handler;
-(void)setImageForButton:(UIImage*)img andTag:(NSInteger)index;
-(void)initializeAndSetting:(int)index andTimeReg:(BOOL)timeReg andDKP:(BOOL)dkp andNoDKP:(BOOL)noDKP withBlock:(void (^)(float height))handler;
-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index;
-(void)drawColor;
@end
