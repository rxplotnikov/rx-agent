//
//  osagoModel.m
//  RX Агент
//
//  Created by RX Group on 14.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "osagoModel.h"

@implementation osagoModel
-(instancetype)init{
    if(self==[super init]){
        _tempPhoto = [NSMutableArray arrayWithArray:@[@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@""]];
        _dictionary = [NSDictionary dictionary];
        _dictionary = @{@"drivers":@[
                                @{
                                @"driverLicensePageFirstID":@"",
                                @"driverLicensePageSecondID":@"",
                                @"scanTranslationPageID":@""
                                },@{
                                @"driverLicensePageFirstID":@"",
                                @"driverLicensePageSecondID":@"",
                                @"scanTranslationPageID":@""
                                },@{
                                 @"driverLicensePageFirstID":@"",
                                 @"driverLicensePageSecondID":@"",
                                 @"scanTranslationPageID":@""
                                 },@{
                                 @"driverLicensePageFirstID":@"",
                                 @"driverLicensePageSecondID":@"",
                                 @"scanTranslationPageID":@""
                                }]};
        _ownerDocumentPageFirstID=@"";
        _ownerDocumentPageSecondID=@"";
        
        //Страхователь
        _documentPageFirstID=@"";
        _documentPageSecondID=@"";
        
        _saleContractPageID=@"";
        _tempRegPageID=@"";
        
        _vehicleTarget=@"";
        
        //ПТС
        _vehiclePassportPageFirstID=@"";
        _vehiclePassportPageSecondID=@"";
        //СРТС
        _vehicleCertificatePageFirstID=@"";
        _vehicleCertificatePageSecondID=@"";
        //ТО
        _cardPageFirstID=@"";
        _cardPageSecondID=@"";
        
        
        //условия
        _creatorFullName=@"";
        _creatorPhone=@"";
        _agentComment=@"";
    }
    return self;
}
@end
