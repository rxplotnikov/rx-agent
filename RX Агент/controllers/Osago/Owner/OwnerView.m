//
//  OwnerView.m
//  RX Агент
//
//  Created by RX Group on 06.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "OwnerView.h"
#import "Singleton.h"


@implementation OwnerView{
    float newY;
}


-(instancetype)initWithFrame:(CGRect)frame andTimeReg:(BOOL)timeReg andDKP:(BOOL)dkp andNoDKP:(BOOL)noDKP withBlock:(void (^)(float height))handler{
    if(self=[super initWithFrame:frame]){
        [self initializeAndSettingTimeReg:timeReg andDKP:dkp andNoDKP:noDKP withBlock:^(float height) {
            handler(height);
        }];
    }
    return self;
}

-(void)initializeAndSettingTimeReg:(BOOL)timeReg andDKP:(BOOL)dkp andNoDKP:(BOOL)noDKP withBlock:(void (^)(float height))handler{

        for (UIView *view in self.subviews) {
            if(![view isKindOfClass:[ButtonWithPhoto class]]){
                [view removeFromSuperview];
            }
        }
     
    [self createHeaderText:@"Владелец"];
    //телефон
    newY=[self getLabelHeight:@"Владелец"];
    
    //регистрация
    newY=newY+11;
    [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+[self getLabelHeight:@"Временная регистрация"]/2-31/2)andIndexTag:1 andOn:timeReg];
    [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Временная регистрация"];
    newY=newY+[self getLabelHeight:@"Временная регистрация"]+10;
    [self createViewAtPoint:CGPointMake(0, newY)];
    
    //ДКП
    newY=newY+11;
    [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+[self getLabelHeight:@"Сменился владелец по ДКП"]/2-31/2)andIndexTag:2 andOn:dkp];
    [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Сменился владелец по ДКП"];
    newY=newY+[self getLabelHeight:@"Сменился владелец по ДКП"]+10;
    [self createViewAtPoint:CGPointMake(0, newY)];
    //нет ДКП
    newY=newY+11;
    if(dkp){
        [self createSwitcherAtPoint:CGPointMake(self.frame.size.width-55, newY+[self getLabelHeight:@"Нет скана ДКП"]/2-31/2)andIndexTag:3 andOn:noDKP];
        [self createLabelAtPoint:CGPointMake(10, newY) withText:@"Нет скана ДКП"];
        newY=newY+[self getLabelHeight:@"Нет скана ДКП"]+10;
        [self createViewAtPoint:CGPointMake(0, newY)];
    }
    //Паспорт левая сторона
    newY=newY+ 11;
    if(! [self getButtonAtIndex:4]){
        [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:4];
    }else{
      
        [[self getButtonAtIndex:4] setFrame:CGRectMake(10, newY, 60, 60)];
    }
    [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"Паспорт\nЛицевая сторона"];
    if(![[Singleton sharedInstance].osago.ownerDocumentPageFirstID isEqualToString:@""]){
        [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:4];
    }
    newY=newY+68;
    
    newY=newY+7;
  
    if(! [self getButtonAtIndex:5]){
        [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:5];
    }else{
        
        [ [self getButtonAtIndex:5] setFrame:CGRectMake(10, newY, 60, 60)];
    }
    [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"Паспорт\nРегистрация"];
    if(![[Singleton sharedInstance].osago.ownerDocumentPageSecondID isEqualToString:@""]){
        [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:5];
    }
    newY=newY+68;
    
    if(!noDKP&&dkp){
        newY=newY+7;
        
        if(! [self getButtonAtIndex:6]){
            [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:6];
        }else{
            [[self getButtonAtIndex:6] setHidden:NO];
            [[self getButtonAtIndex:6] setFrame:CGRectMake(10, newY, 60, 60)];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"ДКП"];
        if(![[Singleton sharedInstance].osago.saleContractPageID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:6];
        }
        newY=newY+68;
    }else{
      
        [[self getButtonAtIndex:6] setHidden:YES];
    }
 
    
    if(timeReg){
        newY=newY+7;
       
        if(! [self getButtonAtIndex:7]){
            [self createPhotoButtonAtPoint:CGPointMake(10, newY) andTag:7];
        }else{
            [ [self getButtonAtIndex:7] setHidden:NO];
            [ [self getButtonAtIndex:7] setFrame:CGRectMake(10, newY, 60, 60)];
        }
        [self createLabelAtPoint:CGPointMake(76, newY+30-22) withText:@"Временная регистрация"];
        if(![[Singleton sharedInstance].osago.tempRegPageID isEqualToString:@""]){
            [self createAttachment:CGPointMake(self.frame.size.width-55, newY+30-16) andTag:7];
        }
        newY=newY+70;
    }else{
      
        [ [self getButtonAtIndex:7] setHidden:YES];
    }
    
    _totalHeight = newY;
    handler(_totalHeight);
}

-(void)drawColor{
    [self setBackgroundColor:[UIColor whiteColor]];
}

-(void)createHeaderText:(NSString*)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica" size:24.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, self.frame.size.width-10, [self getLabelHeight:string]+5)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:233.f/255.f green:148.f/255.f blue:0.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}

-(void)createPhotoButtonAtPoint:(CGPoint)point andTag:(NSInteger)index{
    ButtonWithPhoto *photo = [[ButtonWithPhoto alloc]initWithFrame:CGRectMake(point.x, point.y, 60, 60) andTag:index];
    photo.tag=index;
    photo.photoBtn.tag=index;
    [photo.photoBtn addTarget:self action:@selector(buttonClickedAtIndex:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:photo];
}

-(void)createAttachment:(CGPoint)point andTag:(NSInteger)index{
    UIButton *attach = [[UIButton alloc]initWithFrame:CGRectMake(point.x, point.y, 35, 35)];
    attach.tag=index;
    [attach setImage:[UIImage imageNamed:@"attach"] forState:UIControlStateNormal];
    [attach addTarget:self action:@selector(attachClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:attach];
}

-(void)createViewAtPoint:(CGPoint)point{
    UIView *view= [[UIView alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width, 0.5f)];
    [view setBackgroundColor:[UIColor colorWithRed:151.f/255.f green:151.f/255.f blue:151.f/255.f alpha:1.f]];
    [self addSubview:view];
}

-(void)createLabelAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:18.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, self.frame.size.width-point.x-49, [self getLabelHeight:string])];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:72.f/255.f green:72.f/255.f blue:72.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}



-(void)createSwitcherAtPoint:(CGPoint)point andIndexTag:(NSInteger)index andOn:(BOOL)isON{
    UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(point.x, point.y, 49, 31)];
    [onoff setOn:isON];
    onoff.tag=index;
    [onoff setOnTintColor:[UIColor colorWithRed:243.f/255.f green:180.f/255.f blue:71.f/255.f alpha:1.f]];
    [onoff addTarget: self action: @selector(flip:) forControlEvents: UIControlEventValueChanged];
    [self addSubview: onoff];
}

-(void)flip:(UISwitch*)switcher{
    [self.delegate ownerSwitcherIsOn:switcher.tag andIsON:switcher.isOn];
}

-(void)buttonClickedAtIndex:(UIButton *)btn{
    [self.delegate ownerButtonClicked:btn.tag];
}

-(void)attachClick:(UIButton *)btn{
    [self.delegate ownerAttachmentClick:btn.tag];
}

- (CGFloat)getLabelHeight:(NSString*)text
{
    return 44. ;
}

-(void)setImageForButton:(UIImage*)img andTag:(NSInteger)index{
    for (UIButton *btn in self.subviews) {
        if([btn isKindOfClass:[UIButton class]]){
            if(btn.tag == index){
                [btn setImage:img forState:UIControlStateNormal];
            }
        }
    }
}

-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index{
    for (ButtonWithPhoto *photo in self.subviews) {
        if([photo isKindOfClass:[ButtonWithPhoto class]]){
            if(photo.tag == index){
                return photo;
            }
        }
    }
    return nil;
}

@end
