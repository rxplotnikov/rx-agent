//
//  OwnerView.h
//  RX Агент
//
//  Created by RX Group on 06.06.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonWithPhoto.h"
@protocol  ownerDelegate <NSObject>
-(void)ownerSwitcherIsOn:(NSInteger)index andIsON:(BOOL)isOn;
-(void)ownerButtonClicked:(NSInteger)index;
-(void)ownerAttachmentClick:(NSInteger)index;
@end

@interface OwnerView : UIView
@property(weak, nonatomic) id<ownerDelegate>delegate;

@property UITextField *textField;
@property UILabel *placeholderLbl;
@property float totalHeight;
-(instancetype)initWithFrame:(CGRect)frame andTimeReg:(BOOL)timeReg andDKP:(BOOL)dkp andNoDKP:(BOOL)noDKP withBlock:(void (^)(float height))handler;
-(void)initializeAndSettingTimeReg:(BOOL)timeReg andDKP:(BOOL)dkp andNoDKP:(BOOL)noDKP withBlock:(void (^)(float height))handler;
-(void)setImageForButton:(UIImage*)img andTag:(NSInteger)index;
-(void)drawColor;
-(ButtonWithPhoto*)getButtonAtIndex:(NSInteger)index;
@end
