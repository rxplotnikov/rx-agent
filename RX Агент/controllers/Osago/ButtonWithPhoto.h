//
//  ButtonWithPhoto.h
//  RX Агент
//
//  Created by RX Group on 15.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"
#import "RXServerConnector.h"
#import "Singleton.h"
@protocol  ButtonPhotoDelegate <NSObject>
-(void)loadIsOver;

@end

@interface ButtonWithPhoto : UIView
@property UIButton *photoBtn;
@property MBCircularProgressBarView *progressBar;
@property(weak, nonatomic) id<ButtonPhotoDelegate>delegate;

-(instancetype)initWithFrame:(CGRect)frame andTag:(NSInteger)index;
-(void)setImageForButton:(UIImage*)image;
-(void)setProgress:(float)value;
-(void)loadImageToServer:(UIImage *)image;

@end
