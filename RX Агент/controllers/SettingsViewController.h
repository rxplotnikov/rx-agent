//
//  SettingsViewController.h
//  RX Агент
//
//  Created by RX Group on 28.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end
