//
//  RXLoadingView.m
//  RX Агент
//
//  Created by RX Group on 25.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "RXLoadingView.h"

@implementation RXLoadingView{
    UIWindow *window;
    UIVisualEffectView *viewWithBlurredBackground;
}

-(instancetype)initWithText:(NSString *)text{
    if(self = [super init]){
        //Виндоу
        window = [[UIApplication sharedApplication] windows].lastObject;
        UIBlurEffect * effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        viewWithBlurredBackground =
        [[UIVisualEffectView alloc] initWithEffect:effect];
        [viewWithBlurredBackground setFrame:window.frame];
        //Шильдик
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.color=[UIColor darkGrayColor];
        [activityIndicator setCenter:viewWithBlurredBackground.center];
        [activityIndicator startAnimating];
        [viewWithBlurredBackground.contentView addSubview:activityIndicator];
        //Загрзка
        UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:18.f];
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, activityIndicator.frame.origin.y+activityIndicator.frame.size.height+8,window.frame.size.width-16, 40)];
        fromLabel.text = text;
        fromLabel.font = customFont;
        fromLabel.numberOfLines = 1;
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.clipsToBounds = YES;
        fromLabel.backgroundColor = [UIColor clearColor];
        fromLabel.textColor = [UIColor lightGrayColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        [viewWithBlurredBackground.contentView addSubview:fromLabel];
    }
    return self;
}

-(void)showView{
   [window addSubview:viewWithBlurredBackground];
}

-(void)dissmissView{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (UIVisualEffectView *viewWithBlur in self->window.subviews) {
            [viewWithBlur removeFromSuperview];
        }
    });
  
}
@end
