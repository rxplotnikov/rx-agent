//
//  ViewController.h
//  RX Агент
//
//  Created by RX Group on 21.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTabBar.h"

@interface ViewController : UIViewController<TabBarDelegate>

@property (weak, nonatomic) IBOutlet CustomTabBar *tabBar;
@property (weak, nonatomic) IBOutlet UIView *feedContainer;
@property (weak, nonatomic) IBOutlet UIView *workContainer;
@property (weak, nonatomic) IBOutlet UIView *supportContainer;
@property (weak, nonatomic) IBOutlet UIView *settingsContainer;


@end

