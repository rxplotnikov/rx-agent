//
//  CardController.h
//  RX Агент
//
//  Created by RX Group on 10.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "JGStackedCollectionView.h"
#import "ColorPicker.h"

typedef enum:NSInteger{
    settingType=0,
    payType=1
} enterType;

@interface CardController : UIViewController


@property (weak, nonatomic) IBOutlet JGStackedCollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *add_button;
@property (weak, nonatomic) IBOutlet UIView *settingView;
@property (weak, nonatomic) IBOutlet ColorPicker *picker;
@property (weak, nonatomic) IBOutlet UISwitch *color_switcher;
@property (weak, nonatomic) IBOutlet UISwitch *number_switcher;
@property enterType enterType;
@end
