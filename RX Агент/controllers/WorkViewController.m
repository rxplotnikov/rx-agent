//
//  WorkViewController.m
//  RX Агент
//
//  Created by RX Group on 28.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "WorkViewController.h"
#import "FeedCell.h"
#import "RXServerConnector.h"
#import "RXLoadingView.h"

@interface WorkViewController (){
    NSMutableArray *policess;
    NSIndexPath *expandingIndexPath;
    NSIndexPath *expandedIndexPath;
    RXLoadingView *loadView;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isLoad;
}

@end

@implementation WorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage=1;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self receiveNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification)
                                                 name:@"load1"
                                               object:nil];
}

-(void)receiveNotification{
   
    [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies?status=9"] withBlock:^(NSMutableDictionary *dictionary) {
        self->policess = [NSMutableArray new];
        self->totalPages = [dictionary[@"meta"][@"total_pages"]integerValue];
        for (int i = 0; i<[dictionary[@"eosagoPolicies"]count]; i++) {
            if([dictionary[@"eosagoPolicies"][i][@"state"]integerValue]!=254&&[dictionary[@"eosagoPolicies"][i][@"state"]integerValue]!=4&&[dictionary[@"eosagoPolicies"][i][@"state"]integerValue]!=0){
                [self->policess addObject:dictionary[@"eosagoPolicies"][i]];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->_table reloadData];
        });
    }];
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        
       
       
        if([policess[indexPath.row][@"state"]integerValue]==3||[policess[indexPath.row][@"state"]integerValue]==2||[policess[indexPath.row][@"state"]integerValue]==5||[policess[indexPath.row][@"state"]integerValue]==6||[policess[indexPath.row][@"state"]integerValue]==7||[policess[indexPath.row][@"state"]integerValue]==8||[policess[indexPath.row][@"state"]integerValue]==9){
            if([indexPath isEqual:expandedIndexPath]){
                return 120;
            }
        }
    }else{
        if([policess[indexPath.row-1][@"state"]integerValue]==3||[policess[indexPath.row-1][@"state"]integerValue]==2||[policess[indexPath.row-1][@"state"]integerValue]==5||[policess[indexPath.row-1][@"state"]integerValue]==6||[policess[indexPath.row-1][@"state"]integerValue]==7||[policess[indexPath.row-1][@"state"]integerValue]==8||[policess[indexPath.row-1][@"state"]integerValue]==9){
            if([indexPath isEqual:expandedIndexPath]){
                return 120;
            }
        }
       
    }
    
    return 90;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (expandedIndexPath) {
        return [policess count] + 1;
    }
    
    return [policess count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = nil;
    FeedCell *cell = nil;
    NSIndexPath *theIndexPath = [self actualIndexPathForTappedIndexPath:indexPath];
    
    if ([indexPath isEqual:expandedIndexPath]) {
        CellIdentifier = @"Expanded2";

        if([policess[theIndexPath.row-1][@"state"]integerValue]==1){
            CellIdentifier = @"ExpandedCellIdentifier";
        }
        
    }else{
        CellIdentifier = @"cell";
    }
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[FeedCell alloc] initWithStyle:UITableViewCellStyleDefault
                               reuseIdentifier:CellIdentifier];
    }
    
    if([[cell reuseIdentifier]isEqualToString:@"Expanded2"]){
        cell.FIOLbl.text = [NSString stringWithFormat:@"ФИО: %@",policess[theIndexPath.row-1][@"insurantFullName"]];
        cell.TSlbl.text =[NSString stringWithFormat:@"ТС: %@",policess[theIndexPath.row-1][@"vehicleModel"]];
        cell.summLbl.text =[NSString stringWithFormat:@"Сумма: %ld",[policess[theIndexPath.row-1][@"baseCost"]integerValue]+[policess[theIndexPath.row-1][@"surcharge"]integerValue]];
    }
    
  
    
    if([[cell reuseIdentifier]isEqualToString:@"ExpandedCellIdentifier"]){
        cell.dateLbl.text = [NSString stringWithFormat:@"Дата начала: %@",[self dateForCell:policess[theIndexPath.row-1][@"startDate"]]];
        if([policess[theIndexPath.row-1][@"insurancePeriod"]integerValue]==12){
            cell.period.text =@"Период страхования: 1 год";
        }
        if([policess[theIndexPath.row-1][@"insurancePeriod"]integerValue]==3){
            cell.period.text =@"Период страхования: 3 месяца";
        }
        if([policess[theIndexPath.row-1][@"insurancePeriod"]integerValue]==6){
            cell.period.text =@"Период страхования: 6 месяцев";
        }
    }
    if ([[cell reuseIdentifier] isEqualToString:@"cell"]){
        cell.numberPolice.text = [NSString stringWithFormat:@"№ %@",[policess[theIndexPath.row][@"id"]stringValue]];
        cell.costLbl.text = [NSString stringWithFormat:@"%@ ₽",[policess[theIndexPath.row][@"baseCost"]stringValue]];
        cell.createDate.text = [self dateFromJson:policess[theIndexPath.row][@"createDate"]];
        [ cell setIconFromType:[policess[theIndexPath.row][@"state"] intValue] isShort:[policess[theIndexPath.row][@"isShort"] boolValue] isPaid:[policess[theIndexPath.row][@"paid"] boolValue]];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // disable touch on expanded cell
    
    FeedCell *cell = [self.table cellForRowAtIndexPath:indexPath];
    if ([[cell reuseIdentifier] isEqualToString:@"ExpandedCellIdentifier"]) {
        return;
    }
    if ([[cell reuseIdentifier] isEqualToString:@"Expanded2"]) {
        return;
    }
    
    if([policess[indexPath.row][@"state"]integerValue]==4){
        return ;
    }
    // deselect row
    [tableView deselectRowAtIndexPath:indexPath
                             animated:NO];
    
    // get the actual index path
    indexPath = [self actualIndexPathForTappedIndexPath:indexPath];
    
    // save the expanded cell to delete it later
    NSIndexPath *theExpandedIndexPath = expandedIndexPath;
    
    // same row tapped twice - get rid of the expanded cell
    if ([indexPath isEqual:expandingIndexPath]) {
        expandingIndexPath = nil;
        expandedIndexPath = nil;
    }
    // add the expanded cell
    else {
        expandingIndexPath = indexPath;
        expandedIndexPath = [NSIndexPath indexPathForRow:[indexPath row] + 1
                                               inSection:[indexPath section]];
    }
    
    [tableView beginUpdates];
    
    if (theExpandedIndexPath) {
        [self.table deleteRowsAtIndexPaths:@[theExpandedIndexPath]
                          withRowAnimation:UITableViewRowAnimationNone];
    }
    if (expandedIndexPath) {
        [self.table insertRowsAtIndexPaths:@[expandedIndexPath]
                          withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [tableView endUpdates];
    
    // scroll to the expanded cell
    [self.table scrollToRowAtIndexPath:indexPath
                      atScrollPosition:UITableViewScrollPositionMiddle
                              animated:YES];
}
- (NSIndexPath *)actualIndexPathForTappedIndexPath:(NSIndexPath *)indexPath
{
    if (expandedIndexPath && [indexPath row] > [expandedIndexPath row]) {
        return [NSIndexPath indexPathForRow:[indexPath row] - 1
                                  inSection:[indexPath section]];
    }
    
    return indexPath;
}
- (IBAction)sendCell:(UIButton *)sender {
    [[RXServerConnector sharedConnector]patchRequestFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies/%@/dispatch",policess[sender.tag][@"id"]] withBlock:^(NSMutableDictionary *dictionary) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self receiveNotification];
            self->expandingIndexPath = nil;
            self->expandedIndexPath = nil;
        });
        
    } withErrorBlock:^(NSString *error) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Внимание!"
                                     message:error
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(NSString *)dateForCell:(NSString *)dateText{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    NSDate *date = [dateFormat dateFromString:dateText];
    NSString *dateStri=[dateFormatter stringFromDate:date];
    
    return dateStri;
}
-(NSString *)dateFromJson:(NSString *)dateText{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    NSDate *date = [dateFormat dateFromString:dateText];
    NSString *dateStri=[dateFormatter stringFromDate:date];
    
    return dateStri;
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        if(!isLoad){
            [self loadNewPage];
        }
    }
    
}
-(void)loadNewPage{
    if(currentPage<=totalPages){
        currentPage++;
        isLoad=TRUE;
        [[RXServerConnector sharedConnector]getDataFromURL:[NSString stringWithFormat:@"https://demo-api.rx-agent.ru/v0/EOSAGOPolicies?Page=%ld",currentPage] withBlock:^(NSMutableDictionary *dictionary) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self receiveNotification];
                self->isLoad=FALSE;
                [self->_table reloadData];
            });
            
        }];
    }
    
}
@end
