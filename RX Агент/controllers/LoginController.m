//
//  LoginController.m
//  RX Агент
//
//  Created by RX Group on 18.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "LoginController.h"
#import "AppAuth.h"
#import "AppDelegate.h"
#import "ViewController.h"

static NSString *const kIssuer = @"https://demo-accounts.rx-agent.ru";
static NSString *const kClientID = @"b2b.app.ios";
static NSString *const kRedirectURI = @"b2b.app.ios://ru.rx-agent.demo-accounts/oauthredirect";

@interface LoginController (){
    OIDServiceConfiguration *config;
    NSString *token;
}

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
 
    for (int i = 0; i<22; i++) {
        CGFloat size = [self randomFloatBetween:40 and:150];
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0 , size, size)];
        [image setAlpha:[self randomFloatBetween:0.5f and:1.f]];
        [image setImage:[UIImage imageNamed:@"Rectangle 3"]];
        [self.view addSubview:image];
        NSArray * pathArray = @[ [NSValue valueWithCGPoint:CGPointMake(arc4random_uniform(self.view.frame.size.width), arc4random_uniform(self.view.frame.size.height))], [NSValue valueWithCGPoint:CGPointMake(arc4random_uniform(self.view.frame.size.width), arc4random_uniform(self.view.frame.size.height))],[NSValue valueWithCGPoint:CGPointMake(arc4random_uniform(self.view.frame.size.width), arc4random_uniform(self.view.frame.size.height))], [NSValue valueWithCGPoint:CGPointMake(arc4random_uniform(self.view.frame.size.width), arc4random_uniform(self.view.frame.size.height))],[NSValue valueWithCGPoint:CGPointMake(arc4random_uniform(self.view.frame.size.width), arc4random_uniform(self.view.frame.size.height))], [NSValue valueWithCGPoint:CGPointMake(arc4random_uniform(self.view.frame.size.width), arc4random_uniform(self.view.frame.size.height))] ];
        CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        pathAnimation.values = pathArray;
        pathAnimation.duration = [self randomFloatBetween:8 and:10];
        pathAnimation.repeatCount = INFINITY;
        pathAnimation.autoreverses =YES;
        [image.layer addAnimation:pathAnimation forKey:@"position"];
    }
     [self.view bringSubviewToFront:_logo];

}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

- (IBAction)auth:(id)sender {
    NSURL *issuer = [NSURL URLWithString:kIssuer];
    [OIDAuthorizationService discoverServiceConfigurationForIssuer:issuer
                                                        completion:^(OIDServiceConfiguration *_Nullable configuration,
                                                                     NSError *_Nullable error) {
                                                            if (!configuration) {
                                                                NSLog(@"Error retrieving discovery document: %@",
                                                                      [error localizedDescription]);
                                                                return;
                                                            }
                                                            self->config= configuration;
                                                            [self authtorization];
                                                        }];
    
    
}

-(void)authtorization{
    OIDAuthorizationRequest *request =
    [[OIDAuthorizationRequest alloc] initWithConfiguration:config
                                                  clientId:kClientID
                                                    scopes:@[OIDScopeOpenID,OIDScopeProfile,@"b2b.api"]
                                               redirectURL:[NSURL URLWithString:kRedirectURI]
                                              responseType:OIDResponseTypeCode
                                      additionalParameters:nil];
    AppDelegate *appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.currentAuthorizationFlow =[OIDAuthState authStateByPresentingAuthorizationRequest:request
                                                                         presentingViewController:self
                                                                                         callback:^(OIDAuthState *_Nullable authState,
                                                                                                    NSError *_Nullable error) {
                                                                                             if (authState) {
                                                                                                 
                                                                                                 [[NSUserDefaults standardUserDefaults]setObject:authState.lastTokenResponse.accessToken forKey:@"token"];
                                                                                                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                                                 ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"mainView"];
                                                                                                 [[UIApplication sharedApplication].keyWindow setRootViewController:vc];
                                                                                             } else {
                                                                                                 [self getAlert: [error localizedDescription]];
                                                                                             }
                                                                                             
                                                                                         }];
}

-(void)getAlert:(NSString*)string{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Внимание!" message:string preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end 
