////
////  OsagoViewController.m
////  RX Агент
////
////  Created by RX Group on 28.05.2018.
////  Copyright © 2018 RX Group. All rights reserved.
////
//
//#import "OsagoViewController.h"
//#import "InsurantView.h"
//#import "OwnerView.h"
//#import "UIColor+NavigationColors.h"
//#import "Singleton.h"
//#import "CameraViewController.h"
//#import "VehicleView.h"
//#import "RoundViewChose.h"
//#import "PhotoDriver.h"
//#import "ConditionsView.h"
//#import "CalcViewController.h"
//
//@interface OsagoViewController ()<NavigationDelegate,insurantDelegate,ownerDelegate,vehicleDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,RoundViewDelegate,ConditionsDelegate,UITextViewDelegate>{
//    float totalHeight;
//    NSMutableArray *cardArray;
//    InsurantView *insView;
//    OwnerView *ownView;
//    VehicleView *vehicleView;
//    NSMutableArray *arrayReasons;
//    RoundViewChose *roundView;
//    PhotoDriver *driverCard;
//    ConditionsView *conditions;
//    UIDatePicker *datePicker;
//    BOOL isVehicle;
//    float keyboardHeight;
//}
//
//@end
//
//@implementation OsagoViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//     [_headerLbl setFont:[UIFont fontWithName:@"Helvetica Light" size:24.f]];
//    _navigationBar.delegate=self;
//    cardArray = [NSMutableArray new];
//    totalHeight=10;
//  [[NSNotificationCenter defaultCenter] addObserver:self
//                                           selector:@selector(keyboardWillChangeFrame:)
//                                               name:UIKeyboardWillShowNotification
//                                             object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillBeHidden:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//   
//}
//- (void)keyboardWillChangeFrame:(NSNotification *)notification
//{
//    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
//}
//
//-(void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//    if(!insView){
//        [self creatingCards];
//        UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                action:@selector(hideKeyboard)];
//        tapRecognizer.cancelsTouchesInView = NO;
//        [[self.view window]addGestureRecognizer:tapRecognizer];
//        
//    }else{
//        if([[Singleton sharedInstance]isStrOn]){
//            if([[Singleton sharedInstance] insViewIsDone]){
//                [_navigationBar setColorForItem:0 andColor:[UIColor customGreenColor]];
//            }else{
//                [_navigationBar setColorForItem:0 andColor:[UIColor customYellowColor]];
//            }
//        }else{
//            if([[Singleton sharedInstance] insViewIsDone]){
//                [_navigationBar setColorForItem:0 andColor:[UIColor customGreenColor]];
//            }else{
//                [_navigationBar setColorForItem:0 andColor:[UIColor customYellowColor]];
//            }
//            if([[Singleton sharedInstance] ownViewIsDone]){
//                [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//            }else{
//                [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//            }
//        }
//        if(![[Singleton sharedInstance].arrayOfPhoto[[Singleton sharedInstance].indexForPhoto]isKindOfClass:[NSNull class]]){
//            if([Singleton sharedInstance].isOwner){
//                [ownView setImageForButton:[Singleton sharedInstance].arrayOfPhoto[[Singleton sharedInstance].indexForPhoto] andTag:[Singleton sharedInstance].indexForPhoto];
//            }else{
//                [insView setImageForButton:[Singleton sharedInstance].arrayOfPhoto[[Singleton sharedInstance].indexForPhoto] andTag:[Singleton sharedInstance].indexForPhoto];
//            }
//            
//        }
//            
//    }
//}
//
//-(void)hideKeyboard{
//    [self.view endEditing:YES];
//}
//
//-(void)ownerSwitcherIsOn:(NSInteger)index{
//  
//    if(index==0){
//        [Singleton sharedInstance].isStrOn=![Singleton sharedInstance].isStrOn;
//        [_navigationBar setBigScroll:![Singleton sharedInstance].isStrOn];
//    }
//    if(index==1){
//        [Singleton sharedInstance].ownerTimeReg=![Singleton sharedInstance].ownerTimeReg;
//    }
//    if(index==2){
//        [Singleton sharedInstance].ownerDkp=![Singleton sharedInstance].ownerDkp;
//    }
//    if(![Singleton sharedInstance].isStrOn){
//        if([[Singleton sharedInstance] ownViewIsDone]){
//            [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//        }else{
//            [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//        }
//    }
//  
//  
//    [self creatingCards];
//}
//-(void)switcherIsOn:(NSInteger)index{
//    
//    
//        if(index==0){
//            [Singleton sharedInstance].isStrOn=![Singleton sharedInstance].isStrOn;
//            if([Singleton sharedInstance].isStrOn){
//                [_navigationBar setColorForItem:1 andColor:[UIColor customGrayColor]];
//            }else{
//                if([[Singleton sharedInstance] ownViewIsDone]){
//                    [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//                }else{
//                    [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//                }
//            }
//            [_navigationBar setBigScroll:![Singleton sharedInstance].isStrOn];
//        }
//        if(index==1){
//            [Singleton sharedInstance].timeReg=![Singleton sharedInstance].timeReg;
//        }
//        if(index==2){
//            [Singleton sharedInstance].dkp=![Singleton sharedInstance].dkp;
//        }
//    
//     if([Singleton sharedInstance].isStrOn){
//        if([[Singleton sharedInstance] insViewIsDone]){
//            [_navigationBar setColorForItem:0 andColor:[UIColor customGreenColor]];
//        }else{
//            [_navigationBar setColorForItem:0 andColor:[UIColor customYellowColor]];
//        }
//     }
//    [self creatingCards];
//   
// 
//}
//-(void)creatingCards{
//     cardArray = [NSMutableArray new];
//    for (UIView *view in self.scroll.subviews) {
//        [view removeFromSuperview];
//    }
//    if(![Singleton sharedInstance].isStrOn){
//        totalHeight=10;
//        
//        insView = [[InsurantView alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96)andSettings:1 andTimeReg:[Singleton sharedInstance].timeReg andDKP:[Singleton sharedInstance].dkp];
//        insView.delegate=self;
//        insView.textField.delegate=self;
//        [insView setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, insView.totalHeight)];
//        [cardArray addObject:[NSNumber numberWithFloat:totalHeight]];
//        [self.scroll addSubview:insView];
//        totalHeight = totalHeight+insView.totalHeight+20;
//        
//        ownView = [[OwnerView alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96)andSettings:0 andTimeReg:[Singleton sharedInstance].ownerTimeReg andDKP:[Singleton sharedInstance].ownerDkp];
//        ownView.delegate=self;
//        ownView.textField.delegate=self;
//        [ownView setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, ownView.totalHeight)];
//        [cardArray addObject:[NSNumber numberWithFloat:totalHeight]];
//        [self.scroll addSubview:ownView];
//        totalHeight = totalHeight+ownView.totalHeight+20;
//    
//    }else{
//        totalHeight=10;
//        insView = [[InsurantView alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96)andSettings:0 andTimeReg:[Singleton sharedInstance].timeReg andDKP:[Singleton sharedInstance].dkp];
//        insView.delegate=self;
//        insView.textField.delegate=self;
//        [insView setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, insView.totalHeight)];
//        [cardArray addObject:[NSNumber numberWithFloat:totalHeight]];
//        [self.scroll addSubview:insView];
//        totalHeight = totalHeight+insView.totalHeight+20;
//        
//       
//    }
//  
//    vehicleView = [[VehicleView alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.bounds.size.width, self.view.frame.size.height*0.96) andGos:[Singleton sharedInstance].isGosNumber andOgr:[Singleton sharedInstance].ogrVehicle andPTS:[Singleton sharedInstance].PTS andSRTS:[Singleton sharedInstance].SRTS andPricep:[Singleton sharedInstance].pricep andTO2:[Singleton sharedInstance].isTO2 andNew:[Singleton sharedInstance].isNew];
//    vehicleView.delegate=self;
//    vehicleView.textField.delegate=self;
//    [vehicleView setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, vehicleView.totalHeight)];
//    [cardArray addObject:[NSNumber numberWithFloat:totalHeight]];
//    [self.scroll addSubview:vehicleView];
//    totalHeight = totalHeight+vehicleView.totalHeight+20;
//    [cardArray addObject:[NSNumber numberWithFloat:totalHeight]];
//    if(!roundView){
//        roundView=[[RoundViewChose alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, 70)];
//        [roundView setBackgroundColor:[UIColor whiteColor]];
//        roundView.delegate=self;
//    }else{
//        [roundView setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, 70)];
//    }
//    [self.scroll addSubview:roundView];
//  
//  
//    totalHeight = totalHeight + 90;
//   
//    for (int i=0; i< [Singleton sharedInstance].model.countDriver; i++) {
//        driverCard = [[PhotoDriver alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, 100) andTitle:i];
//       
//        [driverCard setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, driverCard.totalHeight)];
//         totalHeight = totalHeight + driverCard.frame.size.height+20;
//        [_scroll addSubview:driverCard];
//    }
//      [cardArray addObject:[NSNumber numberWithFloat:totalHeight]];
//    conditions = [[ConditionsView alloc]initWithFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, 100) andParameters:nil];
//    conditions.delegate=self;
//    conditions.textView.delegate=self;
//    [conditions.textView setText:@"Комментарий (пожелания к заявке)"];
//    [conditions setFrame:CGRectMake(0, totalHeight, self.scroll.frame.size.width, conditions.totalHeight)];
//    conditions.datefield.delegate=self;
//    totalHeight = totalHeight +conditions.totalHeight;
//    [self.scroll addSubview:conditions];
//    totalHeight = totalHeight +10;
//    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(_scroll.frame.size.width/2-_scroll.frame.size.width*0.8/2, totalHeight,_scroll.frame.size.width*0.8, _scroll.frame.size.height*0.07)];
//    [btn setBackgroundImage:[UIImage imageNamed:@"next_btn"] forState:UIControlStateNormal];
//    [btn addTarget:self action:@selector(issue) forControlEvents:UIControlEventTouchUpInside];
//    [btn setTitle:@"Оформить" forState:UIControlStateNormal];
//    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica Medium" size:18.f]];
//    [self.scroll addSubview:btn];
//     totalHeight = totalHeight +btn.frame.size.height+10;
//    self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, totalHeight);
//    
//   
//}
//-(void)issue{
//    [self dismissViewControllerAnimated:YES completion:^{
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:@"closeCalc"
//         object:self];
//    }];
//    
//}
//- (void)textViewDidBeginEditing:(UITextView *)textView
//{
//    if ([textView.text isEqualToString:@"Комментарий (пожелания к заявке)"])
//    {
//        textView.text = @"";
//        [textView setTextColor:[UIColor blackColor]];
//    }
//    [textView becomeFirstResponder];
//}
//
//- (void)textViewDidEndEditing:(UITextView *)textView
//{
//    if ([textView.text isEqualToString:@""]) {
//        textView.text = @"Комментарий (пожелания к заявке)";
//         [textView setTextColor:[UIColor lightGrayColor]];
//    }
//    [textView resignFirstResponder];
//}
//-(void)buttonSelected:(NSInteger)index{
//     [Singleton sharedInstance].model.countDriver=index+1;
//    [self creatingCards];
//}
//-(BOOL)textFieldShouldClear:(UITextField *)textField{
//    if(textField.tag==0){
//        [Singleton sharedInstance].phone=textField.text;
//        [_navigationBar setColorForItem:0 andColor:[UIColor customYellowColor]];
//    }
//    if(textField.tag==1){
//        
//        [Singleton sharedInstance].ownerPhone=textField.text;
//            [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//        
//    }
//    return YES;
//}
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if(textField.tag==0||textField.tag==1){
//    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    BOOL deleting = [newText length] < [textField.text length];
//    
//    NSString *stripppedNumber = [newText stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [newText length])];
//    NSUInteger digits = [stripppedNumber length];
//    
//    if (digits > 10)
//        stripppedNumber = [stripppedNumber substringToIndex:10];
//    
//    UITextRange *selectedRange = [textField selectedTextRange];
//    NSInteger oldLength = [textField.text length];
//    if (digits == 0)
//        textField.text = @"";
//    else if (digits < 3 || (digits == 3 && deleting))
//        textField.text = [NSString stringWithFormat:@"(%@", stripppedNumber];
//    else if (digits < 6 || (digits == 6 && deleting))
//        textField.text = [NSString stringWithFormat:@"(%@) %@", [stripppedNumber substringToIndex:3], [stripppedNumber substringFromIndex:3]];
//    else
//        textField.text = [NSString stringWithFormat:@"(%@) %@-%@", [stripppedNumber substringToIndex:3], [stripppedNumber substringWithRange:NSMakeRange(3, 3)], [stripppedNumber substringFromIndex:6]];
//    UITextPosition *newPosition = [textField positionFromPosition:selectedRange.start offset:[textField.text length] - oldLength];
//    UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
//    [textField setSelectedTextRange:newRange];
//    if(textField.tag==0){
//    [Singleton sharedInstance].phone=textField.text;
//        if([[Singleton sharedInstance] insViewIsDone]){
//            [_navigationBar setColorForItem:0 andColor:[UIColor customGreenColor]];
//        }else{
//            [_navigationBar setColorForItem:0 andColor:[UIColor customYellowColor]];
//        }
//    }
//    if(textField.tag==1){
//        
//    [Singleton sharedInstance].ownerPhone=textField.text;
//        if([[Singleton sharedInstance] ownViewIsDone]){
//            [_navigationBar setColorForItem:1 andColor:[UIColor customGreenColor]];
//        }else{
//            [_navigationBar setColorForItem:1 andColor:[UIColor customYellowColor]];
//        }
//    }
//    }
//    if(textField.tag==2){
//       [Singleton sharedInstance].prob = textField.text;
//        if(range.length + range.location > textField.text.length)
//        {
//            return NO;
//        }
//       
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        return newLength <= 7;
//    }
//    return NO;
//}
//
//-(void)needOpenPicker{
//    [_pickerView setHidden:NO];
//    arrayReasons =  [NSMutableArray arrayWithArray:@[@"Личная",@"Учебная езда",@"Такси"]];
//     isVehicle=TRUE;
//    [_table reloadData];
//    _reasonlbl.text = @"Цель использования ТС";
//   
//}
//-(void)needOpenTable{
//    [_pickerView setHidden:NO];
//    _reasonlbl.text = @"Период страхования";
//    arrayReasons =  [NSMutableArray arrayWithArray:@[@"1 год ",@"6 месяцев",@"3 месяца"]];
//      isVehicle=FALSE;
//     [_table reloadData];
//  
//}
//
//- (IBAction)closePickerView:(id)sender {
//    [_pickerView setHidden:YES];
//}
//
//
//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    [self keyboardWasShown:sender];
//    if([sender isEqual:conditions.datefield]){
//    datePicker=[[UIDatePicker alloc]init];
//    datePicker.datePickerMode=UIDatePickerModeDate;
//    NSDate *date = [NSDate dateWithTimeInterval:2419196 sinceDate:[NSDate date]];
//    datePicker.date = [NSDate dateWithTimeInterval:172800 sinceDate:[NSDate date]];
//    [datePicker setMaximumDate:date];
//    [datePicker setMinimumDate:datePicker.date];
//    [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"ru"]];
//    [datePicker addTarget:self action:@selector(done) forControlEvents:UIControlEventValueChanged];
//    sender.inputView=datePicker;
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
//    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
//    [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
//    dateFormat.dateStyle = NSDateFormatterLongStyle;
//    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
//    [conditions.datefield setText:[NSString stringWithFormat:@"%@",dateStri]];
//    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
//    [toolBar setTintColor:[UIColor grayColor]];
//    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Готово"
//                                                             style:UIBarButtonItemStyleDone
//                                                            target:self
//                                                            action:@selector(hideKeyboard)];
//    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
//    
//    [conditions.datefield setInputAccessoryView:toolBar];
//    }
//}
//
//-(void)done
//{
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
//    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
//    [dateFormat setDateFormat:@"YYYY-MMMM-dd"];
//    dateFormat.dateStyle = NSDateFormatterLongStyle;
//    NSString *dateStri=[dateFormat stringFromDate:datePicker.date];
//    [conditions.datefield setText:[NSString stringWithFormat:@"%@",dateStri]];
//}
//
//// Called when the UIKeyboardDidShowNotification is sent.
//- (void)keyboardWasShown:(UITextField*)sender
//{
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardHeight, 0.0);
//    _scroll.contentInset = contentInsets;
//    _scroll.scrollIndicatorInsets = contentInsets;
//
//    // If active text field is hidden by keyboard, scroll it so it's visible
//    // Your application might not need or want this behavior.
//    CGRect aRect = self.view.frame;
//    aRect.size.height -= keyboardHeight;
//    if (!CGRectContainsPoint(aRect, sender.frame.origin) ) {
//        CGPoint scrollPoint = CGPointMake(0.0, sender.frame.origin.y-keyboardHeight);
//        [_scroll setContentOffset:scrollPoint animated:YES];
//    }
//}
//
//// Called when the UIKeyboardWillHideNotification is sent
//- (void)keyboardWillBeHidden:(NSNotification*)aNotification
//{
//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    _scroll.contentInset = contentInsets;
//    _scroll.scrollIndicatorInsets = contentInsets;
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return [arrayReasons count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    
//    //Поиск ячейки
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    //Если ячейка не найдена
//    if (cell == nil) {
//        //Создание ячейки
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                       reuseIdentifier:CellIdentifier] ;
//    }
//    cell.selectionStyle=UITableViewCellSelectionStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.textLabel.text = [arrayReasons objectAtIndex:indexPath.row];
//    cell.textLabel.numberOfLines=2;
//    
//    return cell;
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
//    [style setAlignment:NSTextAlignmentLeft];
//    
//    UIFont *font1 = [UIFont fontWithName:@"Helvetica Light" size:18.f];
//    
//    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
//                            NSFontAttributeName:font1,
//                            NSParagraphStyleAttributeName:style,
//                            NSForegroundColorAttributeName : [UIColor blueColor]
//                            };
//    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
//    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:arrayReasons[indexPath.row]    attributes:dict1]];
//    if(isVehicle){
//        [vehicleView.btn setAttributedTitle:attString forState:UIControlStateNormal];
//        vehicleView.btn.titleLabel.numberOfLines = 2; // Dynamic number of lines
//    }else{
//        [conditions.btn setAttributedTitle:attString forState:UIControlStateNormal];
//        conditions.btn.titleLabel.numberOfLines = 2; // Dynamic number of lines
//        
//    }
//    [_pickerView setHidden:YES];
//    [Singleton sharedInstance].reason=arrayReasons[indexPath.row];
//}
//- (void)vehicleButtonClicked:(NSInteger)index {
//    [Singleton sharedInstance].isOwner =FALSE;
//    [Singleton sharedInstance].indexForPhoto=index;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CameraViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"camera"];
//    [self presentViewController:vc animated:YES completion:nil];
//}
//
//
//- (void)vehicleSwitcherIsOn:(NSInteger)index {
//    if(index==0){
//        [Singleton sharedInstance].isGosNumber=![Singleton sharedInstance].isGosNumber;
//    }
//    if(index==1){
//        [Singleton sharedInstance].ogrVehicle=![Singleton sharedInstance].ogrVehicle;
//    }
//    if(index==2){
//        [Singleton sharedInstance].PTS=![Singleton sharedInstance].PTS;
//        if([Singleton sharedInstance].PTS&&[Singleton sharedInstance].SRTS){
//            [Singleton sharedInstance].SRTS=FALSE;
//        }
//    }
//    if(index==3){
//        [Singleton sharedInstance].SRTS=![Singleton sharedInstance].SRTS;
//        if([Singleton sharedInstance].PTS&&[Singleton sharedInstance].SRTS){
//            [Singleton sharedInstance].PTS=FALSE;
//        }
//    }
//    if(index==4){
//        [Singleton sharedInstance].pricep=![Singleton sharedInstance].pricep;
//    }
//    if(index==5){
//        [Singleton sharedInstance].isTO2=![Singleton sharedInstance].isTO2;
//    }
//    if(index==6){
//        [Singleton sharedInstance].isNew=![Singleton sharedInstance].isNew;
//    }
//    [self creatingCards];
//}
//-(void)buttonClicked:(NSInteger)index{
//    [Singleton sharedInstance].isOwner =FALSE;
//    [Singleton sharedInstance].indexForPhoto=index;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CameraViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"camera"];
//    [self presentViewController:vc animated:YES completion:nil];
//}
//
//-(void)selectItemAtIndex:(NSInteger)index{
//    [_scroll setContentOffset:CGPointMake(0, [cardArray[index] floatValue]) animated:YES];
//}
//
//-(void)ownerButtonClicked:(NSInteger)index{
//    [Singleton sharedInstance].isOwner =TRUE;
//    [Singleton sharedInstance].indexForPhoto=index;
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CameraViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"camera"];
//    [self presentViewController:vc animated:YES completion:nil];
//}
//
//-(UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//- (IBAction)back:(id)sender {
//    CATransition *transition = [[CATransition alloc] init];
//    transition.duration =0.5;
//    transition.type = kCATransitionMoveIn;
//    transition.subtype = kCATransitionFromLeft;
//    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
//    [self.view.window.layer addAnimation:transition forKey:kCATransition];
//    [self dismissViewControllerAnimated:NO completion:nil];
//}
//
///*
// #pragma mark - Navigation
// 
// // In a storyboard-based application, you will often want to do a little preparation before navigation
// - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
// // Get the new view controller using [segue destinationViewController].
// // Pass the selected object to the new view controller.
// }
// */
//
//
//
//@end
