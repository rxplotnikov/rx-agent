//
//  UploadTask.h
//  RX Агент
//
//  Created by RX Group on 15.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadTask : NSObject
@property NSInteger itemIndex;
@property NSURLSession *session;

-(instancetype)initWithIndex:(NSInteger)index andSession:(NSURLSession*)session;

@end
