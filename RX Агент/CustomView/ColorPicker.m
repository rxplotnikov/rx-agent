//
//  ColorPicker.m
//  RX Агент
//
//  Created by RX Group on 13.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "ColorPicker.h"
#import "UIColor+NavigationColors.h"

@implementation ColorPicker{
    NSMutableArray <UIButton *>*colorArray;
    NSMutableArray <UIColor *>*backgroundColorsArray;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self=[super initWithCoder:aDecoder]){
        backgroundColorsArray=[NSMutableArray arrayWithArray:@[[UIColor cardColor1],[UIColor cardColor2],[UIColor cardColor3],[UIColor cardColor4],[UIColor cardColor5],[UIColor cardColor6],[UIColor cardColor7],[UIColor cardColor8],[UIColor cardColor9],[UIColor cardColor10],[UIColor cardColor11],[UIColor cardColor12]]];
    }
    return self;
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    colorArray = [NSMutableArray new];
    if(colorArray.count<1){
    for (int j=0; j<2; j++) {
        for (int i =0; i<6; i++) {
            
            UIButton *color = [[UIButton alloc]initWithFrame:CGRectMake(self.frame.size.width/6+(self.frame.size.width/6*i),self.frame.size.height/2*j, self.frame.size.height/2, self.frame.size.height/2)];
            [color setCenter:CGPointMake(self.bounds.size.width/6/2+self.bounds.size.width/6*i, self.frame.size.height/4+self.frame.size.height/2*j)];
            [color addTarget:self action:@selector(selectColor:) forControlEvents:UIControlEventTouchUpInside];
            color.tag=i+(j*6);
            [color setBackgroundColor:backgroundColorsArray[i+(j*6)]];
            color.layer.cornerRadius = color.frame.size.width/2;
            [self addSubview:color];
            [colorArray addObject:color];
        }
      }
    }

}
-(void)selectColor:(UIButton *)btn{
    for (UIButton *button in colorArray) {
        if(btn.tag==button.tag){
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            button.layer.borderWidth = 2.f;
        }else{
            button.layer.borderColor = [UIColor clearColor].CGColor;
            button.layer.borderWidth = 0.f;
        }
    }
    [self.delegate selectColor:backgroundColorsArray[btn.tag]];
}
@end
