//
//  CustomCamera.m
//  RXAgent
//
//  Created by RX Group on 22.02.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CustomCamera.h"


@implementation CustomCamera
@synthesize delegate;

-(instancetype)initWithView:(UIView *)view{
    if(self=[super init]){
        session = [[AVCaptureSession alloc] init];
        [session setSessionPreset:AVCaptureSessionPresetPhoto];
        AVCaptureDevice *inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        NSError *error;
        AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:&error];
        if([session canAddInput:deviceInput]){
            [session addInput:deviceInput];
        }
        AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc]initWithSession:session];
        [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        CALayer *rootLayer = [view layer];
        [rootLayer setMasksToBounds:YES];
        CGRect frame = view.frame;
        [previewLayer setFrame:frame];
        [rootLayer insertSublayer:previewLayer atIndex:0];
        stillImage = [[AVCaptureStillImageOutput alloc]init];
        NSDictionary *outputSettings =[[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey, nil];
        [stillImage setOutputSettings:outputSettings];
        [session addOutput:stillImage];
        [session startRunning];
    }
    return self;
}
-(void)captureImage{
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImage.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if([[port mediaType]isEqual:AVMediaTypeVideo]){
                videoConnection=connection;
                 AudioServicesPlaySystemSound(1108);
                break;
            }
        }
    }
    [stillImage captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef  _Nullable imageDataSampleBuffer, NSError * _Nullable error) {
        if(imageDataSampleBuffer != NULL){
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
            self->theImage = [UIImage imageWithData:imageData];
            [self.delegate photoGrabbed:self->theImage];
        }
            }];
}
@end
