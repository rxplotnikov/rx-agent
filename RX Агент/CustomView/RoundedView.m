//
//  RoundedView.m
//  RX Агент
//
//  Created by RX Group on 06.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "RoundedView.h"

@implementation RoundedView


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    self.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    self.layer.shadowOpacity = 3.f;
    self.layer.shadowRadius = 0.5f;
    self.layer.shadowOffset = CGSizeMake(1.f, 1.f);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextRef, [UIColor whiteColor].CGColor);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:18];
    [path fill];
}


@end
