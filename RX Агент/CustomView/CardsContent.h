//
//  CardsContent.h
//  RX Агент
//
//  Created by RX Group on 13.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CardsContent : NSObject

-(instancetype)initWithParametrs:(UIImage *)image
                         andType:(UIImage *)type
                       andNumber:(NSString *)number
                         andData:(NSString *)data
                        andColor:(UIColor *)color
                         andName:(NSString *)name;

@property UIImage *photoCard;
@property NSString *nameCard;
@property UIImage *typeCard;
@property NSString *numberCard;
@property NSString *dateCard;
@property BOOL isPhoto;
@property BOOL isWhite;
@property BOOL isFull;
@property UIColor *color;
@end
