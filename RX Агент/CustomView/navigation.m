//
//  navigation.m
//  RX Агент
//
//  Created by RX Group on 29.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "navigation.h"


@implementation navigation{
    NSMutableArray *items;
    NSArray *arrayNames;
    NSArray *smallArrayNames;
    NSMutableArray *buttonArray;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self=[super initWithCoder:aDecoder]){
        [self initialize];
    }
    return self;
}

-(void)initialize{
        items=[NSMutableArray new];
        for (UIView *view in self.subviews) {
            [view removeFromSuperview];
        }
        
        if([Singleton sharedInstance].osago.isOwner){
            if([Singleton sharedInstance].isLimit){
                smallArrayNames=@[@"icon1",@"icon2",@"icon3",@"icon4",@"icon5",@"icon5"];
                _countItems=5;
            }else{
                smallArrayNames=@[@"icon1",@"icon2",@"icon4",@"icon5",@"icon3",@"icon11"];
                _countItems=4;
            }
        }else{
            if([Singleton sharedInstance].isLimit){
                smallArrayNames=@[@"icon1",@"icon11",@"icon2",@"icon3",@"icon4",@"icon5"];
                _countItems=6;
            }else{
                smallArrayNames=@[@"icon1",@"icon11",@"icon2",@"icon4",@"icon5",@"icon3"];
                _countItems=5;
            }
            
        }
    
        for (int i =0; i<6; i++) {
            UIView *item =[[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width/_countItems*i, 0, self.frame.size.height-10, self.frame.size.height-10)];
            [item setCenter:CGPointMake(self.bounds.size.width/_countItems/2+self.bounds.size.width/_countItems*i, self.bounds.size.height/2)];
            [item setBackgroundColor:[UIColor colorWithRed:229.f/255.f green:229.f/255.f blue:229.f/255.f alpha:1.f]];
            item.layer.cornerRadius=item.frame.size.width/2;
            UIButton *btn = [[UIButton alloc]initWithFrame:item.bounds];
            btn.tag=i;
            [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            [btn setImage:[UIImage imageNamed:smallArrayNames[i]] forState:UIControlStateNormal];
            [item addSubview:btn];
            [items addObject:item];
            [self addSubview:item];
        }
   
   
}

-(void)setColorForItem:(NSInteger)index andColor:(UIColor*)color{
    [((UIView*)self->items[index]) setBackgroundColor:color];
}



-(void)clickAction:(UIButton*)button{
    [_delegate selectItemAtIndex:button.tag];
}
@end
