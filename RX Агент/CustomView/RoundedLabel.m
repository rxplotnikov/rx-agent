//
//  RoundedLabel.m
//  RX Агент
//
//  Created by RX Group on 09.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "RoundedLabel.h"

@implementation RoundedLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 5, 0, 5};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    self.backgroundColor=[UIColor clearColor];
    self.layer.cornerRadius=6.f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth= 1.f;
}

@end
