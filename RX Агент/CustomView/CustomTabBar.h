//
//  CustomTabBar.h
//  RX Агент
//
//  Created by RX Group on 25.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  TabBarDelegate <NSObject>
-(void)selectItemAtIndex:(NSInteger)index;

@end
@interface CustomTabBar : UIView
@property(weak, nonatomic) id<TabBarDelegate>delegate;
@end
