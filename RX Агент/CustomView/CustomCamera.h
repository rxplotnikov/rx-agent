//
//  CustomCamera.h
//  RXAgent
//
//  Created by RX Group on 22.02.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol CameraDelegate <NSObject>

- (void)photoGrabbed:(UIImage*)image;

@end

@interface CustomCamera : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate>{
    AVCaptureSession *session;
    UIImage *theImage;
    AVCaptureStillImageOutput *stillImage;
}

@property (nonatomic, assign) id<CameraDelegate> delegate;
-(instancetype)initWithView:(UIView *)view;
-(void)captureImage;
@end
