//
//  navigation.h
//  RX Агент
//
//  Created by RX Group on 29.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@protocol  NavigationDelegate <NSObject>
-(void)selectItemAtIndex:(NSInteger)index;

@end
@interface navigation : UIView
@property(weak, nonatomic) id<NavigationDelegate>delegate;
@property NSInteger countItems;
-(void)initialize;
-(void)setColorForItem:(NSInteger)index andColor:(UIColor*)color;
@end
