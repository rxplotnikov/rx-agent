//
//  BankCardView.m
//  RX Агент
//
//  Created by RX Group on 11.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "BankCardView.h"

@implementation BankCardView{
    UIImageView *imageCard;
}

-(void)setCardType:(UIImage *)type andNumber:(NSString *)number andDate:(NSString*)date andColor:(UIColor *)color andFrame:(CGRect)frame andImage:(UIImage*)backgroundImage andName:(NSString *)name{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
        [imageCard removeFromSuperview];
    imageCard=nil;
        self.layer.cornerRadius=8.f;
        self.layer.masksToBounds=YES;
        self.clipsToBounds=YES;
        self.layer.borderColor=[UIColor blackColor].CGColor;
        self.layer.borderWidth=1.f;
        if(backgroundImage){
            if(!imageCard){
                [self createImage];
            }
            [imageCard setFrame:frame];
            [imageCard setImage:backgroundImage];
            [self setBackgroundColor:[UIColor clearColor]];
            [self setNeedsDisplay];
            [self createDataLabelAtPoint:CGPointMake(self.frame.size.width*0.05, self.frame.size.height*0.2) withText:date];
            [self createLabelAtPoint:CGPointMake(self.frame.size.width*0.05, self.frame.size.height*0.5) withText:number];
           
        }else{
            [self setBackgroundColor:color];
             [self createDataLabelAtPoint:CGPointMake(75, self.frame.size.height*0.2) withText:date];
            [self createImage:type atPoint:CGPointMake(10, self.frame.size.height*0.2)];
            [self createLabelAtPoint:CGPointMake(10, self.frame.size.height*0.5) withText:number];
        }
     [self createNameAtPoint:CGPointMake(self.frame.size.width*0.05, self.frame.size.height*0.7) withText:name];
    UIButton *btn = [[UIButton alloc]initWithFrame:self.bounds];
    [btn addTarget:self action:@selector(editCard) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
}
-(instancetype)initWithFrame:(CGRect)frame{
    if(self == [super initWithFrame:frame]){
       [self createImage];
    }
    return self;
}
-(void)createImage{
    imageCard = [[UIImageView alloc]initWithFrame:self.bounds];
    
    [self addSubview:imageCard];
}
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [imageCard setFrame:rect];
}

-(void)createImage:(UIImage *)image atPoint:(CGPoint)point{
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(point.x, point.y, 60, 40)];
    [imageView setImage:image];
    [self addSubview:imageView];
}

-(void)createDataLabelAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Bold" size:20.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, 42, 20)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}
-(void)createNameAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Bold" size:20];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, self.bounds.size.width-point.x*2, 44)];
    fromLabel.text = string;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentCenter;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}
-(void)createLabelAtPoint:(CGPoint)point withText:(NSString *)string{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Bold" size:28.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, self.bounds.size.width-20, 44)];
    fromLabel.text = [self formatCardNumber:string];
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentCenter;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}

-(NSString*)formatCardNumber:(NSString*)text{
    
    for (int i = 0; i < text.length - 4; i++) {
        
        if (![[text substringWithRange:NSMakeRange(i, 1)] isEqual:@" "]) {
            NSRange range = NSMakeRange(i, 1);
            text = [text stringByReplacingCharactersInRange:range withString:@"*"];
        }
        
    }
    
    return [self separateSymbols:text];
}

-(NSString *)separateSymbols:(NSString*)text{
    NSMutableString *resultString = [NSMutableString string];
    
    for(int i = 0; i<[text length]/4; i++)
    {
        NSUInteger fromIndex = i * 4;
        NSUInteger len = [text length] - fromIndex;
        if (len > 4) {
            len = 4;
        }
        
        [resultString appendFormat:@"%@ ",[text substringWithRange:NSMakeRange(fromIndex, len)]];
    }
    return resultString;
}
-(void)editCard{
    [self.delegate cardNeedEdit];
}
@end
