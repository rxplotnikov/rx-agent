//
//  SaverImage.m
//  RX Агент
//
//  Created by RX Group on 12.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "SaverImage.h"

@implementation SaverImage

-(NSString *)getCacheDirectoryPath

{
    
    NSArray *_array =NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    return [_array objectAtIndex:0];
    
}

/* saving image to cache directory */



-(void)saveImage:(UIImage *)img withName:(NSString *)name

{
    
    NSData *_pngData = UIImagePNGRepresentation(img);
    
    NSString *_imagePath =[NSString stringWithFormat:@"%@/%@.png",[self getCacheDirectoryPath],name];
    
    if([[NSFileManager defaultManager]fileExistsAtPath:_imagePath])
        
    {
        
        [[NSFileManager defaultManager]removeItemAtPath:_imagePath error:nil];
        
    }
    
    [_pngData writeToFile:_imagePath atomically:YES]; //Write the file
    
}

/* getting image from local storage */

-(UIImage *)getImageFromCacheWithName:(NSString *)name

{
    
    NSString *_imagePath =[NSString stringWithFormat:@"%@/%@.png",[self getCacheDirectoryPath],name];
    
    if([[NSFileManager defaultManager]fileExistsAtPath:_imagePath])
        
    {
        
        UIImage *_image =[UIImage imageWithContentsOfFile:_imagePath];
        
        return _image;
        
    }
    
    return nil;
    
}
@end
