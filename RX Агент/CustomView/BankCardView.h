//
//  BankCardView.h
//  RX Агент
//
//  Created by RX Group on 11.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  CardDelegate <NSObject>
-(void)cardNeedEdit;

@end
@interface BankCardView : UIView


-(void)setCardType:(UIImage *)type andNumber:(NSString *)number andDate:(NSString*)date andColor:(UIColor *)color andFrame:(CGRect)frame andImage:(UIImage*)backgroundImage andName:(NSString *)name;
@property(weak, nonatomic) id<CardDelegate>delegate;
@end
