//
//  CardCell.m
//  RX Агент
//
//  Created by RX Group on 12.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CardCell.h"

@implementation CardCell
-(void)awakeFromNib{
    [super awakeFromNib];
    self.layer.cornerRadius=12.f;
    self.layer.borderWidth=0.7f;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
   
}

@end
