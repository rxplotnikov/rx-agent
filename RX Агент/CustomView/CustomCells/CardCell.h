//
//  CardCell.h
//  RX Агент
//
//  Created by RX Group on 12.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoCard;
@property (weak, nonatomic) IBOutlet UIImageView *typeCard;
@property (weak, nonatomic) IBOutlet UILabel *numberCard;
@property (weak, nonatomic) IBOutlet UILabel *dateCard;
@property (weak, nonatomic) IBOutlet UILabel *nameCard;
@property BOOL isPhoto;
@property BOOL isWhite;
@property BOOL isFull;
@end
