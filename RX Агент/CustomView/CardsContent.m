//
//  CardsContent.m
//  RX Агент
//
//  Created by RX Group on 13.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CardsContent.h"

@implementation CardsContent

-(instancetype)initWithParametrs:(UIImage *)image
                         andType:(UIImage *)type
                       andNumber:(NSString *)number
                         andData:(NSString *)data
                        andColor:(UIColor *)color
                         andName:(NSString *)name{
    if(self = [super init]){
        if(image){
            _isPhoto=TRUE;
            _photoCard = image;
        }else{
            _isPhoto = FALSE;
            _typeCard = type;
            _color = color;
        }
        _nameCard = name;
        _numberCard = number;
        _dateCard = data;
    }
    return self;
}
@end
