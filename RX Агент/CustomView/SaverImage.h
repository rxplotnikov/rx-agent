//
//  SaverImage.h
//  RX Агент
//
//  Created by RX Group on 12.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SaverImage : NSObject

-(void)saveImage:(UIImage *)img withName:(NSString *)name;
-(UIImage *)getImageFromCacheWithName:(NSString *)name;

@end
