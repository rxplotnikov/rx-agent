//
//  CustomTabBar.m
//  RX Агент
//
//  Created by RX Group on 25.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CustomTabBar.h"
#import "scoreView.h"

@implementation CustomTabBar{
    UIView *mainView;
    UIView *centerBtn;
    NSMutableArray<UIButton*>*buttons;
    scoreView *score;
    NSArray *arrayName;
    NSMutableArray <UIButton *>*actionsArray;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self=[super initWithCoder:aDecoder]){
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countWork:)
                                                 name:@"getCountWork"
                                               object:nil];
     self.backgroundColor = [UIColor clearColor];
     arrayName = @[@"Лента",@"В работе",@"",@"Поддержка",@"Настройки"];
     mainView= [[UIView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height-self.bounds.size.height*0.87, self.frame.size.width, self.bounds.size.height*0.87)];
     [mainView setBackgroundColor:[UIColor whiteColor]];
     [self addSubview:mainView];
     buttons = [NSMutableArray new];
     actionsArray=[NSMutableArray new];
        for (int i = 0 ; i<5; i++) {
            if(i==2){
                centerBtn = [[UIView alloc]initWithFrame:CGRectMake(0.f, 0.f, self.frame.size.height, self.frame.size.height)];
                [centerBtn setBackgroundColor:[UIColor colorWithRed:255.f/255.f green:197.f/255.f blue:42.f/255.f alpha:1.f]];
                [centerBtn setCenter:CGPointMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2)];
                centerBtn.layer.cornerRadius=centerBtn.frame.size.width/2;
              
               
                [mainView addSubview:centerBtn];
                
                UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0.f, 0.f, mainView.bounds.size.height*0.5, mainView.bounds.size.height*0.5)];
                [btn setCenter:centerBtn.center];
                [btn setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
                btn.tag=i;
                
                [mainView addSubview:btn];
                [buttons addObject:btn];
            }else{
                
                if(i==1){
                    score = [[scoreView alloc]initWithFrame:CGRectMake(0.f, 0.f, mainView.bounds.size.width*0.2, mainView.bounds.size.height*0.36)];
                    [score setCenter:CGPointMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2)];
                    [score setScore:@"0"];
                    [buttons addObject:[NSNull null]];
                    [mainView addSubview:score];
                }else{
                    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0.f, 0.f, mainView.bounds.size.height*0.5,    mainView.bounds.size.height*0.5)];
                    [btn setCenter:CGPointMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2)];
                    if(i==3){
                        [btn setImage:[UIImage imageNamed:@"support"] forState:UIControlStateNormal];
                    }
                    else
                    if (i==4){
                        [btn setImage:[UIImage imageNamed:@"settings"] forState:UIControlStateNormal];
                    }else{
                        [btn setImage:[UIImage imageNamed:@"lent"] forState:UIControlStateNormal];
                    }
            
                    btn.tag=i;
                    [mainView addSubview:btn];
                    [buttons addObject:btn];
                }
            }
            UIButton *actionBtn =[[UIButton alloc]initWithFrame:CGRectMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, 0, mainView.bounds.size.width/5, self.bounds.size.height)];
            actionBtn.tag=i;
            [actionBtn addTarget:self action:@selector(tabbaraction:) forControlEvents:UIControlEventTouchUpInside];
            [actionsArray addObject:actionBtn];
            [mainView addSubview:actionBtn];
        }
       
    }
    return self;
}
-(void)countWork:(NSNotification *)notification{
    [score setScore:[notification.userInfo[@"total"] stringValue]];
}
-(void)tabbaraction:(UIButton*)btn{
    for (UILabel *lbl in mainView.subviews) {
        if(btn.tag!=2){
            
            if([lbl isKindOfClass:[UILabel class]]){
                if(lbl.tag == btn.tag){
                  
                    [lbl setTextColor:[UIColor colorWithRed:255.f/255.f green:197.f/255.f blue:42.f/255.f alpha:1.f]];
                }else{
                   
                    [lbl setTextColor:[UIColor colorWithRed:146.f/255.f green:146.f/255.f blue:146.f/255.f alpha:1.f]];
                }
            }
        }
    }
    if(btn.tag!=2){
        for (UIButton *button in buttons) {
            if(![button isKindOfClass:[NSNull class]]){
                if(button.tag == btn.tag){
                    button.layer.shadowColor =[UIColor colorWithRed:255.f/255.f green:197.f/255.f blue:42.f/255.f alpha:1.f].CGColor;
                    button.layer.shadowRadius = 1.f;
                    button.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
                    button.layer.shadowOpacity = 1.f;
                    button.layer.masksToBounds = NO;
                    [button setTintColor:[UIColor colorWithRed:255.f/255.f green:197.f/255.f blue:42.f/255.f alpha:1.f]];
                }else{
                   
                    button.layer.shadowRadius = 0.f;
                    button.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
                    button.layer.shadowOpacity = 0.f;
                    button.layer.masksToBounds = NO;
                    [button setTintColor:[UIColor colorWithRed:146.f/255.f green:146.f/255.f blue:146.f/255.f alpha:1.f]];
                }
        }
        }
    }
    [_delegate selectItemAtIndex:btn.tag];
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [mainView setFrame:CGRectMake(0, self.bounds.size.height-self.bounds.size.height*0.87, self.frame.size.width, self.bounds.size.height*0.87)];
    for (int i = 0; i<buttons.count; i++) {
        [actionsArray[i]setFrame:CGRectMake(mainView.bounds.size.width/5*i, 0, mainView.bounds.size.width/5, self.bounds.size.height)];
        UIFont * customFont = [UIFont fontWithName:@"Helvetica Light" size:11.f]; //custom font
        NSString * text = arrayName[i];
        
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(mainView.bounds.size.width/5*i, mainView.bounds.size.height/1.7, mainView.frame.size.width/5, 20)];
        
        fromLabel.tag=i;
        fromLabel.text = text;
        fromLabel.font = customFont;
        fromLabel.numberOfLines = 1;
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.clipsToBounds = YES;
        fromLabel.backgroundColor = [UIColor clearColor];
        if(i==0){
            [fromLabel setTextColor:[UIColor colorWithRed:255.f/255.f green:197.f/255.f blue:42.f/255.f alpha:1.f]];
        }else{
        fromLabel.textColor = [UIColor colorWithRed:146.f/255.f green:146.f/255.f blue:146.f/255.f alpha:1.f];
        }
        fromLabel.textAlignment = NSTextAlignmentCenter;
        [mainView addSubview:fromLabel];
        if(![buttons[i] isKindOfClass:[NSNull class]]){
            if(i==0){
             [buttons[i] setTintColor:[UIColor colorWithRed:255.f/255.f green:197.f/255.f blue:42.f/255.f alpha:1.f]];
            }else{
            [buttons[i] setTintColor:[UIColor colorWithRed:146.f/255.f green:146.f/255.f blue:146.f/255.f alpha:1.f]];
            }
            if(i==2){
                [centerBtn setFrame:CGRectMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2, self.frame.size.height, self.frame.size.height)];
                [centerBtn setCenter:CGPointMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2.5)];
                centerBtn.layer.cornerRadius=centerBtn.frame.size.width/2;
                [[buttons objectAtIndex:i] setCenter:centerBtn.center];
            }else{
                [[buttons objectAtIndex:i] setCenter:CGPointMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2.5)];
            }
        }else{
            [score setFrame:CGRectMake(0.f, 0.f, mainView.bounds.size.width*0.15, mainView.bounds.size.height*0.36)];
            [score setCenter:CGPointMake(mainView.bounds.size.width/5/2+mainView.bounds.size.width/5*i, mainView.bounds.size.height/2.5)];
           
        }
    }
}
@end
