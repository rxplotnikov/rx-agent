//
//  scoreView.m
//  RX Агент
//
//  Created by RX Group on 25.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "scoreView.h"

@implementation scoreView{
    UILabel *fromLabel;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        
        fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        fromLabel.numberOfLines = 1;
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        fromLabel.adjustsFontSizeToFitWidth = YES;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.clipsToBounds = YES;
        fromLabel.backgroundColor = [UIColor clearColor];
        fromLabel.textColor = [UIColor whiteColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:fromLabel];
    }
    return self;
}
-(void)setScore:(NSString *)score{
    [fromLabel setText:score];
    self.layer.borderWidth=1;
    if([score integerValue]==0){
        self.layer.borderColor =[UIColor colorWithRed:146.f/255.f green:146.f/255.f blue:146.f/255.f alpha:1.f].CGColor;
        [self setBackgroundColor:[UIColor colorWithRed:146.f/255.f green:146.f/255.f blue:146.f/255.f alpha:1.f]];
    }else{
         self.layer.borderColor =[UIColor colorWithRed:206.f/255.f green:226.f/255.f blue:131.f/255.f alpha:1.f].CGColor;
        [self setBackgroundColor:[UIColor colorWithRed:206.f/255.f green:226.f/255.f blue:131.f/255.f alpha:1.f]];
    }
}
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    self.layer.cornerRadius=10.f;
    self.layer.masksToBounds=YES;
    [fromLabel setFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
}
@end
