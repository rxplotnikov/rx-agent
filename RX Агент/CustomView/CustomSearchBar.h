//
//  CustomSearchBar.h
//  RX Агент
//
//  Created by RX Group on 06.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol SearchBarDelegate;

//A clean, easy to use, awesome replacement for UISearchBar.
@interface CustomSearchBar : UIView

//Wrappers around the Textfield subview
@property (nonatomic) NSString *text;
@property (nonatomic) UIFont *font;
@property (nonatomic) NSString *placeholder;

//The text field subview
@property (nonatomic) UITextField *textField;
@property (nonatomic, getter = isCancelButtonHidden) BOOL cancelButtonHidden; //NO by Default
@property (nonatomic, weak) id <SearchBarDelegate> delegate;

@end



@protocol SearchBarDelegate <NSObject>

@optional
- (void)searchBarCancelButtonClicked:(CustomSearchBar *)searchBar;
- (void)searchBarSearchButtonClicked:(CustomSearchBar *)searchBar;

- (BOOL)searchBarShouldBeginEditing:(CustomSearchBar *)searchBar;
- (void)searchBarTextDidBeginEditing:(CustomSearchBar *)searchBar;
- (void)searchBarTextDidEndEditing:(CustomSearchBar *)searchBar;

- (void)searchBar:(CustomSearchBar *)searchBar textDidChange:(NSString *)searchText;

@end



