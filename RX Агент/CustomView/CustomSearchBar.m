//
//  CustomSearchBar.m
//  RX Агент
//
//  Created by RX Group on 06.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CustomSearchBar.h"
#import "RoundedView.h"

#define kXMargin 8
#define kYMargin 4
#define kIconSize 22

#define kSearchBarHeight 32


@interface CustomSearchBar () <UITextFieldDelegate>
{
    BOOL _cancelButtonHidden;
}
@property (nonatomic) UIButton *cancelButton;
@property (nonatomic) UIImageView *searchImageView;
@property (nonatomic) RoundedView *backgroundView;

@property (nonatomic) UIImage *searchImage;

@end

@implementation CustomSearchBar

#pragma mark - Initializers

- (void)setDefaults {
    
    UIImage *searchIcon = [UIImage imageNamed:@"search"];
    _searchImage = searchIcon;
    self.backgroundColor = [UIColor clearColor];
    
    NSUInteger boundsWidth = self.frame.size.width;
    NSUInteger textFieldHeight = self.bounds.size.height - kYMargin;
   
    self.backgroundView = [[RoundedView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.bounds.size.height)];
    [self addSubview:self.backgroundView];
    
   
    self.searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 0, kIconSize, kIconSize)];
    self.searchImageView.image = self.searchImage;
    self.searchImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.searchImageView.center = CGPointMake(kIconSize/2 + kXMargin, CGRectGetMidY(self.bounds));
    [self addSubview:self.searchImageView];
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(2*kXMargin + kIconSize, kYMargin, boundsWidth - (2*kXMargin + kIconSize), textFieldHeight)];
    self.textField.delegate = self;
    self.textField.returnKeyType = UIReturnKeySearch;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    UIFont *defaultFont =  [UIFont fontWithName:@"Helvetica Light" size:16.f];
    self.textField.font = defaultFont;
    self.textField.textColor = [UIColor blackColor];
    [self addSubview:self.textField];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.textField];
    
}
- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [self.backgroundView setFrame:self.bounds];
    [self.textField setFrame:CGRectMake(2*kXMargin + kIconSize, 0, self.bounds.size.width - (2*kXMargin + kIconSize), self.bounds.size.height)];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDefaults];
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    
    CGRect newFrame = frame;
    frame.size.height = kSearchBarHeight;
    frame = newFrame;
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaults];
    }
    return self;
}


- (id)init
{
    return [self initWithFrame:CGRectMake(10, 20, 300, 32)];
}



#pragma mark - Properties and actions

- (NSString *)text {
    return self.textField.text;
}


- (void)setText:(NSString *)text {
    self.textField.text = text;
}


- (NSString *)placeholder {
    return self.textField.placeholder;
}


- (void)setPlaceholder:(NSString *)placeholder {
    self.textField.placeholder = placeholder;
}


- (UIFont *)font {
    return self.textField.font;
}


- (void)setFont:(UIFont *)font {
    self.textField.font = font;
}


- (BOOL)isCancelButtonHidden {
    return _cancelButtonHidden;
}


- (void)setCancelButtonHidden:(BOOL)cancelButtonHidden {
    
    if (_cancelButtonHidden != cancelButtonHidden) {
        _cancelButtonHidden = cancelButtonHidden;
        self.cancelButton.hidden = cancelButtonHidden;
    }
}


- (void)pressedCancel: (id)sender {
    if ([self.delegate respondsToSelector:@selector(searchBarCancelButtonClicked:)])
        [self.delegate searchBarCancelButtonClicked:self];
}


#pragma mark - Text Delegate

- (void)textChanged: (id)sender {
    if ([self.delegate respondsToSelector:@selector(searchBar:textDidChange:)])
        [self.delegate searchBar:self textDidChange:self.text];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(searchBarShouldBeginEditing:)])
        return [self.delegate searchBarShouldBeginEditing:self];
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(searchBarTextDidBeginEditing:)])
        [self.delegate searchBarTextDidBeginEditing:self];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(searchBarTextDidEndEditing:)])
        [self.delegate searchBarTextDidEndEditing:self];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(searchBarSearchButtonClicked:)])
        [self.delegate searchBarSearchButtonClicked:self];
    
    return YES;
}


- (BOOL)isFirstResponder {
    return [self.textField isFirstResponder];
}


- (BOOL)becomeFirstResponder {
    return [self.textField becomeFirstResponder];
}


- (BOOL)resignFirstResponder {
    [self.textField resignFirstResponder];
    return YES;
}



@end
