//
//  CategoryView.m
//  RX Агент
//
//  Created by RX Group on 22.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "CategoryView.h"
#import "Singleton.h"

@implementation CategoryView

-(instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        self.layer.cornerRadius=12.f;
        self.layer.masksToBounds=YES;
        [self setBackgroundColor:[UIColor colorWithRed:227./255. green:240./255. blue:216./255. alpha:1.f]];
        [self createLabel];
    }
    return self;
}
-(void)createLabel{
    UIFont * customFont = [UIFont fontWithName:@"Helvetica Bold" size:18.f];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:self.bounds];
    fromLabel.text = [Singleton sharedInstance].osago.category;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 2;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor colorWithRed:83.f/255.f green:117.f/255.f blue:67.f/255.f alpha:1.f];
    fromLabel.textAlignment = NSTextAlignmentCenter;
    [self bringSubviewToFront:fromLabel];
    [self addSubview:fromLabel];
}
@end
