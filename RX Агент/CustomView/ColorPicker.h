//
//  ColorPicker.h
//  RX Агент
//
//  Created by RX Group on 13.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  ColorPickerDelegate <NSObject>
-(void)selectColor:(UIColor *)color;

@end

@interface ColorPicker : UIView
@property(weak, nonatomic) id<ColorPickerDelegate>delegate;
@end
