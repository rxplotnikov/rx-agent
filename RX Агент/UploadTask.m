//
//  UploadTask.m
//  RX Агент
//
//  Created by RX Group on 15.08.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import "UploadTask.h"

@implementation UploadTask

-(instancetype)initWithIndex:(NSInteger)index andSession:(NSURLSession*)session{
    if(self=[super init]){
        _itemIndex=index;
        _session=session;
    }
    return self;
}

@end
