//
//  AppDelegate.h
//  RX Агент
//
//  Created by RX Group on 21.05.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@protocol OIDExternalUserAgentSession;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(nonatomic, strong, nullable) UIWindow *window;
@property(nonatomic, strong, nullable) id<OIDExternalUserAgentSession> currentAuthorizationFlow;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

