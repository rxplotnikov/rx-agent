//
//  RXServerConnector.h
//  RX Агент
//
//  Created by RX Group on 20.07.2018.
//  Copyright © 2018 RX Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadTask.h"
#import <UIKit/UIKit.h>


@protocol RXConnectorDelegate <NSObject>

- (void)progressLoading:(float)progress atIndex:(NSInteger)index;

@end

@interface RXServerConnector : NSObject<NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>

@property NSString *token;
@property NSMutableArray *uploadTasks;
@property (nonatomic,weak) id<RXConnectorDelegate> delegate;

+ (RXServerConnector*)sharedConnector;


-(void)getDataFromURL:(NSString*)url
            withBlock:(void (^)(NSMutableDictionary *dictionary))handler;
-(void)sendImageToServer:(NSString*)url
                 andName:(NSString*)name
                andImage:(NSData* )image
                andIndex:(NSInteger)index
               withBlock:(void (^)(NSMutableDictionary *dictionary))handler;
-(void)postJsonToServer:(NSString *)postJSON posturl:(NSString *)post_url withBlock:(void (^)(NSDictionary *json))block;
-(void)patchRequestFromURL:(NSString*)url withBlock:(void (^)(NSMutableDictionary *dictionary))handler withErrorBlock:(void (^)(NSString *error))errorHandler;
@end

